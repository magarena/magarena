name=Murmuring Bosk
image=https://cards.scryfall.io/border_crop/front/5/a/5aca73a9-e90d-48c6-bdd9-9a3f4f552de3.jpg?1673306119
image_updated=2017-08-17
value=4.185
rarity=R
type=Land
subtype=Forest
ability=As SN enters the battlefield, you may reveal a Treefolk card from your hand. If you don't, SN enters the battlefield tapped.;\
        {T}: Add {W} or {B}. SN deals 1 damage to you.
timing=land
oracle=As Murmuring Bosk enters the battlefield, you may reveal a Treefolk card from your hand. If you don't, Murmuring Bosk enters the battlefield tapped.\n{T}: Add {W} or {B}. Murmuring Bosk deals 1 damage to you.
status=not supported: reveal-cost
