name=Rattlechains
image=https://cards.scryfall.io/border_crop/front/7/0/709f6376-9a33-4900-a7ae-00767fb9f9bf.jpg?1641602019
value=2.500
rarity=R
type=Creature
subtype=Spirit
cost={1}{U}
pt=2/1
ability=Flash;\
        Flying;\
        When SN enters the battlefield, target Spirit gains hexproof until end of turn.;\
        You may cast Spirit spells as though they had flash.
timing=flash
oracle=Flash\nFlying\nWhen Rattlechains enters the battlefield, target Spirit gains hexproof until end of turn.\nYou may cast Spirit spells as though they had flash.
status=not supported: card-modification
