name=Cultivator Drone
image=https://cards.scryfall.io/border_crop/front/b/b/bbe4bb6f-bec6-49d2-99d7-e3750a0ea03f.jpg?1562932773
image_updated=2016-02-29
value=2.500
rarity=C
type=Creature
subtype=Eldrazi,Drone
cost={2}{U}
pt=2/3
ability=Devoid;\
        {T}: Add {C}. Spend this mana only to cast a colorless spell, activate an ability of a colorless permanent, or pay a cost that contains {C}.
timing=main
oracle=Devoid\n{T}: Add {C}. Spend this mana only to cast a colorless spell, activate an ability of a colorless permanent, or pay a cost that contains {C}.
status=not supported: mana-restriction
