name=Preacher
image=https://cards.scryfall.io/border_crop/front/d/8/d8e6f74c-0c69-4b83-ba4e-4470f50c1fb4.jpg?1559592268
value=4.297
rarity=R
type=Creature
subtype=Human,Cleric
cost={1}{W}{W}
pt=1/1
ability=You may choose not to untap SN during your untap step.;\
        {T}: For as long as SN remains tapped, gain control of target creature of an opponent's choice he or she controls.
timing=main
oracle=You may choose not to untap Preacher during your untap step.\n{T}: For as long as Preacher remains tapped, gain control of target creature of an opponent's choice he or she controls.
status=needs groovy
