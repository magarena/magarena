name=Flameblade Adept
image=https://cards.scryfall.io/border_crop/front/f/9/f974d05a-3b31-4661-af18-58d87b76f19e.jpg?1543675570
value=2.500
rarity=U
type=Creature
subtype=Jackal,Warrior
cost={R}
pt=1/2
ability=Menace;\
        Whenever you cycle or discard a card, SN gets +1/+0 until end of turn.
timing=main
oracle=Menace\nWhenever you cycle or discard a card, Flameblade Adept gets +1/+0 until end of turn.
status=not supported: blocking-restriction
