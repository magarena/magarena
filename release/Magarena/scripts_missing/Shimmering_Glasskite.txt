name=Shimmering Glasskite
image=https://cards.scryfall.io/border_crop/front/9/4/94bfedca-5a85-41e9-94be-67ed16bd634e.jpg?1562878484
value=3.297
rarity=C
type=Creature
subtype=Spirit
cost={3}{U}
pt=2/3
ability=Flying;\
        Whenever SN becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.
timing=main
oracle=Flying\nWhenever Shimmering Glasskite becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.
status=not supported: no-info
