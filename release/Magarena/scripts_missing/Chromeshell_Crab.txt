name=Chromeshell Crab
image=https://cards.scryfall.io/border_crop/front/c/9/c91cf95f-5007-409c-b891-00e10a3477e0.jpg?1568003959
value=3.765
rarity=R
type=Creature
subtype=Crab,Beast
cost={4}{U}
pt=3/3
ability=Morph {4}{U};\
        When SN is turned face up, you may exchange control of target creature you control and target creature an opponent controls.
timing=main
oracle=Morph {4}{U}\nWhen Chromeshell Crab is turned face up, you may exchange control of target creature you control and target creature an opponent controls.
status=not supported: multiple-targets
