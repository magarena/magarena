name=Hope of Ghirapur
image=https://cards.scryfall.io/border_crop/front/6/f/6f4bcadd-7eff-4294-94d5-52482a734d5b.jpg?1576382268
value=2.500
rarity=R
type=Legendary,Artifact,Creature
subtype=Thopter
cost={1}
pt=1/1
ability=Flying;\
        Sacrifice SN: Until your next turn, target player who was dealt combat damage by SN this turn can't cast noncreature spells.
timing=main
oracle=Flying\nSacrifice Hope of Ghirapur: Until your next turn, target player who was dealt combat damage by Hope of Ghirapur this turn can't cast noncreature spells.
status=not supported: no-info
