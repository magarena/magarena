name=Torchling
image=https://cards.scryfall.io/border_crop/front/a/2/a2ba03cd-1347-46e6-9117-2b22ce62a645.jpg?1592765764
image_updated=2017-01-11
value=3.468
rarity=R
type=Creature
subtype=Shapeshifter
cost={3}{R}{R}
pt=3/3
ability={R}: Untap SN.;\
        {R}: Target creature blocks SN this turn if able.;\
        {R}: Change the target of target spell that targets only SN.;\
        {1}: SN gets +1/-1 until end of turn.;\
        {1}: SN gets -1/+1 until end of turn.
timing=main
oracle={R}: Untap Torchling.\n{R}: Target creature blocks Torchling this turn if able.\n{R}: Change the target of target spell that targets only Torchling.\n{1}: Torchling gets +1/-1 until end of turn.\n{1}: Torchling gets -1/+1 until end of turn.
status=not supported: blocking-restriction
