name=Nether Traitor
image=https://cards.scryfall.io/border_crop/front/8/f/8f3482ec-8466-4680-bfdc-e1029c0fbe89.jpg?1619395794
value=4.141
rarity=R
type=Creature
subtype=Spirit
cost={B}{B}
pt=1/1
ability=Haste;\
        Shadow;\
        Whenever another creature is put into your graveyard from the battlefield, you may pay {B}. If you do, return SN from your graveyard to the battlefield.
timing=fmain
oracle=Haste\nShadow\nWhenever another creature is put into your graveyard from the battlefield, you may pay {B}. If you do, return Nether Traitor from your graveyard to the battlefield.
status=not supported: trigger-from-graveyard
