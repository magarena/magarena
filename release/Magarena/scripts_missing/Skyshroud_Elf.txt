name=Skyshroud Elf
image=https://cards.scryfall.io/border_crop/front/c/d/cd47a927-3ab3-4d2e-b962-ce94eeaba4f9.jpg?1562431737
value=2.500
rarity=C
type=Creature
subtype=Elf,Druid
cost={1}{G}
pt=1/1
ability={T}: Add {G}.;\
        {1}: Add {R} or {W}.
timing=main
oracle={T}: Add {G}.\n{1}: Add {R} or {W}.
status=not supported: mana-ability-with-choice
