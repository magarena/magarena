name=Purphoros's Emissary
image=https://cards.scryfall.io/border_crop/front/3/d/3dd318d3-3d84-4f3f-a48f-6a242474a4a2.jpg?1673147882
value=3.538
rarity=U
type=Enchantment,Creature
subtype=Ox
cost={3}{R}
pt=3/3
ability=Bestow {6}{R};\
        Menace;\
        Enchanted creature gets +3/+3 and has menace.
timing=main
oracle=Bestow {6}{R}\nMenace\nEnchanted creature gets +3/+3 and has menace.
status=not supported: blocking-restriction
