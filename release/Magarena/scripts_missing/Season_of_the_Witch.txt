name=Season of the Witch
image=https://cards.scryfall.io/border_crop/front/0/6/06900a71-34ca-48c6-94ac-fca744356829.jpg?1562896564
value=3.053
rarity=R
type=Enchantment
cost={B}{B}{B}
ability=At the beginning of your upkeep, sacrifice SN unless you pay 2 life.;\
        At the beginning of the end step, destroy all untapped creatures that didn't attack this turn, except for creatures that couldn't attack.
timing=enchantment
oracle=At the beginning of your upkeep, sacrifice Season of the Witch unless you pay 2 life.\nAt the beginning of the end step, destroy all untapped creatures that didn't attack this turn, except for creatures that couldn't attack.
status=not supported: no-info
