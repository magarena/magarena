name=Plated Pegasus
image=https://cards.scryfall.io/border_crop/front/8/7/873fddf3-40e7-41ee-9519-98d0a5268fd1.jpg?1562923541
value=2.848
rarity=U
type=Creature
subtype=Pegasus
cost={2}{W}
pt=1/2
ability=Flash;\
        Flying;\
        If a spell would deal damage to a creature or player, prevent 1 damage that spell would deal to that creature or player.
timing=flash
oracle=Flash\nFlying\nIf a spell would deal damage to a creature or player, prevent 1 damage that spell would deal to that creature or player.
status=needs groovy
