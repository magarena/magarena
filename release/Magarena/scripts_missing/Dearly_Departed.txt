name=Dearly Departed
image=https://cards.scryfall.io/border_crop/front/2/7/27c38500-1eb5-4ea5-8dcd-53bf4e1ee09c.jpg?1637629133
value=3.627
rarity=R
type=Creature
subtype=Spirit
cost={4}{W}{W}
pt=5/5
ability=Flying;\
        As long as SN is in your graveyard, each Human creature you control enters the battlefield with an additional +1/+1 counter on it.
timing=main
oracle=Flying\nAs long as Dearly Departed is in your graveyard, each Human creature you control enters the battlefield with an additional +1/+1 counter on it.
status=not supported: trigger-from-graveyard
