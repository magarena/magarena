name=Golden Guardian
image=https://cards.scryfall.io/border_crop/front/3/9/397ba02d-f347-46f7-b028-dd4ba55faa2f.jpg?1572373698
value=2.500
rarity=R
type=Artifact,Creature
subtype=Golem
cost={4}
pt=4/4
ability=Defender;\
        {2}: SN fights another target creature you control. When SN dies this turn, return it to the battlefield transformed under your control.
transform=Gold-Forge Garrison
timing=smain
oracle=Defender\n{2}: Golden Guardian fights another target creature you control. When Golden Guardian dies this turn, return it to the battlefield transformed under your control.
