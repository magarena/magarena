name=Balduvian Fallen
image=https://cards.scryfall.io/border_crop/front/6/a/6a52b952-6e3b-403b-b355-2af47a282ab6.jpg?1593275042
value=2.690
rarity=U
type=Creature
subtype=Zombie
cost={3}{B}
pt=3/5
ability=Cumulative upkeep {1};\
        Whenever SN's cumulative upkeep is paid, it gets +1/+0 until end of turn for each {B} or {R} spent this way.
timing=main
oracle=Cumulative upkeep {1}\nWhenever Balduvian Fallen's cumulative upkeep is paid, it gets +1/+0 until end of turn for each {B} or {R} spent this way.
status=not supported: mana-spent
