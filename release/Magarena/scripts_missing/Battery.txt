name=Battery
image=https://cards.scryfall.io/border_crop/front/f/e/fe26ff4c-7811-442f-8225-026048920d3f.jpg?1562843378
image_updated=2016-12-13
value=3.628
rarity=U
type=Sorcery
cost={3}{G}
effect=Create a 3/3 green Elephant creature token.
split=Assault
second_half
timing=main
oracle=Create a 3/3 green Elephant creature token.
status=not supported: split-card
