name=Sphinx of Uthuun
image=https://cards.scryfall.io/border_crop/front/5/b/5bd5cd33-20e2-4063-b422-2bde13021888.jpg?1608912186
image_updated=2017-04-12
value=2.500
rarity=R
type=Creature
subtype=Sphinx
cost={5}{U}{U}
pt=5/6
ability=Flying;\
        When SN enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.
timing=main
oracle=Flying\nWhen Sphinx of Uthuun enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.
status=not supported: piles
