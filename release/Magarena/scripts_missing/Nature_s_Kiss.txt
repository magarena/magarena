name=Nature's Kiss
image=https://cards.scryfall.io/border_crop/front/6/4/64b09c44-d463-45a9-9fa2-89407c21200b.jpg?1562801200
value=2.467
rarity=C
type=Enchantment
subtype=Aura
cost={1}{G}
ability=Enchant creature;\
        {1}, Exile the top card of your graveyard: Enchanted creature gets +1/+1 until end of turn.
timing=aura
enchant=default,creature
oracle=Enchant creature\n{1}, Exile the top card of your graveyard: Enchanted creature gets +1/+1 until end of turn.
