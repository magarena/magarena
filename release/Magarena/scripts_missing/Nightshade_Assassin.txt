name=Nightshade Assassin
image=https://cards.scryfall.io/border_crop/front/c/d/cd610675-f8ab-47a7-a4db-c1bfb0e9d5c8.jpg?1619395818
value=3.369
rarity=U
type=Creature
subtype=Human,Assassin
cost={2}{B}{B}
pt=2/1
ability=First strike;\
        When SN enters the battlefield, you may reveal X black cards in your hand. If you do, target creature gets -X/-X until end of turn.;\
        Madness {1}{B}
timing=main
oracle=First strike\nWhen Nightshade Assassin enters the battlefield, you may reveal X black cards in your hand. If you do, target creature gets -X/-X until end of turn.\nMadness {1}{B}
status=not supported: non-mana-x-cost
