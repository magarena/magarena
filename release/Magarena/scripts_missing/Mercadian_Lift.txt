name=Mercadian Lift
image=https://cards.scryfall.io/border_crop/front/3/9/395a1a8a-785f-442b-8e95-8b4ca44af2a3.jpg?1562379931
value=2.538
rarity=R
type=Artifact
cost={2}
ability={1}, {T}: Put a winch counter on SN.;\
        {T}, Remove X winch counters from SN: You may put a creature card with converted mana cost X from your hand onto the battlefield.
timing=artifact
oracle={1}, {T}: Put a winch counter on Mercadian Lift.\n{T}, Remove X winch counters from Mercadian Lift: You may put a creature card with converted mana cost X from your hand onto the battlefield.
status=not supported: non-mana-x-cost
