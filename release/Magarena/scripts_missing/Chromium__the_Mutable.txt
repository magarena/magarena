name=Chromium, the Mutable
image=https://cards.scryfall.io/border_crop/front/5/0/50c1de2c-1acc-47c8-9b5e-a9dae3da8a49.jpg?1562302164
value=2.500
rarity=M
type=Legendary,Creature
subtype=Elder,Dragon
cost={4}{W}{U}{B}
pt=7/7
ability=Flash;\
        This spell can't be countered.;\
        Flying;\
        Discard a card: Until end of turn, SN becomes a Human with base power and toughness 1/1, loses all abilities, and gains hexproof. It can't be blocked this turn.
timing=flash
oracle=Flash\nThis spell can't be countered.\nFlying\nDiscard a card: Until end of turn, Chromium, the Mutable becomes a Human with base power and toughness 1/1, loses all abilities, and gains hexproof. It can't be blocked this turn.
