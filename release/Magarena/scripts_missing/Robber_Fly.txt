name=Robber Fly
image=https://cards.scryfall.io/border_crop/front/7/d/7d5cf073-2ba0-463e-bcd4-979ad18e28fc.jpg?1562381818
value=2.871
rarity=U
type=Creature
subtype=Insect
cost={2}{R}
pt=1/1
ability=Flying;\
        Whenever SN becomes blocked, defending player discards all the cards in his or her hand, then draws that many cards.
timing=main
oracle=Flying\nWhenever Robber Fly becomes blocked, defending player discards all the cards in his or her hand, then draws that many cards.
status=needs groovy
