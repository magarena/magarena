name=Fountain of Cho
image=https://cards.scryfall.io/border_crop/front/4/1/41f352c3-4b63-4174-b2b4-6c19fb8c06ff.jpg?1562380406
value=3.260
rarity=U
type=Land
ability=SN enters the battlefield tapped.;\
        {T}: Put a storage counter on SN.;\
        {T}, Remove any number of storage counters from SN: Add {W} for each storage counter removed this way.
timing=land
oracle=Fountain of Cho enters the battlefield tapped.\n{T}: Put a storage counter on Fountain of Cho.\n{T}, Remove any number of storage counters from Fountain of Cho: Add {W} for each storage counter removed this way.
status=not supported: add-multiple-mana
