name=Teferi, Mage of Zhalfir
image=https://cards.scryfall.io/border_crop/front/8/f/8fc5e4a8-ea7b-4803-a7ed-3b915708661f.jpg?1619394801
image_updated=2017-04-12
value=4.123
rarity=R
type=Legendary,Creature
subtype=Human,Wizard
cost={2}{U}{U}{U}
pt=3/4
ability=Flash;\
        Creature cards you own that aren't on the battlefield have flash.;\
        Each opponent can cast spells only any time he or she could cast a sorcery.
timing=flash
oracle=Flash\nCreature cards you own that aren't on the battlefield have flash.\nEach opponent can cast spells only any time he or she could cast a sorcery.
status=not supported: card-modification
