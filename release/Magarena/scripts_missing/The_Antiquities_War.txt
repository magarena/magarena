name=The Antiquities War
image=https://cards.scryfall.io/border_crop/front/b/b/bbda670a-00a7-419c-b4b5-bfdb323f006d.jpg?1562741999
value=2.500
rarity=R
type=Enchantment
subtype=Saga
cost={3}{U}
ability=I, II — Look at the top five cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in a random order.;\
        III — Artifacts you control become artifact creatures with base power and toughness 5/5 until end of turn.
timing=enchantment
oracle=I, II — Look at the top five cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in a random order.\nIII — Artifacts you control become artifact creatures with base power and toughness 5/5 until end of turn.
