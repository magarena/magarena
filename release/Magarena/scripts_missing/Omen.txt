name=Omen
image=https://cards.scryfall.io/border_crop/front/6/b/6be1956e-c43e-4a6c-950a-5be6e6ca013f.jpg?1562446859
value=2.314
rarity=C
type=Sorcery
cost={1}{U}
effect=Look at the top three cards of your library, then put them back in any order. You may shuffle your library.~Draw a card.
timing=main
oracle=Look at the top three cards of your library, then put them back in any order. You may shuffle your library.\nDraw a card.
status=not supported: ordering-lib-top
