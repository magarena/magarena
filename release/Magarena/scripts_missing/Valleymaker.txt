name=Valleymaker
image=https://cards.scryfall.io/border_crop/front/5/0/50b4fa59-8315-4a66-b5fb-f78daa92aca7.jpg?1562830092
value=3.015
rarity=R
type=Creature
subtype=Giant,Shaman
cost={5}{R/G}
pt=5/5
ability={T}, Sacrifice a Mountain: SN deals 3 damage to target creature.;\
        {T}, Sacrifice a Forest: Choose a player. That player adds {G}{G}{G}.
timing=main
oracle={T}, Sacrifice a Mountain: Valleymaker deals 3 damage.
status=not supported: add-multiple-mana
