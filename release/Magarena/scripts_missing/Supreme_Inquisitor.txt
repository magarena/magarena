name=Supreme Inquisitor
image=https://cards.scryfall.io/border_crop/front/8/6/867de3d2-2178-4931-823e-ff439e1a45ea.jpg?1562926695
value=4.034
rarity=R
type=Creature
subtype=Human,Wizard
cost={3}{U}{U}
pt=1/3
ability=Tap five untapped Wizards you control: Search target player's library for up to five cards and exile them. Then that player shuffles his or her library.
timing=main
oracle=Tap five untapped Wizards you control: Search target player's library for up to five cards and exile them. Then that player shuffles his or her library.
