name=Pilgrim of Virtue
image=https://cards.scryfall.io/border_crop/front/b/e/be58f0dd-3856-4960-83de-06eed78ed703.jpg?1562930545
value=2.327
rarity=C
type=Creature
subtype=Human,Cleric
cost={2}{W}
pt=1/3
ability=Protection from black;\
        {W}, Sacrifice SN: The next time a black source of your choice would deal damage this turn, prevent that damage.
timing=main
oracle=Protection from black\n{W}, Sacrifice Pilgrim of Virtue: The next time a black source of your choice would deal damage this turn, prevent that damage.
status=not supported: source-deal-damage
