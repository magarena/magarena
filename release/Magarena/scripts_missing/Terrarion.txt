name=Terrarion
image=https://cards.scryfall.io/border_crop/front/4/8/48eda056-e00f-4e28-ad26-9150a4704d21.jpg?1601081087
image_updated=2016-09-09
value=3.318
rarity=C
type=Artifact
cost={1}
ability=SN enters the battlefield tapped.;\
        {2}, {T}, Sacrifice SN: Add two mana in any combination of colors.;\
        When SN is put into a graveyard from the battlefield, draw a card.
timing=artifact
oracle=Terrarion enters the battlefield tapped.\n{2}, {T}, Sacrifice Terrarion: Add two mana in any combination of colors.\nWhen Terrarion is put into a graveyard from the battlefield, draw a card.
status=not supported: add-multiple-mana
