name=Imprisoned in the Moon
image=https://cards.scryfall.io/border_crop/front/8/1/8181f54d-4515-43c6-8d08-b23a9e4199cc.jpg?1682208779
image_updated=2016-09-05
value=2.500
rarity=R
type=Enchantment
subtype=Aura
cost={2}{U}
ability=Enchant creature, land, or planeswalker;\
        Enchanted permanent is a colorless land with "{T}: Add {C}" and loses all other card types and abilities.
timing=aura
enchant=default,creature
oracle=Enchant creature, land, or planeswalker\nEnchanted permanent is a colorless land with "{T}: Add {C}" and loses all other card types and abilities.
status=needs groovy
