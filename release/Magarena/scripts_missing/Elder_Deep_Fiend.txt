name=Elder Deep-Fiend
image=https://cards.scryfall.io/border_crop/front/4/c/4cffed4c-4e2b-414a-9b20-90ce21b47d16.jpg?1610074944
image_updated=2016-09-05
value=2.500
rarity=R
type=Creature
subtype=Eldrazi,Octopus
cost={8}
pt=5/6
ability=Flash;\
        Emerge {5}{U}{U};\
        When you cast SN, tap up to four target permanents.
timing=flash
oracle=Flash\nEmerge {5}{U}{U}\nWhen you cast Elder Deep-Fiend, tap up to four target permanents.
status=not supported: multiple-targets
