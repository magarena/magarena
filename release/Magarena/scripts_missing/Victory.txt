name=Victory
image=https://cards.scryfall.io/border_crop/front/5/1/517b32e4-4b34-431f-8f3b-98a6cffc245a.jpg?1549941725
value=2.500
rarity=U
type=Sorcery
cost={2}{W}
split=Onward
ability=Aftermath
effect=Target creature gains double strike until end of turn.
timing=main
second_half
oracle=Aftermath\nTarget creature gains double strike until end of turn.
status=not supported: split-card
