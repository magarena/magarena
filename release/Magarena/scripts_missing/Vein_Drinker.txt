name=Vein Drinker
image=https://cards.scryfall.io/border_crop/front/2/5/25ad3372-51cb-4b82-a23c-81938b7c01b6.jpg?1562602686
image_updated=2017-10-03
value=3.313
rarity=R
type=Creature
subtype=Vampire
cost={4}{B}{B}
pt=4/4
ability=Flying;\
        {R}, {T}: SN deals damage equal to its power to target creature. That creature deals damage equal to its power to SN.;\
        Whenever a creature dealt damage by SN this turn dies, put a +1/+1 counter on SN.
timing=main
oracle=Flying\n{R}, {T}: Vein Drinker deals damage equal to its power to target creature. That creature deals damage equal to its power to Vein Drinker.\nWhenever a creature dealt damage by Vein Drinker this turn dies, put a +1/+1 counter on Vein Drinker.
status=not supported: damage-by-SN-dies
