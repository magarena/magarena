name=Gauntlet of Might
image=https://cards.scryfall.io/border_crop/front/d/f/df3bc33f-23f8-4eb4-a70e-b8b7af5b40f6.jpg?1562948147
value=4.543
rarity=R
type=Artifact
cost={4}
ability=Red creatures get +1/+1.;\
        Whenever a Mountain is tapped for mana, its controller adds {R}.
timing=artifact
oracle=Red creatures get +1/+1.\nWhenever a Mountain is tapped for mana, its controller adds {R}.
status=not supported: non-activated-mana-ability
