name=Hushwing Gryff
image=https://cards.scryfall.io/border_crop/front/6/0/604b8ad4-b606-4be2-b3de-ad9f24b01eac.jpg?1562402533
image_updated=2017-08-17
value=3.812
rarity=R
type=Creature
subtype=Hippogriff
cost={2}{W}
pt=2/1
ability=Flash;\
        Flying;\
        Creatures entering the battlefield don't cause abilities to trigger.
timing=flash
oracle=Flash\nFlying\nCreatures entering the battlefield don't cause abilities to trigger.
status=not supported: permanents-cant
