name=Uncage the Menagerie
image=https://cards.scryfall.io/border_crop/front/b/c/bc4aa918-53b9-4177-a859-f5a8eff09fe5.jpg?1562812515
value=2.500
rarity=M
type=Sorcery
cost={X}{G}{G}
effect=Search your library for up to X creature cards with different names that each have converted mana cost X, reveal them, put them into your hand, then shuffle your library.
timing=main
oracle=Search your library for up to X creature cards with different names that each have converted mana cost X, reveal them, put them into your hand, then shuffle your library.
