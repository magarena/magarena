name=Armageddon Clock
image=https://cards.scryfall.io/border_crop/front/a/b/ab55bb84-03c2-4989-8db4-0d5578ea0431.jpg?1562935409
value=3.889
rarity=R
type=Artifact
cost={6}
ability=At the beginning of your upkeep, put a doom counter on SN.;\
        At the beginning of your draw step, SN deals damage equal to the number of doom counters on it to each player.;\
        {4}: Remove a doom counter from SN. Any player may activate this ability but only during any upkeep step.
timing=artifact
oracle=At the beginning of your upkeep, put a doom counter on Armageddon Clock.\nAt the beginning of your draw step, Armageddon Clock deals damage equal to the number of doom counters on it to each player.\n{4}: Remove a doom counter from Armageddon Clock. Any player may activate this ability but only during any upkeep step.
status=not supported: any-player-activate
