name=Wall of Shields
image=https://cards.scryfall.io/border_crop/front/6/3/6376c7c4-aaca-4625-83d4-a49f01aec535.jpg?1587912176
value=2.462
rarity=U
type=Artifact,Creature
subtype=Wall
cost={3}
pt=0/4
ability=Defender;\
        Banding
timing=smain
oracle=Defender\nBanding
status=not supported: banding
