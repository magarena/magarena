name=Soul Scourge
image=https://cards.scryfall.io/border_crop/front/4/a/4aa7d91b-cfbb-4da0-97fa-3c0a15f1dbb9.jpg?1562629621
value=2.257
rarity=C
type=Creature
subtype=Nightmare,Horror
cost={4}{B}
pt=3/2
ability=Flying;\
        When SN enters the battlefield, target player loses 3 life.;\
        When SN leaves the battlefield, that player gains 3 life.
timing=main
oracle=Flying\nWhen Soul Scourge enters the battlefield, target player loses 3 life.\nWhen Soul Scourge leaves the battlefield, that player gains 3 life.
status=needs groovy
