name=Kavu Primarch
image=https://cards.scryfall.io/border_crop/front/4/7/475c3439-74a9-4a53-9e8b-c1c84b60ea35.jpg?1619398222
value=2.500
rarity=C
type=Creature
subtype=Kavu
cost={3}{G}
pt=3/3
ability=Kicker {4};\
        Convoke;\
        If SN was kicked, it enters the battlefield with four +1/+1 counters on it.
timing=main
oracle=Kicker {4}\nConvoke\nIf Kavu Primarch was kicked, it enters the battlefield with four +1/+1 counters on it.
status=not supported: convoke
