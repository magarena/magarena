name=Jandor's Ring
image=https://cards.scryfall.io/border_crop/front/a/f/af9988fd-72a0-42dc-9f10-85edd3133977.jpg?1559592701
value=0.952
rarity=R
type=Artifact
cost={6}
ability={2}, {T}, Discard the last card you drew this turn: Draw a card.
timing=artifact
oracle={2}, {T}, Discard the last card you drew this turn: Draw a card.
status=not supported: no-info
