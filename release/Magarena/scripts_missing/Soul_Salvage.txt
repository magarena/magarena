name=Soul Salvage
image=https://cards.scryfall.io/border_crop/front/d/b/db574719-746a-433e-a5be-06d232a01021.jpg?1600701577
value=2.500
rarity=C
type=Sorcery
cost={2}{B}
effect=Return up to two target creature cards from your graveyard to your hand.
timing=main
oracle=Return up to two target creature cards from your graveyard to your hand.
