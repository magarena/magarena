name=Aggressive Mining
image=https://cards.scryfall.io/border_crop/front/e/1/e15a3f1a-83af-4541-8d1c-bf7844f969c9.jpg?1562795703
value=2.636
rarity=R
type=Enchantment
cost={3}{R}
ability=You can't play lands.;\
        Sacrifice a land: Draw two cards. Activate this ability only once each turn.
timing=enchantment
oracle=You can't play lands.\nSacrifice a land: Draw two cards. Activate this ability only once each turn.
status=not supported: players-cant
