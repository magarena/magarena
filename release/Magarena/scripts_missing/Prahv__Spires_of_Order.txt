name=Prahv, Spires of Order
image=https://cards.scryfall.io/border_crop/front/2/a/2a315f63-96ad-4a5f-8eb4-d81361797348.jpg?1593274115
value=2.978
rarity=U
type=Land
ability={T}: Add {C}.;\
        {4}{W}{U}, {T}: Prevent all damage a source of your choice would deal this turn.
timing=land
oracle={T}: Add {C}.\n{4}{W}{U}, {T}: Prevent all damage a source of your choice would deal this turn.
status=not supported: source-deal-damage
