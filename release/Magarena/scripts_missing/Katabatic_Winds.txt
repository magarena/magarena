name=Katabatic Winds
image=https://cards.scryfall.io/border_crop/front/9/7/97b34ce8-1eb2-44eb-813a-09d0308e27a0.jpg?1562278154
value=3.788
rarity=R
type=Enchantment
cost={2}{G}
ability=Phasing;\
        Creatures with flying can't attack or block, and their activated abilities with {T} in their costs can't be activated.
timing=enchantment
oracle=Phasing\nCreatures with flying can't attack or block, and their activated abilities with {T} in their costs can't be activated.
status=not supported: phasing
