name=Smuggler Captain
image=https://cards.scryfall.io/border_crop/front/b/1/b10ce284-c994-429c-ad61-ffe1ca88d691.jpg?1576381870
image_updated=2016-09-05
value=2.500
rarity=U
type=Creature
subtype=Human,Pirate
cost={3}{B}
pt=2/2
ability=Draft SN face up.;\
        As you draft a card, you may reveal it, note its name, then turn SN face down.;\
        When SN enters the battlefield, you may search your library for a card with a name you noted for cards named Smuggler Captain, reveal it, put it into your hand, then shuffle your library.
timing=main
oracle=Draft Smuggler Captain face up.\nAs you draft a card, you may reveal it, note its name, then turn Smuggler Captain face down.\nWhen Smuggler Captain enters the battlefield, you may search your library for a card with a name you noted for cards named Smuggler Captain, reveal it, put it into your hand, then shuffle your library.
status=not supported: draft
