name=Guardian of Tazeem
image=https://cards.scryfall.io/border_crop/front/5/b/5b55cf5e-4499-4e3a-9748-7b1568286b5a.jpg?1562916528
image_updated=2015-11-08
value=2.500
rarity=R
type=Creature
subtype=Sphinx
cost={3}{U}{U}
pt=4/5
ability=Flying;\
        Whenever a land enters the battlefield under your control, tap target creature an opponent controls. If that land is an Island, that creature doesn't untap during its controller's next untap step.
timing=main
oracle=Flying\nLandfall — Whenever a land enters the battlefield under your control, tap target creature an opponent controls. If that land is an Island, that creature doesn't untap during its controller's next untap step.
status=needs groovy
