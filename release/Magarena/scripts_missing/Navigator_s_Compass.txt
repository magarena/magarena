name=Navigator's Compass
image=https://cards.scryfall.io/border_crop/front/6/a/6a283135-7a51-4cf7-82a6-7e50894e64a5.jpg?1562737167
value=2.500
rarity=C
type=Artifact
cost={1}
ability=When SN enters the battlefield, you gain 3 life.;\
        {T}: Until end of turn, target land you control becomes the basic land type of your choice in addition to its other types.
timing=artifact
oracle=When Navigator's Compass enters the battlefield, you gain 3 life.\n{T}: Until end of turn, target land you control becomes the basic land type of your choice in addition to its other types.
