name=Eldrazi Temple
image=https://cards.scryfall.io/border_crop/front/c/4/c42049e2-fe4e-4fe9-9599-faf1af50b97d.jpg?1593095918
value=2.500
rarity=U
type=Land
ability={T}: Add {C}.;\
        {T}: Add {C}{C}. Spend this mana only to cast colorless Eldrazi spells or activate abilities of colorless Eldrazi.
timing=land
oracle={T}: Add {C}.\n{T}: Add {C}{C}. Spend this mana only to cast colorless Eldrazi spells or activate abilities of colorless Eldrazi.
status=not supported: add-multiple-mana
