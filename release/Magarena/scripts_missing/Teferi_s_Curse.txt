name=Teferi's Curse
image=https://cards.scryfall.io/border_crop/front/f/b/fb813ef5-8441-4024-a585-1ea24145e1bd.jpg?1562722870
value=3.050
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}
ability=Enchant artifact or creature;\
        Enchanted permanent has phasing.
timing=aura
enchant=default,artifact or creature
oracle=Enchant artifact or creature\nEnchanted permanent has phasing.
status=not supported: phasing
