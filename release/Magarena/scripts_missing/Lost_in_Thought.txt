name=Lost in Thought
image=https://cards.scryfall.io/border_crop/front/f/5/f5fb391a-2687-461d-b5ef-a494287ddb5d.jpg?1562632720
value=3.056
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}
ability=Enchant creature;\
        Enchanted creature can't attack or block, and its activated abilities can't be activated. Its controller may exile three cards from his or her graveyard for that player to ignore this effect until end of turn.
timing=aura
enchant=default,creature
oracle=Enchant creature\nEnchanted creature can't attack or block, and its activated abilities can't be activated. Its controller may exile three cards from his or her graveyard for that player to ignore this effect until end of turn.
status=not supported: ignore-effect
