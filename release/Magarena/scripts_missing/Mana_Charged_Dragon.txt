name=Mana-Charged Dragon
image=https://cards.scryfall.io/border_crop/front/0/3/03245712-18b9-48d2-b95e-24a38367abb9.jpg?1592672811
value=3.800
rarity=R
type=Creature
subtype=Dragon
cost={4}{R}{R}
pt=5/5
ability=Flying, trample;\
        Whenever SN attacks or blocks, each player starting with you may pay any amount of mana. SN gets +X/+0 until end of turn, where X is the total amount of mana paid this way.
timing=main
oracle=Flying, trample\nJoin forces — Whenever Mana-Charged Dragon attacks or blocks, each player starting with you may pay any amount of mana. Mana-Charged Dragon gets +X/+0 until end of turn, where X is the total amount of mana paid this way.
status=needs groovy
