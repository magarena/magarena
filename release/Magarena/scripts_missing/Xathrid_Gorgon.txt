name=Xathrid Gorgon
image=https://cards.scryfall.io/border_crop/front/e/0/e07524e0-303d-465d-b112-ca605b9b27fc.jpg?1562561034
value=3.803
rarity=R
type=Creature
subtype=Gorgon
cost={5}{B}
pt=3/6
ability=Deathtouch;\
        {2}{B}, {T}: Put a petrification counter on target creature. It gains defender and becomes a colorless artifact in addition to its other types. Its activated abilities can't be activated.
timing=main
oracle=Deathtouch\n{2}{B}, {T}: Put a petrification counter on target creature. It gains defender and becomes a colorless artifact in addition to its other types. Its activated abilities can't be activated.
status=needs groovy
