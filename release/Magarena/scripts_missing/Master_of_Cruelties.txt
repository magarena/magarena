name=Master of Cruelties
image=https://cards.scryfall.io/border_crop/front/2/5/25266b22-3cee-4ac8-91e8-5a23fbaa457a.jpg?1673148945
value=3.858
rarity=M
type=Creature
subtype=Demon
cost={3}{B}{R}
pt=1/4
ability=First strike, deathtouch;\
        SN can only attack alone.;\
        Whenever SN attacks a player and isn't blocked, that player's life total becomes 1. SN assigns no combat damage this combat.
timing=main
oracle=First strike, deathtouch\nMaster of Cruelties can only attack alone.\nWhenever Master of Cruelties attacks a player and isn't blocked, that player's life total becomes 1. Master of Cruelties assigns no combat damage this combat.
status=not supported: attacking-restriction
