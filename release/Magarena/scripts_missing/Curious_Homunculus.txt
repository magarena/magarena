name=Curious Homunculus
image=https://cards.scryfall.io/border_crop/front/2/2/22e816af-df55-4a3f-a6e7-0ff3bb1b45b5.jpg?1576384071
image_updated=2016-09-05
value=2.500
rarity=U
type=Creature
subtype=Homunculus
cost={1}{U}
pt=1/1
ability={T}: Add {C}. Spend this mana only to cast an instant or sorcery spell.;\
        At the beginning of your upkeep, if there are three or more instant and/or sorcery cards in your graveyard, transform SN.
transform=Voracious Reader
timing=main
oracle={T}: Add {C}. Spend this mana only to cast an instant or sorcery spell.\nAt the beginning of your upkeep, if there are three or more instant and/or sorcery cards in your graveyard, transform Curious Homunculus.
status=not supported: mana-spent
