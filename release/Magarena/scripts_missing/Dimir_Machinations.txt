name=Dimir Machinations
image=https://cards.scryfall.io/border_crop/front/1/4/14bfd72a-78c1-4167-89bf-ea1fccccd5b1.jpg?1598914791
value=3.807
rarity=U
type=Sorcery
cost={2}{B}
ability=Transmute {1}{B}{B}
effect=Look at the top three cards of target player's library. Exile any number of those cards, then put the rest back in any order.
timing=main
oracle=Look at the top three cards of target player's library. Exile any number of those cards, then put the rest back in any order.\nTransmute {1}{B}{B}
status=not supported: ordering-lib-top
