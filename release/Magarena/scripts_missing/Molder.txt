name=Molder
image=https://cards.scryfall.io/border_crop/front/8/c/8c0eb32c-89b1-40be-a77c-62c114a73ccc.jpg?1562924615
value=3.128
rarity=C
type=Instant
cost={X}{G}
effect=Destroy target artifact or enchantment with converted mana cost X. It can't be regenerated. You gain X life.
timing=removal
oracle=Destroy target artifact or enchantment with converted mana cost X. It can't be regenerated. You gain X life.
status=not supported: x-cost-as-target
