name=Purgatory
image=https://cards.scryfall.io/border_crop/front/9/e/9ed5e58b-46f3-437c-89ac-24235a65bd1f.jpg?1562720941
value=3.968
rarity=R
type=Enchantment
cost={2}{W}{B}
ability=Whenever a nontoken creature is put into your graveyard from the battlefield, exile that card.;\
        At the beginning of your upkeep, you may pay {4} and 2 life. If you do, return a card exiled with SN to the battlefield.
timing=enchantment
oracle=Whenever a nontoken creature is put into your graveyard from the battlefield, exile that card.\nAt the beginning of your upkeep, you may pay {4} and 2 life. If you do, return a card exiled with Purgatory to the battlefield.
status=needs groovy
