name=Storm Cauldron
image=https://cards.scryfall.io/border_crop/front/0/b/0bb5bdd3-6ecd-49cd-bfa2-e7da1ee85d88.jpg?1562232316
value=3.802
rarity=R
type=Artifact
cost={5}
ability=Each player may play an additional land during each of his or her turns.;\
        Whenever a land is tapped for mana, return it to its owner's hand.
timing=artifact
oracle=Each player may play an additional land during each of his or her turns.\nWhenever a land is tapped for mana, return it to its owner's hand.
status=not supported: when-tap-for-mana
