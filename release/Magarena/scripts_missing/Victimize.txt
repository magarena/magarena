name=Victimize
image=https://cards.scryfall.io/border_crop/front/5/4/54b5c19e-f50a-4cd1-bf42-e545da7a6b0f.jpg?1682209249
value=2.500
rarity=U
type=Sorcery
cost={2}{B}
effect=Choose two target creature cards in your graveyard. Sacrifice a creature. If you do, return the chosen cards to the battlefield tapped.
timing=main
oracle=Choose two target creature cards in your graveyard. Sacrifice a creature. If you do, return the chosen cards to the battlefield tapped.
status=not supported: multiple-targets
