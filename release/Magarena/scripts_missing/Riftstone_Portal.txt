name=Riftstone Portal
image=https://cards.scryfall.io/border_crop/front/9/2/92ece630-e484-4221-911f-e32048894f23.jpg?1562630998
value=4.136
rarity=U
type=Land
ability={T}: Add {C}.;\
        As long as SN is in your graveyard, lands you control have "{T}: Add {G} or {W}."
timing=land
oracle={T}: Add {C}.\nAs long as Riftstone Portal is in your graveyard, lands you control have "{T}: Add {G} or {W}."
status=not supported: static-from-graveyard
