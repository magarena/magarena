name=Secluded Glen
image=https://cards.scryfall.io/border_crop/front/9/e/9e4afa65-7933-4a64-b50f-a9a9f832b112.jpg?1562360339
value=4.096
rarity=R
type=Land
ability=As SN enters the battlefield, you may reveal a Faerie card from your hand. If you don't, SN enters the battlefield tapped.;\
        {T}: Add {U} or {B}.
timing=land
oracle=As Secluded Glen enters the battlefield, you may reveal a Faerie card from your hand. If you don't, Secluded Glen enters the battlefield tapped.\n{T}: Add {U} or {B}.
status=not supported: reveal-cost
