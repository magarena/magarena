name=Leeching Licid
image=https://cards.scryfall.io/border_crop/front/2/7/27bffefb-23c0-4d03-b716-b1a7eff39a05.jpg?1562053262
value=2.621
rarity=U
type=Creature
subtype=Licid
cost={1}{B}
pt=1/1
ability={B}, {T}: SN loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {B} to end this effect.;\
        At the beginning of the upkeep of enchanted creature's controller, SN deals 1 damage to that player.
timing=main
oracle={B}, {T}: Leeching Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {B} to end this effect.\nAt the beginning of the upkeep of enchanted creature's controller, Leeching Licid deals 1 damage to that player.
status=not supported: end-effect
