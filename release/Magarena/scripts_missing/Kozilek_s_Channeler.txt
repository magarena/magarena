name=Kozilek's Channeler
image=https://cards.scryfall.io/border_crop/front/c/5/c550d179-32ec-4ad8-91c2-d79320a21cba.jpg?1562941588
image_updated=2015-11-08
value=2.500
rarity=C
type=Creature
subtype=Eldrazi
cost={5}
pt=4/4
ability={T}: Add {C}{C}.
timing=main
oracle={T}: Add {C}{C}.
status=not supported: add-multiple-mana
