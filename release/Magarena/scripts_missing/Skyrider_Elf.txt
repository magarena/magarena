name=Skyrider Elf
image=https://cards.scryfall.io/border_crop/front/4/7/477eb787-aec7-43fd-9813-184111d9dcc5.jpg?1562911739
image_updated=2015-11-08
value=2.500
rarity=U
type=Creature
subtype=Elf,Warrior,Ally
cost={X}{G}{U}
pt=0/0
ability=Flying;\
        SN enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.
timing=main
oracle=Flying\nConverge — Skyrider Elf enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.
status=not supported: mana-spent
