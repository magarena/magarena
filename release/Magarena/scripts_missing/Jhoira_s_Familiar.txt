name=Jhoira's Familiar
image=https://cards.scryfall.io/border_crop/front/7/b/7b89a074-9aca-4f7f-953a-b401199be1cb.jpg?1599709387
value=2.500
rarity=U
type=Artifact,Creature
subtype=Bird
cost={4}
pt=2/2
ability=Flying;\
        Historic spells you cast cost {1} less to cast.
timing=main
oracle=Flying\nHistoric spells you cast cost {1} less to cast.
