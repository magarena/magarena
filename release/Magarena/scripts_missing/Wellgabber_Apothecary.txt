name=Wellgabber Apothecary
image=https://cards.scryfall.io/border_crop/front/1/2/129933ba-60a0-4764-a705-28fa7bb10bd3.jpg?1562338707
value=2.000
rarity=C
type=Creature
subtype=Merfolk,Cleric
cost={4}{W}
pt=2/3
ability={1}{W}: Prevent all damage that would be dealt to target tapped Merfolk or Kithkin creature this turn.
timing=main
oracle={1}{W}: Prevent all damage that would be dealt to target tapped Merfolk or Kithkin creature this turn.
status=needs groovy
