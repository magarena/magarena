name=Secrets of the Dead
image=https://cards.scryfall.io/border_crop/front/0/6/066311f5-51fc-4da9-a5fa-fa5059a88e5b.jpg?1568004066
value=3.339
rarity=U
type=Enchantment
cost={2}{U}
ability=Whenever you cast a spell from your graveyard, draw a card.
timing=enchantment
oracle=Whenever you cast a spell from your graveyard, draw a card.
status=needs groovy
