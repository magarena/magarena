name=Draconic Roar
image=https://cards.scryfall.io/border_crop/front/d/8/d8a0ec06-7c48-4334-ac50-c249e7e91dbe.jpg?1601077491
image_updated=2017-04-12
value=2.500
rarity=C
type=Instant
cost={1}{R}
ability=As an additional cost to cast SN, you may reveal a Dragon card from your hand.
effect=SN deals 3 damage to target creature. If you revealed a Dragon card or controlled a Dragon as you cast SN, SN deals 3 damage to that creature's controller.
timing=removal
oracle=As an additional cost to cast Draconic Roar, you may reveal a Dragon card from your hand.\nDraconic Roar deals 3 damage to target creature. If you revealed a Dragon card or controlled a Dragon as you cast Draconic Roar, Draconic Roar deals 3 damage to that creature's controller.
status=not supported: reveal-cost
