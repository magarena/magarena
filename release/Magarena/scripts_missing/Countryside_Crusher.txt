name=Countryside Crusher
image=https://cards.scryfall.io/border_crop/front/5/4/54582b89-a152-4686-8172-82db740dfea2.jpg?1561967345
value=4.013
rarity=R
type=Creature
subtype=Giant,Warrior
cost={1}{R}{R}
pt=3/3
ability=At the beginning of your upkeep, reveal the top card of your library. If it's a land card, put it into your graveyard and repeat this process.;\
        Whenever a land card is put into your graveyard from anywhere, put a +1/+1 counter on SN.
timing=main
oracle=At the beginning of your upkeep, reveal the top card of your library. If it's a land card, put it into your graveyard and repeat this process.\nWhenever a land card is put into your graveyard from anywhere, put a +1/+1 counter on Countryside Crusher.
status=needs groovy
