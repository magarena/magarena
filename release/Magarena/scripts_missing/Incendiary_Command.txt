name=Incendiary Command
image=https://cards.scryfall.io/border_crop/front/9/2/925d6ae9-deb8-41a4-99de-fd6fc6924d3b.jpg?1562925920
value=3.615
rarity=R
type=Sorcery
cost={3}{R}{R}
effect=Choose two — (1) SN deals 4 damage to target player. (2) SN deals 2 damage to each creature. (3) Destroy target nonbasic land. (4) Each player discards all the cards in his or her hand, then draws that many cards.
timing=main
oracle=Choose two —\n• Incendiary Command deals 4 damage to target player.\n• Incendiary Command deals 2 damage to each creature.\n• Destroy target nonbasic land.\n• Each player discards all the cards in his or her hand, then draws that many cards.
status=not supported: multi-choice-modal
