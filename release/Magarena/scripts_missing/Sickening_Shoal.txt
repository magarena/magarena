name=Sickening Shoal
image=https://cards.scryfall.io/border_crop/front/d/9/d92f4129-19fc-4ee9-9e3d-77fcf1563e4b.jpg?1562880104
value=4.304
rarity=R
type=Instant
subtype=Arcane
cost={X}{B}{B}
ability=You may exile a black card with converted mana cost X from your hand rather than pay this spell's mana cost.
effect=Target creature gets -X/-X until end of turn.
timing=removal
oracle=You may exile a black card with converted mana cost X from your hand rather than pay this spell's mana cost.\nTarget creature gets -X/-X until end of turn.
status=needs groovy
