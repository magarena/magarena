name=Elephant Grass
image=https://cards.scryfall.io/border_crop/front/f/4/f4c1f5a7-0d28-43ab-9b66-937e963f42cd.jpg?1587912980
value=4.048
rarity=U
type=Enchantment
cost={G}
ability=Cumulative upkeep {1};\
        Black creatures can't attack you.;\
        Nonblack creatures can't attack you unless their controller pays {2} for each creature he or she controls that's attacking you.
timing=enchantment
oracle=Cumulative upkeep {1}\nBlack creatures can't attack you.\nNonblack creatures can't attack you unless their controller pays {2} for each creature he or she controls that's attacking you.
status=not supported: cant-attack-unless-cost
