name=Pyrewild Shaman
image=https://cards.scryfall.io/border_crop/front/2/f/2f195c52-4018-4d57-9c62-5f0a10f7336c.jpg?1599706584
image_updated=2017-08-08
value=3.718
rarity=U
type=Creature
subtype=Goblin,Shaman
cost={2}{R}
pt=3/1
ability={1}{R}, Discard SN: Target attacking creature gets +3/+1 until end of turn.;\
        Whenever one or more creatures you control deal combat damage to a player, if SN is in your graveyard, you may pay {3}. If you do, return SN to your hand.
timing=main
oracle=Bloodrush — {1}{R}, Discard Pyrewild Shaman: Target attacking creature gets +3/+1 until end of turn.\nWhenever one or more creatures you control deal combat damage to a player, if Pyrewild Shaman is in your graveyard, you may pay {3}. If you do, return Pyrewild Shaman to your hand.
status=not supported: trigger-from-graveyard
