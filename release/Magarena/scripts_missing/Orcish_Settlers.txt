name=Orcish Settlers
image=https://cards.scryfall.io/border_crop/front/d/5/d54764f6-6f65-405c-ba30-1e485ce3fe21.jpg?1562803564
value=4.300
rarity=U
type=Creature
subtype=Orc
cost={1}{R}
pt=1/1
ability={X}{X}{R}, {T}, Sacrifice SN: Destroy X target lands.
timing=main
oracle={X}{X}{R}, {T}, Sacrifice Orcish Settlers: Destroy X target lands.
status=not supported: multiple-targets
