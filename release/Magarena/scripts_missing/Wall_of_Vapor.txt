name=Wall of Vapor
image=https://cards.scryfall.io/border_crop/front/3/0/309c1b2a-0230-4b66-84a0-32b8cd6d31eb.jpg?1587912489
image_updated=2017-01-11
value=2.302
rarity=C
type=Creature
subtype=Wall
cost={3}{U}
pt=0/1
ability=Defender;\
        Prevent all damage that would be dealt to SN by creatures it's blocking.
timing=smain
oracle=Defender\nPrevent all damage that would be dealt to Wall of Vapor by creatures it's blocking.
status=needs groovy
