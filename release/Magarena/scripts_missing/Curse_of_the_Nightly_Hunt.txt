name=Curse of the Nightly Hunt
image=https://cards.scryfall.io/border_crop/front/2/9/2983c017-1697-4d45-ac58-3f3ccbaaefff.jpg?1562272943
image_updated=2016-02-29
value=3.106
rarity=U
type=Enchantment
subtype=Aura,Curse
cost={2}{R}
ability=Enchant player;\
        Creatures enchanted player controls attack each combat if able.
timing=aura
enchant=default,player
oracle=Enchant player\nCreatures enchanted player controls attack each combat if able.
status=not supported: enchant-player
