name=Storm Elemental
image=https://cards.scryfall.io/border_crop/front/2/9/2903c66e-056d-4ab6-9dfe-81b18a8da454.jpg?1562868059
value=2.014
rarity=U
type=Creature
subtype=Elemental
cost={5}{U}
pt=3/4
ability=Flying;\
        {U}, Exile the top card of your library: Tap target creature with flying.;\
        {U}, Exile the top card of your library: If the exiled card is a snow land, SN gets +1/+1 until end of turn.
timing=main
oracle=Flying\n{U}, Exile the top card of your library: Tap target creature with flying.\n{U}, Exile the top card of your library: If the exiled card is a snow land, Storm Elemental gets +1/+1 until end of turn.
