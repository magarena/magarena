name=Cabal Therapy
image=https://cards.scryfall.io/border_crop/front/2/f/2f5b12f0-986f-43d0-a81b-64111e7f17e6.jpg?1580014199
value=4.542
rarity=U
type=Sorcery
cost={B}
ability=Flashback—Sacrifice a creature.
effect=Choose a nonland card name. Target player reveals his or her hand and discards all cards with that name.
timing=main
oracle=Choose a nonland card name. Target player reveals his or her hand and discards all cards with that name.\nFlashback—Sacrifice a creature.
status=not supported: name-card
