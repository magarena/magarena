name=Golgari Decoy
image=https://cards.scryfall.io/border_crop/front/5/1/511a42a8-71ce-476f-98fa-fc0dc822edcf.jpg?1562786246
value=3.074
rarity=U
type=Creature
subtype=Elf,Rogue
cost={3}{G}
pt=2/2
ability=All creatures able to block SN do so.;\
        Scavenge {3}{G}{G}
timing=main
oracle=All creatures able to block Golgari Decoy do so.\nScavenge {3}{G}{G}
status=not supported: blocking-restriction
