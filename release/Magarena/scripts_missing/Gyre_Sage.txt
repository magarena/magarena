name=Gyre Sage
image=https://cards.scryfall.io/border_crop/front/b/e/be573ae7-967a-4b98-9058-ec3ff82ff63f.jpg?1682209516
value=3.787
rarity=R
type=Creature
subtype=Elf,Druid
cost={1}{G}
pt=1/2
ability=Evolve;\
        {T}: Add {G} for each +1/+1 counter on SN.
timing=main
oracle=Evolve\n{T}: Add {G} for each +1/+1 counter on Gyre Sage.
status=not supported: add-multiple-mana
