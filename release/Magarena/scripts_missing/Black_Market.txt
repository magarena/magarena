name=Black Market
image=https://cards.scryfall.io/border_crop/front/7/8/78c80c6d-f3ae-48d3-ba6a-b9b738e1affb.jpg?1674141496
image_updated=2017-10-03
value=4.174
rarity=R
type=Enchantment
cost={3}{B}{B}
ability=Whenever a creature dies, put a charge counter on SN.;\
        At the beginning of your precombat main phase, add {B} for each charge counter on SN.
timing=enchantment
oracle=Whenever a creature dies, put a charge counter on Black Market.\nAt the beginning of your precombat main phase, add {B} for each charge counter on Black Market.
status=not supported: add-multiple-mana
