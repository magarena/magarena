name=Shisato, Whispering Hunter
image=https://cards.scryfall.io/border_crop/front/5/c/5cc79c9c-f776-4c55-a7d1-9fac33f14630.jpg?1562760556
value=2.880
rarity=R
type=Legendary,Creature
subtype=Snake,Warrior
cost={3}{G}
pt=2/2
ability=At the beginning of your upkeep, sacrifice a Snake.;\
        Whenever SN deals combat damage to a player, that player skips his or her next untap step.
timing=main
oracle=At the beginning of your upkeep, sacrifice a Snake.\nWhenever Shisato, Whispering Hunter deals combat damage to a player, that player skips his or her next untap step.
status=not supported: skip-action
