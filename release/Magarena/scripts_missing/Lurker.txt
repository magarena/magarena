name=Lurker
image=https://cards.scryfall.io/border_crop/front/b/3/b39eb671-e17e-4c5a-8913-1e3be7faedfb.jpg?1587910787
value=2.985
rarity=R
type=Creature
subtype=Beast
cost={2}{G}
pt=2/3
ability=SN can't be the target of spells unless it attacked or blocked this turn.
timing=main
oracle=Lurker can't be the target of spells unless it attacked or blocked this turn.
status=not supported: cant-be-target
