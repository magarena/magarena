name=Task Mage Assembly
image=https://cards.scryfall.io/border_crop/front/2/5/258a9cdd-c626-404e-b82d-01091f11f107.jpg?1562901991
value=2.704
rarity=R
type=Enchantment
cost={2}{R}
ability=When there are no creatures on the battlefield, sacrifice SN.;\
        {2}: SN deals 1 damage to target creature. Any player may activate this ability but only any time he or she could cast a sorcery.
timing=enchantment
oracle=When there are no creatures on the battlefield, sacrifice Task Mage Assembly.\n{2}: Task Mage Assembly deals 1 damage to target creature. Any player may activate this ability but only any time he or she could cast a sorcery.
status=not supported: any-player-activate
