name=Warcry Phoenix
image=https://cards.scryfall.io/border_crop/front/f/f/ff561f01-8dbc-4988-be00-592d1e417396.jpg?1562746405
value=2.500
rarity=U
type=Creature
subtype=Phoenix
cost={3}{R}
pt=2/2
ability=Flying, haste;\
        Whenever you attack with three or more creatures, you may pay {2}{R}. If you do, return SN from your graveyard to the battlefield tapped and attacking.
timing=main
oracle=Flying, haste\nWhenever you attack with three or more creatures, you may pay {2}{R}. If you do, return Warcry Phoenix from your graveyard to the battlefield tapped and attacking.
