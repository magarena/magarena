name=Commander's Sphere
image=https://cards.scryfall.io/border_crop/front/c/8/c8ffce25-18a5-4232-aa7c-d5c1a88e6052.jpg?1682209978
image_updated=2017-08-17
value=2.500
rarity=C
type=Artifact
cost={3}
ability={T}: Add one mana of any color in your commander's color identity.;\
        Sacrifice SN: Draw a card.
timing=artifact
oracle={T}: Add one mana of any color in your commander's color identity.\nSacrifice Commander's Sphere: Draw a card.
status=not supported: commander
