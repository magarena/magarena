name=Tajuru Stalwart
image=https://cards.scryfall.io/border_crop/front/f/9/f916220b-a942-41af-8949-aa384fa7857d.jpg?1562954039
image_updated=2015-11-08
value=2.500
rarity=C
type=Creature
subtype=Elf,Scout,Ally
cost={2}{G}
pt=0/1
ability=SN enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.
timing=main
oracle=Converge — Tajuru Stalwart enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.
status=not supported: mana-spent
