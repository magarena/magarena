name=Ugin's Nexus
image=https://cards.scryfall.io/border_crop/front/9/4/94002868-a48a-4ea8-bfce-17257078f5db.jpg?1562790526
value=3.500
rarity=M
type=Legendary,Artifact
cost={5}
ability=If a player would begin an extra turn, that player skips that turn instead.;\
        If SN would be put into a graveyard from the battlefield, instead exile it and take an extra turn after this one.
timing=artifact
oracle=If a player would begin an extra turn, that player skips that turn instead.\nIf Ugin's Nexus would be put into a graveyard from the battlefield, instead exile it and take an extra turn after this one.
status=not supported: skip-action
