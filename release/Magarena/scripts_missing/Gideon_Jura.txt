name=Gideon Jura
image=https://cards.scryfall.io/border_crop/front/f/b/fb4ed14d-369e-4dd0-a6bf-258f031855fb.jpg?1592765314
value=4.098
rarity=M
type=Legendary,Planeswalker
subtype=Gideon
cost={3}{W}{W}
loyalty=6
ability=+2: During target opponent's next turn, creatures that player controls attack SN if able.;\
        −2: Destroy target tapped creature.;\
        0: Until end of turn, SN becomes a 6/6 Human Soldier creature that's still a planeswalker. Prevent all damage that would be dealt to him this turn.
timing=main
oracle=+2: During target opponent's next turn, creatures that player controls attack Gideon Jura if able.\n−2: Destroy target tapped creature.\n0: Until end of turn, Gideon Jura becomes a 6/6 Human Soldier creature that's still a planeswalker. Prevent all damage that would be dealt to him this turn.
status=not supported: attacking-restriction
