name=Dictate of Karametra
image=https://cards.scryfall.io/border_crop/front/d/5/d5114607-df8f-4e44-8ef5-b6af89964bbc.jpg?1593096177
value=3.667
rarity=R
type=Enchantment
cost={3}{G}{G}
ability=Flash;\
        Whenever a player taps a land for mana, that player adds one mana of any type that land produced.
timing=flash
oracle=Flash\nWhenever a player taps a land for mana, that player adds one mana of any type that land produced.
status=not supported: non-activated-mana-ability
