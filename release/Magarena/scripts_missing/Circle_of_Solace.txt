name=Circle of Solace
image=https://cards.scryfall.io/border_crop/front/0/7/07f567dc-8a60-40e1-b947-199872d8df08.jpg?1562896849
value=2.938
rarity=R
type=Enchantment
cost={3}{W}
ability=As SN enters the battlefield, choose a creature type.;\
        {1}{W}: The next time a creature of the chosen type would deal damage to you this turn, prevent that damage.
timing=enchantment
oracle=As Circle of Solace enters the battlefield, choose a creature type.\n{1}{W}: The next time a creature of the chosen type would deal damage to you this turn, prevent that damage.
status=not supported: choose-creature-type
