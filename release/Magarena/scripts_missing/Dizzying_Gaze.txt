name=Dizzying Gaze
image=https://cards.scryfall.io/border_crop/front/7/1/71a482cf-a1cd-47b5-a76a-08e03965c679.jpg?1562087856
value=2.672
rarity=C
type=Enchantment
subtype=Aura
cost={R}
ability=Enchant creature you control;\
        {R}: Enchanted creature deals 1 damage to target creature with flying.
timing=aura
enchant=default,creature you control
oracle=Enchant creature you control\n{R}: Enchanted creature deals 1 damage to target creature with flying.
status=needs groovy
