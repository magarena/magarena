name=Chandra, Flamecaller
image=https://cards.scryfall.io/border_crop/front/a/8/a83ca3a1-82a9-4f47-b0b8-21687afd705b.jpg?1591320552
image_updated=2016-02-29
value=2.500
rarity=M
type=Legendary,Planeswalker
subtype=Chandra
cost={4}{R}{R}
loyalty=4
ability=+1: Create two 3/1 red Elemental creature tokens with haste. Exile them at the beginning of the next end step.;\
        0: Discard all the cards in your hand, then draw that many cards plus one.;\
        −X: SN deals X damage to each creature.
timing=main
oracle=+1: Create two 3/1 red Elemental creature tokens with haste. Exile them at the beginning of the next end step.\n0: Discard all the cards in your hand, then draw that many cards plus one.\n−X: Chandra, Flamecaller deals X damage to each creature.
status=not supported: non-mana-x-cost
