name=Clergy of the Holy Nimbus
image=https://cards.scryfall.io/border_crop/front/d/b/db1f578f-fa3b-4447-953b-1490852b6c80.jpg?1617613157
value=2.990
rarity=C
type=Creature
subtype=Human,Cleric
cost={W}
pt=1/1
ability=If SN would be destroyed, regenerate it.;\
        {1}: SN can't be regenerated this turn. Only any opponent may activate this ability.
timing=main
oracle=If Clergy of the Holy Nimbus would be destroyed, regenerate it.\n{1}: Clergy of the Holy Nimbus can't be regenerated this turn. Only any opponent may activate this ability.
status=not supported: any-player-activate
