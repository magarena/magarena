name=Ire Shaman
image=https://cards.scryfall.io/border_crop/front/e/a/ea54760c-2cd3-43eb-bc45-adc0997b34b0.jpg?1562442605
image_updated=2018-04-25
value=2.500
rarity=U
type=Creature
subtype=Orc,Shaman
cost={1}{R}
pt=2/1
ability=Menace;\
        Megamorph {R};\
        When SN is turned face up, exile the top card of your library. Until end of turn, you may play that card.
timing=main
oracle=Menace\nMegamorph {R}\nWhen Ire Shaman is turned face up, exile the top card of your library. Until end of turn, you may play that card.
status=not supported: blocking-restriction
