name=Corrupted Grafstone
image=https://cards.scryfall.io/border_crop/front/1/f/1ffa34cc-4042-4d2b-a7a0-eca0b593eeb6.jpg?1576385400
value=2.500
rarity=R
type=Artifact
cost={2}
ability=SN enters the battlefield tapped.;\
        {T}: Choose a color of a card in your graveyard. Add one mana of that color.
timing=artifact
oracle=Corrupted Grafstone enters the battlefield tapped.\n{T}: Choose a color of a card in your graveyard. Add one mana of that color.
status=needs groovy
