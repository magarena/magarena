name=Circle of Affliction
image=https://cards.scryfall.io/border_crop/front/1/f/1f059cd0-7924-495e-bac4-021f0e46bb87.jpg?1562567207
value=3.375
rarity=U
type=Enchantment
cost={1}{B}
ability=As SN enters the battlefield, choose a color.;\
        Whenever a source of the chosen color deals damage to you, you may pay {1}. If you do, target player loses 1 life and you gain 1 life.
timing=enchantment
oracle=As Circle of Affliction enters the battlefield, choose a color.\nWhenever a source of the chosen color deals damage to you, you may pay {1}. If you do, target player loses 1 life and you gain 1 life.
status=needs groovy
