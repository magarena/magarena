name=Engineered Plague
image=https://cards.scryfall.io/border_crop/front/b/6/b669e43e-3b11-42c9-8f20-0acce129e63c.jpg?1562246287
value=4.350
rarity=U
type=Enchantment
cost={2}{B}
ability=As SN enters the battlefield, choose a creature type.;\
        All creatures of the chosen type get -1/-1.
timing=enchantment
oracle=As Engineered Plague enters the battlefield, choose a creature type.\nAll creatures of the chosen type get -1/-1.
status=not supported: choose-creature-type
