name=Zurgo Helmsmasher
image=https://cards.scryfall.io/border_crop/front/1/3/13f4bafe-0d21-47ba-8f16-0274107d618c.jpg?1562782879
value=3.750
rarity=M
type=Legendary,Creature
subtype=Orc,Warrior
cost={2}{R}{W}{B}
pt=7/2
ability=Haste;\
        SN attacks each combat if able.;\
        SN has indestructible as long as it's your turn.;\
        Whenever a creature dealt damage by SN this turn dies, put a +1/+1 counter on SN.
timing=fmain
oracle=Haste\nZurgo Helmsmasher attacks each combat if able.\nZurgo Helmsmasher has indestructible as long as it's your turn.\nWhenever a creature dealt damage by Zurgo Helmsmasher this turn dies, put a +1/+1 counter on Zurgo Helmsmasher.
status=not supported: damage-by-SN-dies
