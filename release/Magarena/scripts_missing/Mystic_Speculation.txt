name=Mystic Speculation
image=https://cards.scryfall.io/border_crop/front/9/2/92adfcb4-6a0a-46f9-a171-bfa9fade2156.jpg?1562924619
value=3.865
rarity=U
type=Sorcery
cost={U}
ability=Buyback {2}
effect=Scry 3.
timing=main
oracle=Buyback {2}\nScry 3.
status=not supported: ordering-lib-top
