name=Skull Storm
image=https://api.scryfall.com/cards/c18/18/en?format=image&version=border_crop
value=2.500
rarity=R
type=Sorcery
cost={7}{B}{B}
effect=When you cast this spell, copy it for each time you've cast your commander from the command zone this game.~Each opponent sacrifices a creature. Each opponent who can't loses half their life, rounded up.
timing=main
oracle=When you cast this spell, copy it for each time you've cast your commander from the command zone this game.\nEach opponent sacrifices a creature. Each opponent who can't loses half their life, rounded up.
