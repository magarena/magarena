name=Marble Priest
image=https://cards.scryfall.io/border_crop/front/4/5/459b71d7-34c1-43b9-93ff-364f95aa4789.jpg?1562858713
value=1.919
rarity=U
type=Artifact,Creature
subtype=Cleric
cost={5}
pt=3/3
ability=All Walls able to block SN do so.;\
        Prevent all combat damage that would be dealt to SN by Walls.
timing=main
oracle=All Walls able to block Marble Priest do so.\nPrevent all combat damage that would be dealt to Marble Priest by Walls.
status=not supported: blocking-restriction
