name=Lure
image=https://cards.scryfall.io/border_crop/front/7/2/72c8336d-54cf-45af-a9ef-a1428facf91b.jpg?1562850140
image_updated=2017-04-12
value=4.042
rarity=U
type=Enchantment
subtype=Aura
cost={1}{G}{G}
ability=Enchant creature;\
        All creatures able to block enchanted creature do so.
timing=aura
enchant=default,creature
oracle=Enchant creature\nAll creatures able to block enchanted creature do so.
status=not supported: blocking-restriction
