name=Trickbind
image=https://cards.scryfall.io/border_crop/front/f/2/f2e58ff2-dea3-42b3-8c22-3e6202a7d433.jpg?1562946300
value=4.434
rarity=R
type=Instant
cost={1}{U}
ability=Split second
effect=Counter target activated or triggered ability. If a permanent's ability is countered this way, activated abilities of that permanent can't be activated this turn.
timing=counter
oracle=Split second\nCounter target activated or triggered ability. If a permanent's ability is countered this way, activated abilities of that permanent can't be activated this turn.
status=needs groovy
