name=Cho-Manno's Blessing
image=https://cards.scryfall.io/border_crop/front/5/c/5c9f33c6-5294-4584-854d-c8c0f847aba8.jpg?1562380883
value=3.882
rarity=C
type=Enchantment
subtype=Aura
cost={W}{W}
ability=Flash;\
        Enchant creature;\
        As SN enters the battlefield, choose a color.;\
        Enchanted creature has protection from the chosen color. This effect doesn't remove SN.
timing=flash
enchant=default,creature
oracle=Flash\nEnchant creature\nAs Cho-Manno's Blessing enters the battlefield, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn't remove Cho-Manno's Blessing.
status=not supported: doesnt-remove
