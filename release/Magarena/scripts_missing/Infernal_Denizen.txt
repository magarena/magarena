name=Infernal Denizen
image=https://cards.scryfall.io/border_crop/front/b/6/b63ac9a6-aaa5-4659-97d1-c5f6b0d5ccfe.jpg?1562928960
value=2.133
rarity=R
type=Creature
subtype=Demon
cost={7}{B}
pt=5/7
ability=At the beginning of your upkeep, sacrifice two Swamps. If you can't, tap SN, and an opponent may gain control of a creature you control of his or her choice for as long as SN remains on the battlefield.;\
        {T}: Gain control of target creature for as long as SN remains on the battlefield.
timing=main
oracle=At the beginning of your upkeep, sacrifice two Swamps. If you can't, tap Infernal Denizen, and an opponent may gain control of a creature you control of his or her choice for as long as Infernal Denizen remains on the battlefield.\n{T}: Gain control of target creature for as long as Infernal Denizen remains on the battlefield.
