name=Geothermal Crevice
image=https://cards.scryfall.io/border_crop/front/e/7/e744b593-13fe-4967-b492-ac02f5815e57.jpg?1562941417
value=3.423
rarity=C
type=Land
ability=SN enters the battlefield tapped.;\
        {T}: Add {R}.;\
        {T}, Sacrifice SN: Add {B}{G}.
timing=land
oracle=Geothermal Crevice enters the battlefield tapped.\n{T}: Add {R}.\n{T}, Sacrifice Geothermal Crevice: Add {B}{G}.
status=not supported: add-multiple-mana
