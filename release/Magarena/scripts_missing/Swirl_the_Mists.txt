name=Swirl the Mists
image=https://cards.scryfall.io/border_crop/front/2/e/2ea50e09-4ab3-439e-83d7-d584f8af8f16.jpg?1562758889
value=3.240
rarity=R
type=Enchantment
cost={2}{U}{U}
ability=As SN enters the battlefield, choose a color word.;\
        All instances of color words in the text of spells and permanents are changed to the chosen color word.
timing=enchantment
oracle=As Swirl the Mists enters the battlefield, choose a color word.\nAll instances of color words in the text of spells and permanents are changed to the chosen color word.
status=not supported: change-text
