name=Chandra, Roaring Flame
image=https://cards.scryfall.io/border_crop/back/b/0/b0d6caf0-4fa8-4ec5-b7f4-1307474d1b13.jpg?1562036951
image_updated=2015-11-05
value=2.500
rarity=M
type=Legendary,Planeswalker
subtype=Chandra
color=r
loyalty=4
ability=+1: SN deals 2 damage to target player.;\
        −2: SN deals 2 damage to target creature.;\
        −7: SN deals 6 damage to each opponent. Each player dealt damage this way gets an emblem with "At the beginning of your upkeep, this emblem deals 3 damage to you."
timing=main
hidden
transform=Chandra, Fire of Kaladesh
oracle=+1: Chandra, Roaring Flame deals 2 damage to target player.\n−2: Chandra, Roaring Flame deals 2 damage to target creature.\n−7: Chandra, Roaring Flame deals 6 damage to each opponent. Each player dealt damage this way gets an emblem with "At the beginning of your upkeep, this emblem deals 3 damage to you."
status=needs groovy
