name=Arashin Sovereign
image=https://cards.scryfall.io/border_crop/front/d/b/db7fbf78-8cb6-4fbf-b925-5543bcb64b45.jpg?1562794043
value=2.500
rarity=R
type=Creature
subtype=Dragon
cost={5}{G}{W}
pt=6/6
ability=Flying;\
        When SN dies, you may put it on the top or bottom of its owner's library.
timing=main
oracle=Flying\nWhen Arashin Sovereign dies, you may put it on the top or bottom of its owner's library.
