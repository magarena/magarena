name=Sandstone Needle
image=https://cards.scryfall.io/border_crop/front/8/2/82bc7c6b-2e3d-42d1-b2bb-b37b6f34d33b.jpg?1562381828
value=4.182
rarity=C
type=Land
ability=SN enters the battlefield tapped with two depletion counters on it.;\
        {T}, Remove a depletion counter from SN: Add {R}{R}. If there are no depletion counters on SN, sacrifice it.
timing=land
oracle=Sandstone Needle enters the battlefield tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Sandstone Needle: Add {R}{R}. If there are no depletion counters on Sandstone Needle, sacrifice it.
status=not supported: add-multiple-mana
