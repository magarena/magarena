name=Kiora, Master of the Depths
image=https://cards.scryfall.io/border_crop/front/4/b/4b63c228-3265-4029-9bc0-36b3e31dfb53.jpg?1562912680
image_updated=2015-11-08
value=2.500
rarity=M
type=Legendary,Planeswalker
subtype=Kiora
cost={2}{G}{U}
loyalty=4
ability=+1: Untap up to one target creature and up to one target land.;\
        −2: Reveal the top four cards of your library. You may put a creature card and/or a land card from among them into your hand. Put the rest into your graveyard.;\
        −8: You get an emblem with "Whenever a creature enters the battlefield under your control, you may have it fight target creature." Then create three 8/8 blue Octopus creature tokens.
timing=main
oracle=+1: Untap up to one target creature and up to one target land.\n−2: Reveal the top four cards of your library. You may put a creature card and/or a land card from among them into your hand. Put the rest into your graveyard.\n−8: You get an emblem with "Whenever a creature enters the battlefield under your control, you may have it fight target creature." Then create three 8/8 blue Octopus creature tokens.
status=not supported: multiple-targets
