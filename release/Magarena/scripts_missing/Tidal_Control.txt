name=Tidal Control
image=https://cards.scryfall.io/border_crop/front/c/b/cb9a7b7d-3d37-4bb6-ab48-1fec2bfb4fdc.jpg?1562770174
value=3.000
rarity=R
type=Enchantment
cost={1}{U}{U}
ability=Cumulative upkeep {2};\
        Pay 2 life or {2}: Counter target red or green spell. Any player may activate this ability.
timing=enchantment
oracle=Cumulative upkeep {2}\nPay 2 life or {2}: Counter target red or green spell. Any player may activate this ability.
status=not supported: any-player-activate
