name=Shifty Doppelganger
image=https://cards.scryfall.io/border_crop/front/5/3/538ffe1e-c312-4797-99dd-9bf5f896bdc3.jpg?1562910319
value=3.935
rarity=R
type=Creature
subtype=Shapeshifter
cost={2}{U}
pt=1/1
ability={3}{U}, Exile SN: You may put a creature card from your hand onto the battlefield. If you do, that creature gains haste until end of turn. At the beginning of the next end step, sacrifice that creature. If you do, return SN to the battlefield.
timing=main
oracle={3}{U}, Exile Shifty Doppelganger: You may put a creature card from your hand onto the battlefield. If you do, that creature gains haste until end of turn. At the beginning of the next end step, sacrifice that creature. If you do, return Shifty Doppelganger to the battlefield.
