name=Haven of the Spirit Dragon
image=https://cards.scryfall.io/border_crop/front/6/3/637ea3f0-d3ec-4cd4-8725-b845c50a6b96.jpg?1631589809
value=2.500
rarity=R
type=Land
ability={T}: Add {C}.;\
        {T}: Add one mana of any color. Spend this mana only to cast a Dragon creature spell.;\
        {2}, {T}, Sacrifice SN: Return target Dragon creature card or Ugin planeswalker card from your graveyard to your hand.
timing=land
oracle={T}: Add {C}.\n{T}: Add one mana of any color. Spend this mana only to cast a Dragon creature spell.\n{2}, {T}, Sacrifice Haven of the Spirit Dragon: Return target Dragon creature card or Ugin planeswalker card from your graveyard to your hand.
status=not supported: mana-restriction
