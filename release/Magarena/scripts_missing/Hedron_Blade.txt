name=Hedron Blade
image=https://cards.scryfall.io/border_crop/front/8/7/87242e1d-babe-4b0d-aa1f-ab62e0bc4ab7.jpg?1562926916
image_updated=2015-11-08
value=2.500
rarity=C
type=Artifact
subtype=Equipment
cost={1}
ability=Equipped creature gets +1/+1.;\
        Whenever equipped creature becomes blocked by one or more colorless creatures, it gains deathtouch until end of turn.;\
        Equip {2}
timing=equipment
oracle=Equipped creature gets +1/+1.\nWhenever equipped creature becomes blocked by one or more colorless creatures, it gains deathtouch until end of turn.\nEquip {2}
status=needs groovy
