name=Sirocco
image=https://cards.scryfall.io/border_crop/front/c/e/ce37492e-4f07-4171-97d4-84f28fb4e2be.jpg?1562721916
value=2.788
rarity=U
type=Instant
cost={1}{R}
effect=Target player reveals his or her hand. For each blue instant card revealed this way, that player discards that card unless he or she pays 4 life.
timing=removal
oracle=Target player reveals his or her hand. For each blue instant card revealed this way, that player discards that card unless he or she pays 4 life.
status=not supported: reveal-hand
