name=Kilnmouth Dragon
image=https://cards.scryfall.io/border_crop/front/6/7/67bbc1c3-8806-49d4-9111-a2334c66e5ec.jpg?1562916671
image_updated=2016-12-21
value=4.106
rarity=R
type=Creature
subtype=Dragon
cost={5}{R}{R}
pt=5/5
ability=Amplify 3;\
        Flying;\
        {T}: SN deals damage equal to the number of +1/+1 counters on it to target creature or player.
timing=main
oracle=Amplify 3\nFlying\n{T}: Kilnmouth Dragon deals damage equal to the number of +1/+1 counters on it to target creature or player.
status=not supported: amplify
