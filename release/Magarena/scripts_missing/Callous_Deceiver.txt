name=Callous Deceiver
image=https://cards.scryfall.io/border_crop/front/6/2/628d3058-d32e-438c-9a86-3e9c2be73c4a.jpg?1562760615
value=1.265
rarity=C
type=Creature
subtype=Spirit
cost={2}{U}
pt=1/3
ability={1}: Look at the top card of your library.;\
        {2}: Reveal the top card of your library. If it's a land card, SN gets +1/+0 and gains flying until end of turn. Activate this ability only once each turn.
timing=main
oracle={1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it's a land card, Callous Deceiver gets +1/+0 and gains flying until end of turn. Activate this ability only once each turn.
status=needs groovy
