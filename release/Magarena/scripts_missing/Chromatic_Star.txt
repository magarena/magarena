name=Chromatic Star
image=https://cards.scryfall.io/border_crop/front/c/2/c2e8d492-2c67-410b-b556-c157a14c4cec.jpg?1681724919
value=3.742
rarity=U
type=Artifact
cost={1}
ability={1}, {T}, Sacrifice SN: Add one mana of any color.;\
        When SN is put into a graveyard from the battlefield, draw a card.
timing=artifact
oracle={1}, {T}, Sacrifice Chromatic Star: Add one mana of any color.\nWhen Chromatic Star is put into a graveyard from the battlefield, draw a card.
status=not supported: mana-ability-with-choice
