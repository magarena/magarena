name=Prophetic Flamespeaker
image=https://cards.scryfall.io/border_crop/front/7/3/73326c79-f884-42d5-8546-3db46ce4fe52.jpg?1593096059
value=4.262
rarity=M
type=Creature
subtype=Human,Shaman
cost={1}{R}{R}
pt=1/3
ability=Double strike, trample;\
        Whenever SN deals combat damage to a player, exile the top card of your library. You may play it this turn.
timing=main
oracle=Double strike, trample\nWhenever Prophetic Flamespeaker deals combat damage to a player, exile the top card of your library. You may play it this turn.
status=not supported: may-play-card
