name=Prognostic Sphinx
image=https://cards.scryfall.io/border_crop/front/a/c/acd9e468-093b-4c88-8bf6-041bee6a1ea2.jpg?1631586402
value=3.535
rarity=R
type=Creature
subtype=Sphinx
cost={3}{U}{U}
pt=3/5
ability=Flying;\
        Discard a card: SN gains hexproof until end of turn. Tap it.;\
        Whenever SN attacks, scry 3.
timing=main
oracle=Flying\nDiscard a card: Prognostic Sphinx gains hexproof until end of turn. Tap it.\nWhenever Prognostic Sphinx attacks, scry 3.
status=not supported: ordering-lib-top
