name=Rishkar, Peema Renegade
image=https://cards.scryfall.io/border_crop/front/6/3/6310af34-e671-4974-b291-279b91585459.jpg?1682209604
value=2.500
rarity=R
type=Legendary,Creature
subtype=Elf,Druid
cost={2}{G}
pt=2/2
ability=When SN enters the battlefield, put a +1/+1 counter on each of up to two target creatures.;\
        Each creature you control with a counter on it has "{T}: Add {G}."
timing=main
oracle=When Rishkar, Peema Renegade enters the battlefield, put a +1/+1 counter on each of up to two target creatures.\nEach creature you control with a counter on it has "{T}: Add {G}."
status=not supported: multiple-targets
