name=Lurking Automaton
image=https://cards.scryfall.io/border_crop/front/e/0/e052a306-2bcd-4dcd-b41d-fa886227dab6.jpg?1562867339
value=3.625
rarity=C
type=Artifact,Creature
subtype=Construct
cost={5}
pt=0/0
ability=Reveal SN as you draft it and note how many cards you've drafted this draft round, including SN.;\
        SN enters the battlefield with X +1/+1 counters on it, where X is the highest number you noted for cards named Lurking Automaton.
timing=main
oracle=Reveal Lurking Automaton as you draft it and note how many cards you've drafted this draft round, including Lurking Automaton.\nLurking Automaton enters the battlefield with X +1/+1 counters on it, where X is the highest number you noted for cards named Lurking Automaton.
status=not supported: draft
