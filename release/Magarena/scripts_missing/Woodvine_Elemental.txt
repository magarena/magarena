name=Woodvine Elemental
image=https://cards.scryfall.io/border_crop/front/8/a/8acdb147-2339-4c55-b4f6-6a79679f2040.jpg?1562865926
value=3.778
rarity=U
type=Creature
subtype=Elemental
cost={4}{G}{W}
pt=4/4
ability=Trample;\
        Whenever SN attacks, each player reveals the top card of his or her library. For each nonland card revealed this way, attacking creatures you control get +1/+1 until end of turn. Then each player draws a card.
timing=main
oracle=Trample\nParley — Whenever Woodvine Elemental attacks, each player reveals the top card of his or her library. For each nonland card revealed this way, attacking creatures you control get +1/+1 until end of turn. Then each player draws a card.
status=needs groovy
