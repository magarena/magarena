name=Winding Canyons
image=https://cards.scryfall.io/border_crop/front/f/2/f26672a8-f4ff-4c64-bb3e-f5072bbc9e3e.jpg?1562804204
value=4.260
rarity=R
type=Land
ability={T}: Add {C}.;\
        {2}, {T}: Until end of turn, you may cast creature spells as though they had flash.
timing=land
oracle={T}: Add {C}.\n{2}, {T}: Until end of turn, you may cast creature spells as though they had flash.
status=not supported: card-modification
