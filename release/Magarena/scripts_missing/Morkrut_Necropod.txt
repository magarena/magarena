name=Morkrut Necropod
image=https://cards.scryfall.io/border_crop/front/f/7/f726f215-d0a5-4e2c-b874-9dc209479993.jpg?1576384528
value=2.500
rarity=U
type=Creature
subtype=Slug,Horror
cost={5}{B}
pt=7/7
ability=Menace;\
        Whenever SN attacks or blocks, sacrifice another creature or land.
timing=main
oracle=Menace\nWhenever Morkrut Necropod attacks or blocks, sacrifice another creature or land.
status=not supported: blocking-restriction
