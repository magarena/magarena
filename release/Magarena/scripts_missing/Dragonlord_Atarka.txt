name=Dragonlord Atarka
image=https://cards.scryfall.io/border_crop/front/c/e/ce0c3b45-cf1d-4d85-9fe3-fb26b4b15dfd.jpg?1562793278
value=2.500
rarity=M
type=Legendary,Creature
subtype=Elder,Dragon
cost={5}{R}{G}
pt=8/8
ability=Flying, trample;\
        When SN enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or planeswalkers your opponents control.
timing=main
oracle=Flying, trample\nWhen Dragonlord Atarka enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or planeswalkers your opponents control.
status=not supported: multiple-targets
