name=Away
image=https://cards.scryfall.io/border_crop/front/d/1/d13cdb71-a499-41db-84e6-95f84650c524.jpg?1562934087
value=4.197
rarity=U
type=Instant
cost={2}{B}
split=Far
ability=Fuse
effect=Target player sacrifices a creature.
timing=removal
second_half
oracle=Target player sacrifices a creature.\nFuse
status=not supported: split-card
