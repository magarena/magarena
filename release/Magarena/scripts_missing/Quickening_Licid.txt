name=Quickening Licid
image=https://cards.scryfall.io/border_crop/front/e/6/e6e91f3d-5a23-4df1-a879-d18a3af92a28.jpg?1562057343
value=2.759
rarity=U
type=Creature
subtype=Licid
cost={1}{W}
pt=1/1
ability={1}{W}, {T}: SN loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {W} to end this effect.;\
        Enchanted creature has first strike.
timing=main
oracle={1}{W}, {T}: Quickening Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {W} to end this effect.\nEnchanted creature has first strike.
status=not supported: end-effect
