name=Reyhan, Last of the Abzan
image=https://cards.scryfall.io/border_crop/front/8/a/8afad106-a49b-4910-959e-228c109ea983.jpg?1644853038
image_updated=2016-12-28
value=2.500
rarity=R
type=Legendary,Creature
subtype=Human,Warrior
cost={1}{B}{G}
pt=0/0
ability=SN enters the battlefield with three +1/+1 counters on it.;\
        Whenever a creature you control dies or is put into the command zone, if it had one or more +1/+1 counters on it, you may put that many +1/+1 counters on target creature.;\
        Partner
timing=main
oracle=Reyhan, Last of the Abzan enters the battlefield with three +1/+1 counters on it.\nWhenever a creature you control dies or is put into the command zone, if it had one or more +1/+1 counters on it, you may put that many +1/+1 counters on target creature.\nPartner
status=needs groovy
