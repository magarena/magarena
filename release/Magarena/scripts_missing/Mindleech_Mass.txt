name=Mindleech Mass
image=https://cards.scryfall.io/border_crop/front/7/c/7cd1ace7-d4fe-4f96-9434-7ab1442bf36f.jpg?1598917172
value=4.029
rarity=R
type=Creature
subtype=Horror
cost={5}{U}{B}{B}
pt=6/6
ability=Trample;\
        Whenever SN deals combat damage to a player, you may look at that player's hand. If you do, you may cast a nonland card in it without paying that card's mana cost.
timing=main
oracle=Trample\nWhenever Mindleech Mass deals combat damage to a player, you may look at that player's hand. If you do, you may cast a nonland card in it without paying that card's mana cost.
status=not supported: reveal-hand
