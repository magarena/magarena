name=Gravegouger
image=https://cards.scryfall.io/border_crop/front/a/c/acfcd559-374e-4e6f-9333-2e5c855abff5.jpg?1562631503
value=2.167
rarity=C
type=Creature
subtype=Nightmare,Horror
cost={2}{B}
pt=2/2
ability=When SN enters the battlefield, exile up to two target cards from a single graveyard.;\
        When SN leaves the battlefield, return the exiled cards to their owner's graveyard.
timing=main
oracle=When Gravegouger enters the battlefield, exile up to two target cards from a single graveyard.\nWhen Gravegouger leaves the battlefield, return the exiled cards to their owner's graveyard.
status=not supported: multiple-targets
