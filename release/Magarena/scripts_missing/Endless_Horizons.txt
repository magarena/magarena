name=Endless Horizons
image=https://cards.scryfall.io/border_crop/front/3/2/32385604-dcff-41e4-97a5-fcf230033a87.jpg?1562905505
value=3.947
rarity=R
type=Enchantment
cost={3}{W}
ability=When SN enters the battlefield, search your library for any number of Plains cards and exile them. Then shuffle your library.;\
        At the beginning of your upkeep, you may put a card you own exiled with SN into your hand.
timing=enchantment
oracle=When Endless Horizons enters the battlefield, search your library for any number of Plains cards and exile them. Then shuffle your library.\nAt the beginning of your upkeep, you may put a card you own exiled with Endless Horizons into your hand.
status=not supported: any-number-of
