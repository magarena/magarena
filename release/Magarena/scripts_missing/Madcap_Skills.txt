name=Madcap Skills
image=https://cards.scryfall.io/border_crop/front/b/a/ba77b1ff-e73d-4181-9564-d71df348b2fe.jpg?1593813488
image_updated=2017-08-08
value=3.667
rarity=C
type=Enchantment
subtype=Aura
cost={1}{R}
ability=Enchant creature;\
        Enchanted creature gets +3/+0 and has menace.
timing=aura
enchant=default,creature
oracle=Enchant creature\nEnchanted creature gets +3/+0 and has menace.
status=not supported: blocking-restriction
