name=Metamorphic Alteration
image=https://cards.scryfall.io/border_crop/front/e/f/eff03d37-d90a-4dcc-bacd-64fd71301354.jpg?1562304888
value=2.500
rarity=R
type=Enchantment
subtype=Aura
cost={1}{U}
ability=Enchant creature;\
        As SN enters the battlefield, choose a creature.;\
        Enchanted creature is a copy of the chosen creature.
timing=aura
enchant=default,creature
oracle=Enchant creature\nAs Metamorphic Alteration enters the battlefield, choose a creature.\nEnchanted creature is a copy of the chosen creature.
