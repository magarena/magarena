name=Flamekin Village
image=https://cards.scryfall.io/border_crop/front/9/3/930e6064-3d4b-40f1-850d-daa15cea756b.jpg?1661583454
value=2.500
rarity=R
type=Land
ability=As SN enters the battlefield, you may reveal an Elemental card from your hand. If you don't, SN enters the battlefield tapped.;\
        {T}: Add {R}.;\
        {R}, {T}: Target creature gains haste until end of turn.
timing=land
oracle=As Flamekin Village enters the battlefield, you may reveal an Elemental card from your hand. If you don't, Flamekin Village enters the battlefield tapped.\n{T}: Add {R}.\n{R}, {T}: Target creature gains haste until end of turn.
status=not supported: reveal-cost
