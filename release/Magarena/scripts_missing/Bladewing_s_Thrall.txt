name=Bladewing's Thrall
image=https://cards.scryfall.io/border_crop/front/9/d/9d63d3d8-5699-4577-a908-4415bc96b404.jpg?1562852063
image_updated=2017-04-12
value=3.382
rarity=U
type=Creature
subtype=Zombie
cost={2}{B}{B}
pt=3/3
ability=SN has flying as long as you control a Dragon.;\
        When a Dragon enters the battlefield, you may return SN from your graveyard to the battlefield.
timing=main
oracle=Bladewing's Thrall has flying as long as you control a Dragon.\nWhen a Dragon enters the battlefield, you may return Bladewing's Thrall from your graveyard to the battlefield.
status=not supported: trigger-from-graveyard
