name=Gatstaf Ravagers
image=https://cards.scryfall.io/border_crop/back/6/a/6ab67098-d3d5-4799-ae03-c734244f370e.jpg?1576384747
value=2.500
rarity=C
type=Creature
subtype=Werewolf
color=r
pt=6/5
ability=Menace;\
        At the beginning of each upkeep, if a player cast two or more spells last turn, transform SN.
transform=Gatstaf Arsonists
timing=main
hidden
oracle=Menace\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Gatstaf Ravagers.
status=not supported: blocking-restriction
