name=Everflowing Chalice
image=https://cards.scryfall.io/border_crop/front/f/b/fb35c235-7749-4016-917c-dd8b586ac819.jpg?1674142591
image_updated=2017-08-17
value=2.500
rarity=U
type=Artifact
cost={0}
ability=Multikicker {2};\
        SN enters the battlefield with a charge counter on it for each time it was kicked.;\
        {T}: Add {C} for each charge counter on SN.
timing=artifact
oracle=Multikicker {2}\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {C} for each charge counter on Everflowing Chalice.
status=not supported: add-multiple-mana
