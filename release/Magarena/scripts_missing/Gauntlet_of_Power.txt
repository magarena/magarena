name=Gauntlet of Power
image=https://cards.scryfall.io/border_crop/front/1/4/14e870a0-5699-4f42-b67a-8dbd24e0c9c2.jpg?1619404297
value=4.157
rarity=R
type=Artifact
cost={5}
ability=As SN enters the battlefield, choose a color.;\
        Creatures of the chosen color get +1/+1.;\
        Whenever a basic land is tapped for mana of the chosen color, its controller adds one mana of that color.
timing=artifact
oracle=As Gauntlet of Power enters the battlefield, choose a color.\nCreatures of the chosen color get +1/+1.\nWhenever a basic land is tapped for mana of the chosen color, its controller adds one mana of that color.
status=not supported: non-activated-mana-ability
