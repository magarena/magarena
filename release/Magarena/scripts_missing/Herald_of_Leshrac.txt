name=Herald of Leshrac
image=https://cards.scryfall.io/border_crop/front/a/d/ad6080b1-b032-4172-8594-4d894a60a80d.jpg?1593275109
value=3.900
rarity=R
type=Creature
subtype=Avatar
cost={6}{B}
pt=2/4
ability=Flying;\
        Cumulative upkeep—Gain control of a land you don't control.;\
        SN gets +1/+1 for each land you control but don't own.;\
        When SN leaves the battlefield, each player gains control of each land he or she owns that you control.
timing=main
oracle=Flying\nCumulative upkeep—Gain control of a land you don't control.\nHerald of Leshrac gets +1/+1 for each land you control but don't own.\nWhen Herald of Leshrac leaves the battlefield, each player gains control of each land he or she owns that you control.
