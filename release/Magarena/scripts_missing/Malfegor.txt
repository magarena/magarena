name=Malfegor
image=https://cards.scryfall.io/border_crop/front/0/1/010a4b07-c2d5-433e-9898-6e148104e9e0.jpg?1562845263
image_updated=2017-04-12
value=3.263
rarity=R
type=Legendary,Creature
subtype=Demon,Dragon
cost={2}{B}{B}{R}{R}
pt=6/6
ability=Flying;\
        When SN enters the battlefield, discard your hand. Each opponent sacrifices a creature for each card discarded this way.
timing=main
oracle=Flying\nWhen Malfegor enters the battlefield, discard your hand. Each opponent sacrifices a creature for each card discarded this way.
status=needs groovy
