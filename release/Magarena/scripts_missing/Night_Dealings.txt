name=Night Dealings
image=https://cards.scryfall.io/border_crop/front/5/8/58d012ee-9523-469f-8ddb-f4b664093c13.jpg?1562760139
value=3.083
rarity=R
type=Enchantment
cost={2}{B}{B}
ability=Whenever a source you control deals damage to another player, put that many theft counters on SN.;\
        {2}{B}{B}, Remove X theft counters from SN: Search your library for a nonland card with converted mana cost X, reveal it, and put it into your hand. Then shuffle your library.
timing=enchantment
oracle=Whenever a source you control deals damage to another player, put that many theft counters on Night Dealings.\n{2}{B}{B}, Remove X theft counters from Night Dealings: Search your library for a nonland card with converted mana cost X, reveal it, and put it into your hand. Then shuffle your library.
status=not supported: non-mana-x-cost
