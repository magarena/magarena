name=Mizzium Transreliquat
image=https://cards.scryfall.io/border_crop/front/d/2/d29c3bf9-d37c-4e0a-ac44-15d47770ed44.jpg?1593272943
value=4.200
rarity=R
type=Artifact
cost={3}
ability={3}: SN becomes a copy of target artifact until end of turn.;\
        {1}{U}{R}: SN becomes a copy of target artifact and gains this ability.
timing=artifact
oracle={3}: Mizzium Transreliquat becomes a copy of target artifact until end of turn.\n{1}{U}{R}: Mizzium Transreliquat becomes a copy of target artifact and gains this ability.
status=not supported: becomes-copy
