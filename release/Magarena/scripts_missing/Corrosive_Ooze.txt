name=Corrosive Ooze
image=https://cards.scryfall.io/border_crop/front/d/1/d13deb9c-5985-4355-8716-ee1c7b54b8e2.jpg?1562743360
value=2.500
rarity=C
type=Creature
subtype=Ooze
cost={1}{G}
pt=2/2
ability=Whenever SN blocks or becomes blocked by an equipped creature, destroy all Equipment attached to that creature at end of combat.
timing=main
oracle=Whenever Corrosive Ooze blocks or becomes blocked by an equipped creature, destroy all Equipment attached to that creature at end of combat.
