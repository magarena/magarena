name=Chandra's Phoenix
image=https://cards.scryfall.io/border_crop/front/1/a/1a91cd9b-9a5d-4251-bd6c-41c3ebd6d991.jpg?1592765637
value=3.709
rarity=R
type=Creature
subtype=Phoenix
cost={1}{R}{R}
pt=2/2
ability=Flying;\
        Haste;\
        Whenever an opponent is dealt damage by a red instant or sorcery spell you control or by a red planeswalker you control, return SN from your graveyard to your hand.
timing=fmain
oracle=Flying\nHaste\nWhenever an opponent is dealt damage by a red instant or sorcery spell you control or by a red planeswalker you control, return Chandra's Phoenix from your graveyard to your hand.
status=not supported: trigger-from-graveyard
