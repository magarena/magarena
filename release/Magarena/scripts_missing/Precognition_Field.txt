name=Precognition Field
image=https://cards.scryfall.io/border_crop/front/c/f/cf2a92ee-2b20-4299-84b2-b4963f4a42a1.jpg?1562743197
value=2.500
rarity=R
type=Enchantment
cost={3}{U}
ability=You may look at the top card of your library.;\
        You may cast the top card of your library if it's an instant or sorcery card.;\
        {3}: Exile the top card of your library.
timing=enchantment
oracle=You may look at the top card of your library.\nYou may cast the top card of your library if it's an instant or sorcery card.\n{3}: Exile the top card of your library.
