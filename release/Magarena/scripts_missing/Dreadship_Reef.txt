name=Dreadship Reef
image=https://cards.scryfall.io/border_crop/front/1/8/18642a9e-ac45-402b-bb56-08cb6336c485.jpg?1619399194
image_updated=2017-08-17
value=3.309
rarity=U
type=Land
ability={T}: Add {C}.;\
        {1}, {T}: Put a storage counter on SN.;\
        {1}, Remove X storage counters from SN: Add X mana in any combination of {U} and/or {B}.
timing=land
oracle={T}: Add {C}.\n{1}, {T}: Put a storage counter on Dreadship Reef.\n{1}, Remove X storage counters from Dreadship Reef: Add X mana in any combination of {U} and/or {B}.
status=not supported: add-multiple-mana
