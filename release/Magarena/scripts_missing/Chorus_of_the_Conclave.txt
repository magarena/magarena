name=Chorus of the Conclave
image=https://cards.scryfall.io/border_crop/front/f/6/f6cde2c4-f53d-4b44-b83b-37b12e00c835.jpg?1592713989
value=3.288
rarity=R
type=Legendary,Creature
subtype=Dryad
cost={4}{G}{G}{W}{W}
pt=3/8
ability=Forestwalk;\
        As an additional cost to cast creature spells, you may pay any amount of mana. If you do, that creature enters the battlefield with that many additional +1/+1 counters on it.
timing=main
oracle=Forestwalk\nAs an additional cost to cast creature spells, you may pay any amount of mana. If you do, that creature enters the battlefield with that many additional +1/+1 counters on it.
status=not supported: complex-cost-modification
