name=Freyalise's Winds
image=https://cards.scryfall.io/border_crop/front/a/4/a4a9d2fb-fdd1-4287-b48c-21ded059fd23.jpg?1562929713
value=3.759
rarity=R
type=Enchantment
cost={2}{G}{G}
ability=Whenever a permanent becomes tapped, put a wind counter on it.;\
        If a permanent with a wind counter on it would untap during its controller's untap step, remove all wind counters from it instead.
timing=enchantment
oracle=Whenever a permanent becomes tapped, put a wind counter on it.\nIf a permanent with a wind counter on it would untap during its controller's untap step, remove all wind counters from it instead.
