name=Sweatworks Brawler
image=https://cards.scryfall.io/border_crop/front/b/4/b477d12a-9ba5-4302-a3f0-af0afb5d87f4.jpg?1576381921
value=2.500
rarity=C
type=Creature
subtype=Human,Artificer
cost={3}{R}
pt=3/3
ability=Improvise;\
        Menace
timing=main
oracle=Improvise\nMenace
status=not supported: blocking-restriction
