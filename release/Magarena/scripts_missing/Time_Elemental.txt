name=Time Elemental
image=https://cards.scryfall.io/border_crop/front/7/2/720672dc-d75a-44c3-a48d-d2fe3993fbbf.jpg?1559592438
value=3.420
rarity=R
type=Creature
subtype=Elemental
cost={2}{U}
pt=0/2
ability=When SN attacks or blocks, at end of combat, sacrifice it and it deals 5 damage to you.;\
        {2}{U}{U}, {T}: Return target permanent that isn't enchanted to its owner's hand.
timing=main
oracle=When Time Elemental attacks or blocks, at end of combat, sacrifice it and it deals 5 damage to you.\n{2}{U}{U}, {T}: Return target permanent that isn't enchanted to its owner's hand.
status=needs groovy
