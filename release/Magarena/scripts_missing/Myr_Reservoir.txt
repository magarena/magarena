name=Myr Reservoir
image=https://cards.scryfall.io/border_crop/front/6/0/60678391-44b2-4525-94dc-ffc5a433b79b.jpg?1562818213
value=3.629
rarity=R
type=Artifact
cost={3}
ability={T}: Add {C}{C}. Spend this mana only to cast Myr spells or activate abilities of Myr.;\
        {3}, {T}: Return target Myr card from your graveyard to your hand.
timing=artifact
oracle={T}: Add {C}{C}. Spend this mana only to cast Myr spells or activate abilities of Myr.\n{3}, {T}: Return target Myr card from your graveyard to your hand.
status=not supported: add-multiple-mana
