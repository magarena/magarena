name=Endbringer's Revel
image=https://cards.scryfall.io/border_crop/front/b/7/b76843b0-0e71-473b-8c9b-6a8bc30255da.jpg?1562929116
value=2.886
rarity=U
type=Enchantment
cost={2}{B}
ability={4}: Return target creature card from a graveyard to its owner's hand. Any player may activate this ability but only any time he or she could cast a sorcery.
timing=enchantment
oracle={4}: Return target creature card from a graveyard to its owner's hand. Any player may activate this ability but only any time he or she could cast a sorcery.
status=not supported: any-player-activate
