name=Caller of the Hunt
image=https://cards.scryfall.io/border_crop/front/c/0/c0e8e1cf-0a47-4ce4-889a-091229d0e466.jpg?1562383014
value=3.733
rarity=R
type=Creature
subtype=Human
cost={2}{G}
pt=*/*
ability=As an additional cost to cast SN, choose a creature type.;\
        SN's power and toughness are each equal to the number of creatures of the chosen type on the battlefield.
timing=main
oracle=As an additional cost to cast Caller of the Hunt, choose a creature type.\nCaller of the Hunt's power and toughness are each equal to the number of creatures of the chosen type on the battlefield.
status=not supported: choose-creature-type
