name=Golgari Brownscale
image=https://cards.scryfall.io/border_crop/front/e/4/e41f3323-f702-46a6-92db-33ec0afd75d2.jpg?1547517583
value=3.000
rarity=C
type=Creature
subtype=Lizard
cost={1}{G}{G}
pt=2/3
ability=When SN is put into your hand from your graveyard, you gain 2 life.;\
        Dredge 2
timing=main
oracle=When Golgari Brownscale is put into your hand from your graveyard, you gain 2 life.\nDredge 2
status=not supported: trigger-from-graveyard
