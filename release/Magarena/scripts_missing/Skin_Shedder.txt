name=Skin Shedder
image=https://cards.scryfall.io/border_crop/back/c/0/c0243401-e3ae-41c6-a3a0-9cd4b330c200.jpg?1576384912
value=2.500
rarity=U
type=Creature
subtype=Insect,Horror
color=r
pt=3/4
transform=Skin Invasion
timing=main
hidden
oracle=NONE
status=needs other half
