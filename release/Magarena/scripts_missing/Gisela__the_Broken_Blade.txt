name=Gisela, the Broken Blade
image=https://cards.scryfall.io/border_crop/front/c/7/c75c035a-7da9-4b36-982d-fca8220b1797.jpg?1625771749
image_updated=2016-09-05
value=2.500
rarity=M
type=Legendary,Creature
subtype=Angel,Horror
cost={2}{W}{W}
pt=4/3
ability=Flying, first strike, lifelink;\
        At the beginning of your end step, if you both own and control SN and a creature named Bruna, the Fading Light, exile them, then meld them into Brisela, Voice of Nightmares.
transform=Brisela, Voice of Nightmares
timing=main
oracle=Flying, first strike, lifelink\nAt the beginning of your end step, if you both own and control Gisela, the Broken Blade and a creature named Bruna, the Fading Light, exile them, then meld them into Brisela, Voice of Nightmares.
status=not supported: meld
