name=Ayli, Eternal Pilgrim
image=https://cards.scryfall.io/border_crop/front/e/7/e7b5893d-6df6-4cae-ae70-d02d443d1740.jpg?1562941484
image_updated=2016-02-29
value=2.500
rarity=R
type=Legendary,Creature
subtype=Kor,Cleric
cost={W}{B}
pt=2/3
ability=Deathtouch;\
        {1}, Sacrifice another creature: You gain life equal to the sacrificed creature's toughness.;\
        {1}{W}{B}, Sacrifice another creature: Exile target nonland permanent. Activate this ability only if you have at least 10 life more than your starting life total.
timing=main
oracle=Deathtouch\n{1}, Sacrifice another creature: You gain life equal to the sacrificed creature's toughness.\n{1}{W}{B}, Sacrifice another creature: Exile target nonland permanent. Activate this ability only if you have at least 10 life more than your starting life total.
status=needs groovy
