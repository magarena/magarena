name=Paliano Vanguard
image=https://cards.scryfall.io/border_crop/front/2/b/2b120f2d-2585-4827-a8c5-d6ecdc8108e5.jpg?1576381619
image_updated=2016-09-05
value=2.500
rarity=R
type=Creature
subtype=Human,Soldier
cost={1}{W}
pt=2/2
ability=Draft SN face up.;\
        As you draft a creature card, you may reveal it, note its creature types, then turn SN face down.;\
        Other creatures you control of a type you noted for cards named Paliano Vanguard get +1/+1.
timing=main
oracle=Draft Paliano Vanguard face up.\nAs you draft a creature card, you may reveal it, note its creature types, then turn Paliano Vanguard face down.\nOther creatures you control of a type you noted for cards named Paliano Vanguard get +1/+1.
status=not supported: draft
