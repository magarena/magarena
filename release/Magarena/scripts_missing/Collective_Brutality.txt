name=Collective Brutality
image=https://cards.scryfall.io/border_crop/front/c/b/cb94a02f-4660-45b6-8a39-941b710cf8f3.jpg?1576384285
image_updated=2016-09-05
value=2.500
rarity=R
type=Sorcery
cost={1}{B}
effect=Escalate—Discard a card.~Choose one or more — (1) Target opponent reveals his or her hand. You choose an instant or sorcery card from it. That player discards that card. (2) Target creature gets -2/-2 until end of turn. (3) Target opponent loses 2 life and you gain 2 life.
timing=main
oracle=Escalate—Discard a card.\nChoose one or more —\n• Target opponent reveals his or her hand. You choose an instant or sorcery card from it. That player discards that card.\n• Target creature gets -2/-2 until end of turn.\n• Target opponent loses 2 life and you gain 2 life.
status=not supported: multi-choice-modal
