name=Infectious Curse
image=https://cards.scryfall.io/border_crop/back/8/0/8093f8b0-5d50-48ca-b616-e92535a47138.jpg?1576384328
value=2.500
rarity=U
type=Enchantment
subtype=Aura,Curse
color=b
ability=Enchant player;\
        Spells you cast that target enchanted player cost {1} less to cast.;\
        At the beginning of enchanted player's upkeep, that player loses 1 life and you gain 1 life.
transform=Accursed Witch
timing=aura
enchant=default,creature
hidden
oracle=Enchant player\nSpells you cast that target enchanted player cost {1} less to cast.\nAt the beginning of enchanted player's upkeep, that player loses 1 life and you gain 1 life.
status=not supported: complex-cost-modification
