name=Cover of Darkness
image=https://cards.scryfall.io/border_crop/front/0/d/0d6d7d88-d82b-40f4-bf57-ec5d7c480689.jpg?1562898088
value=4.110
rarity=R
type=Enchantment
cost={1}{B}
ability=As SN enters the battlefield, choose a creature type.;\
        Creatures of the chosen type have fear.
timing=enchantment
oracle=As Cover of Darkness enters the battlefield, choose a creature type.\nCreatures of the chosen type have fear.
status=not supported: choose-creature-type
