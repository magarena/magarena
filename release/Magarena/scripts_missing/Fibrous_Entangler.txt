name=Fibrous Entangler
image=https://cards.scryfall.io/border_crop/back/e/e/ee648500-a213-4aa4-a97c-b7223c11bebd.jpg?1576384915
image_updated=2016-09-05
value=2.500
rarity=U
type=Creature
subtype=Eldrazi,Werewolf
color=
pt=4/6
ability=Vigilance;\
        SN must be blocked if able.;\
        SN can block an additional creature each combat.
transform=Tangleclaw Werewolf
timing=main
hidden
oracle=Vigilance\nFibrous Entangler must be blocked if able.\nFibrous Entangler can block an additional creature each combat.
status=not supported: block-multiple
