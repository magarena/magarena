name=Master of the Wild Hunt
image=https://cards.scryfall.io/border_crop/front/3/c/3c56fda1-d286-46b1-9617-4f7c430abb79.jpg?1562434903
image_updated=2018-04-25
value=4.282
rarity=M
type=Creature
subtype=Human,Shaman
cost={2}{G}{G}
pt=3/3
ability=At the beginning of your upkeep, create a 2/2 green Wolf creature token.;\
        {T}: Tap all untapped Wolf creatures you control. Each Wolf tapped this way deals damage equal to its power to target creature. That creature deals damage equal to its power divided as its controller chooses among any number of those Wolves.
timing=main
oracle=At the beginning of your upkeep, create a 2/2 green Wolf creature token.\n{T}: Tap all untapped Wolf creatures you control. Each Wolf tapped this way deals damage equal to its power to target creature. That creature deals damage equal to its power divided as its controller chooses among any number of those Wolves.
status=not supported: any-number-of
