name=Entropic Specter
image=https://cards.scryfall.io/border_crop/front/b/d/bdb04d81-b0ab-4bc7-935d-c31005887240.jpg?1562088783
value=1.706
rarity=R
type=Creature
subtype=Specter,Spirit
cost={3}{B}{B}
pt=*/*
ability=Flying;\
        As SN enters the battlefield, choose an opponent.;\
        Whenever SN deals damage to a player, that player discards a card.
timing=main
requires_groovy_code
oracle=Flying\nAs Entropic Specter enters the battlefield, choose an opponent.\nEntropic Specter's power and toughness are each equal to the number of cards in the chosen player's hand.\nWhenever Entropic Specter deals damage to a player, that player discards a card.
