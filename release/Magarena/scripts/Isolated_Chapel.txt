name=Isolated Chapel
image=https://cards.scryfall.io/border_crop/front/a/1/a1d95d37-5dbe-4a25-bc80-a4db08f3c63a.jpg?1562740520
image_updated=2018-08-07
value=3.375
rarity=R
type=Land
mana=w3b3
ability=SN enters the battlefield tapped unless you control a Plains or a Swamp.;\
        {T}: Add {W} or {B}.
timing=land
oracle=Isolated Chapel enters the battlefield tapped unless you control a Plains or a Swamp.\n{T}: Add {W} or {B}.
