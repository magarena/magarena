name=Ancient of the Equinox
image=https://cards.scryfall.io/border_crop/back/d/4/d41b07c8-f0c8-4654-bcc2-00c785c8c628.jpg?1576384999
value=2.500
rarity=U
type=Creature
subtype=Treefolk
color=g
pt=4/4
ability=Trample, hexproof
transform=Autumnal Gloom
timing=main
hidden
oracle=Trample, hexproof
