name=Jace's Sanctum
image=https://cards.scryfall.io/border_crop/front/8/1/8148adc6-7946-4abd-8601-b1f4cd6916c2.jpg?1568004013
image_updated=2015-11-05
value=2.500
rarity=R
type=Enchantment
cost={3}{U}
ability=Instant and sorcery spells you cast cost {1} less to cast.;\
        Whenever you cast an instant or sorcery spell, scry 1.
timing=enchantment
oracle=Instant and sorcery spells you cast cost {1} less to cast.\nWhenever you cast an instant or sorcery spell, scry 1.
