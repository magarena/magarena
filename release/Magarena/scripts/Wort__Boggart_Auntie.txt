name=Wort, Boggart Auntie
image=https://cards.scryfall.io/border_crop/front/a/0/a08a1377-dede-47c8-8447-c9df125f3b14.jpg?1562360544
value=4.067
rarity=R
type=Legendary,Creature
subtype=Goblin,Shaman
cost={2}{B}{R}
pt=3/3
ability=Fear;\
        At the beginning of your upkeep, you may return target Goblin card from your graveyard to your hand.
timing=main
oracle=Fear\nAt the beginning of your upkeep, you may return target Goblin card from your graveyard to your hand.
