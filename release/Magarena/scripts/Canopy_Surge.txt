name=Canopy Surge
image=https://cards.scryfall.io/border_crop/front/2/e/2e19d68e-7554-4627-a316-beb1f75fa494.jpg?1562904391
value=3.600
rarity=U
type=Sorcery
cost={1}{G}
ability=Kicker {2}
timing=removal
requires_groovy_code
oracle=Kicker {2}\nCanopy Surge deals 1 damage to each creature with flying and each player. If Canopy Surge was kicked, it deals 4 damage to each creature with flying and each player instead.
