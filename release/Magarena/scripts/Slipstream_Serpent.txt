name=Slipstream Serpent
image=https://cards.scryfall.io/border_crop/front/f/1/f16dac30-52d2-4b87-90b4-4bc7f8f729ee.jpg?1619394674
value=1.000
rarity=C
type=Creature
subtype=Serpent
cost={7}{U}
pt=6/6
ability=SN can't attack unless defending player controls an Island.;\
        When you control no Islands, sacrifice SN.;\
        Morph {5}{U}
timing=main
oracle=Slipstream Serpent can't attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Slipstream Serpent.\nMorph {5}{U}
