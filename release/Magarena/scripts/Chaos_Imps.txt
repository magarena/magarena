name=Chaos Imps
image=https://cards.scryfall.io/border_crop/front/c/7/c70a702a-c9be-4bee-9087-22b5905f783a.jpg?1562793016
value=3.566
rarity=R
type=Creature
subtype=Imp
cost={4}{R}{R}
pt=6/5
ability=Flying;\
        Unleash;\
        SN has trample as long as it has a +1/+1 counter on it.
timing=main
oracle=Flying\nUnleash\nChaos Imps has trample as long as it has a +1/+1 counter on it.
