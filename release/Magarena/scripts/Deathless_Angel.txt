name=Deathless Angel
image=https://cards.scryfall.io/border_crop/front/0/4/049fb314-184c-4411-9035-f04215659056.jpg?1562700754
value=4.041
rarity=R
type=Creature
subtype=Angel
cost={4}{W}{W}
pt=5/7
ability=Flying;\
        {W}{W}: Target creature gains indestructible until end of turn.
timing=main
oracle=Flying\n{W}{W}: Target creature gains indestructible until end of turn.
