name=Frost Raptor
image=https://cards.scryfall.io/border_crop/front/4/a/4ab2f81a-fcbe-44d1-8281-04dd78bb9ea3.jpg?1593274931
value=3.012
rarity=C
type=Snow,Creature
subtype=Bird
cost={2}{U}
pt=2/2
ability=Flying;\
        {S}{S}: SN gains shroud until end of turn.
timing=main
oracle=Flying\n{S}{S}: Frost Raptor gains shroud until end of turn.
