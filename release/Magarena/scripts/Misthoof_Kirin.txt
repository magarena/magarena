name=Misthoof Kirin
image=https://cards.scryfall.io/border_crop/front/c/c/ccef8595-96e8-4df3-aa5a-9fc8c5fa9a3d.jpg?1562793228
value=2.500
rarity=C
type=Creature
subtype=Kirin
cost={2}{W}
pt=2/1
ability=Flying, vigilance;\
        Megamorph {1}{W}
timing=main
oracle=Flying, vigilance\nMegamorph {1}{W}
