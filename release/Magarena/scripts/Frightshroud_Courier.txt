name=Frightshroud Courier
image=https://cards.scryfall.io/border_crop/front/4/a/4a0fa75a-a82b-44cd-965f-07e0fe7a111a.jpg?1562912314
value=2.833
rarity=U
type=Creature
subtype=Zombie
cost={2}{B}
pt=2/1
ability=You may choose not to untap SN during your untap step.
timing=main
requires_groovy_code
oracle=You may choose not to untap Frightshroud Courier during your untap step.\n{2}{B}, {T}: Target Zombie creature gets +2/+2 and has fear for as long as Frightshroud Courier remains tapped.
