name=Airdrop Condor
image=https://cards.scryfall.io/border_crop/front/e/c/ec9796ac-11e2-4295-bf00-f684d0111970.jpg?1562951282
value=2.519
rarity=U
type=Creature
subtype=Bird
cost={4}{R}
pt=2/2
ability=Flying
timing=main
requires_groovy_code
oracle=Flying\n{1}{R}, Sacrifice a Goblin creature: Airdrop Condor deals damage equal to the sacrificed creature's power to target creature or player.
