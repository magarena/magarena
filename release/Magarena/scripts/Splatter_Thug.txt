name=Splatter Thug
image=https://cards.scryfall.io/border_crop/front/8/0/80656711-0e36-4bf6-9ba2-ef4fbdfa433a.jpg?1562850632
image_updated=2017-04-12
value=3.074
rarity=C
type=Creature
subtype=Human,Warrior
cost={2}{R}
pt=2/2
ability=First strike;\
        Unleash
timing=main
oracle=First strike\nUnleash
