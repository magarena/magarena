name=Daru Lancer
image=https://cards.scryfall.io/border_crop/front/c/d/cd888ca8-0ebe-46f0-9317-3b193ccc43fb.jpg?1562943531
value=2.683
rarity=C
type=Creature
subtype=Human,Soldier
cost={4}{W}{W}
pt=3/4
ability=First strike;\
        Morph {2}{W}{W}
timing=main
oracle=First strike\nMorph {2}{W}{W}
