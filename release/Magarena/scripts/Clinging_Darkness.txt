name=Clinging Darkness
image=https://cards.scryfall.io/border_crop/front/2/a/2a939a4e-5a9e-454f-8716-cee7470f05e2.jpg?1598914701
value=3.065
rarity=C
type=Enchantment
subtype=Aura
cost={1}{B}
ability=Enchant creature;\
        Enchanted creature gets -4/-1.
timing=aura
enchant=weaken -4/-1,neg creature
oracle=Enchant creature\nEnchanted creature gets -4/-1.
