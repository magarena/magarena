name=Trailblazer's Boots
image=https://cards.scryfall.io/border_crop/front/a/f/afeb5a30-016a-465f-bdf0-e2df2d1bef8c.jpg?1592757830
value=3.109
rarity=U
type=Artifact
subtype=Equipment
cost={2}
ability=Equipped creature has nonbasic landwalk.;\
        Equip {2}
timing=equipment
oracle=Equipped creature has nonbasic landwalk.\nEquip {2}
