name=Aethersphere Harvester
image=https://cards.scryfall.io/border_crop/front/4/1/4160bb5f-4b49-4535-94f5-776d7abd1d1a.jpg?1576382193
value=2.500
rarity=R
type=Artifact
subtype=Vehicle
cost={3}
pt=3/5
ability=Flying;\
        When SN enters the battlefield, you get {E}{E}.;\
        Pay {E}: SN gains lifelink until end of turn.;\
        Crew 1
timing=artifact
oracle=Flying\nWhen Aethersphere Harvester enters the battlefield, you get {E}{E}.\nPay {E}: Aethersphere Harvester gains lifelink until end of turn.\nCrew 1
