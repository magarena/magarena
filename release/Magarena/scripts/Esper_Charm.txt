name=Esper Charm
image=https://cards.scryfall.io/border_crop/front/6/2/62add4fe-c19e-40bd-82f6-cb1307a331b1.jpg?1592711092
value=3.836
rarity=U
type=Instant
cost={W}{U}{B}
effect=Choose one — (1) Destroy target enchantment. (2) Draw two cards. (3) Target player discards two cards.
timing=removal
oracle=Choose one —\n• Destroy target enchantment.\n• Draw two cards.\n• Target player discards two cards.
