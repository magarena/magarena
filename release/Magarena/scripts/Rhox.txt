name=Rhox
image=https://cards.scryfall.io/border_crop/front/6/a/6aa50126-f6ec-4917-ac45-4b1f8f23ca09.jpg?1562549251
value=4.295
rarity=R
type=Creature
subtype=Rhino,Beast
cost={4}{G}{G}
pt=5/5
timing=main
ability={2}{G}: Regenerate SN.
requires_groovy_code
oracle=You may have Rhox assign its combat damage as though it weren't blocked.\n{2}{G}: Regenerate Rhox.
