name=Worm Harvest
image=https://cards.scryfall.io/border_crop/front/3/6/36b5c006-115f-441e-84d4-fbff390a2c00.jpg?1592711180
image_updated=2017-08-17
value=3.870
rarity=R
type=Sorcery
cost={2}{B/G}{B/G}{B/G}
ability=Retrace
effect=Create a 1/1 black and green Worm creature token for each land card in your graveyard.
timing=token
oracle=Create a 1/1 black and green Worm creature token for each land card in your graveyard.\nRetrace
