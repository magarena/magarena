name=Blitz Hellion
image=https://cards.scryfall.io/border_crop/front/0/b/0b4a16be-ab3b-4020-9f26-631c0ee6730a.jpg?1562639575
value=3.302
rarity=R
type=Creature
subtype=Hellion
cost={3}{R}{G}
pt=7/7
ability=Trample, haste;\
        At the beginning of the end step, SN's owner shuffles it into his or her library.
timing=fmain
oracle=Trample, haste\nAt the beginning of the end step, Blitz Hellion's owner shuffles it into his or her library.
