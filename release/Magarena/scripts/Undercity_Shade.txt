name=Undercity Shade
image=https://cards.scryfall.io/border_crop/front/6/6/6609aa2e-6832-4c20-beda-e5f2506bf9b2.jpg?1619741479
value=2.111
rarity=U
type=Creature
subtype=Shade
cost={4}{B}
pt=1/1
ability=Fear;\
        {B}: SN gets +1/+1 until end of turn.
timing=main
oracle=Fear\n{B}: Undercity Shade gets +1/+1 until end of turn.
