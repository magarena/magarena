name=Riot Devils
image=https://cards.scryfall.io/border_crop/front/c/d/cd35107b-6aaf-4fd8-bf1c-12b724d1482e.jpg?1562837220
value=2.158
rarity=C
type=Creature
subtype=Devil
cost={2}{R}
pt=2/3
timing=main
oracle=NONE
