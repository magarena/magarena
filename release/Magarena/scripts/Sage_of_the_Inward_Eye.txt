name=Sage of the Inward Eye
image=https://cards.scryfall.io/border_crop/front/2/9/297c336b-3f70-4518-b1f1-7b773774895d.jpg?1562784071
value=3.750
rarity=R
type=Creature
subtype=Djinn,Wizard
cost={2}{U}{R}{W}
pt=3/4
ability=Flying;\
        Whenever you cast a noncreature spell, creatures you control gain lifelink until end of turn.
timing=main
oracle=Flying\nWhenever you cast a noncreature spell, creatures you control gain lifelink until end of turn.
