name=Twisted Abomination
image=https://cards.scryfall.io/border_crop/front/f/f/ffae38ec-7712-4701-b072-9f962ec9c259.jpg?1599706072
image_updated=2018-04-27
value=3.750
rarity=C
type=Creature
subtype=Zombie,Mutant
cost={5}{B}
pt=5/3
ability={B}: Regenerate SN.;\
        Swampcycling {2}
timing=main
oracle={B}: Regenerate Twisted Abomination.\nSwampcycling {2}
