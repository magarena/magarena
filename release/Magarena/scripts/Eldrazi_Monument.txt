name=Eldrazi Monument
image=https://cards.scryfall.io/border_crop/front/d/5/d5439677-d3db-4083-8dd9-0d654016bd79.jpg?1592673603
image_updated=2016-02-29
value=4.065
rarity=M
type=Artifact
cost={5}
ability=Creatures you control get +1/+1;\
        Creatures you control have flying, indestructible;\
        At the beginning of your upkeep, sacrifice a creature. If you can't, sacrifice SN.
static=player
timing=artifact
oracle=Creatures you control get +1/+1 and have flying and indestructible.\nAt the beginning of your upkeep, sacrifice a creature. If you can't, sacrifice Eldrazi Monument.
