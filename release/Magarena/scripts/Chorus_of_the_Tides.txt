name=Chorus of the Tides
image=https://cards.scryfall.io/border_crop/front/6/e/6e576f7a-0390-4514-aaed-20406fe6acdf.jpg?1593091617
value=3.000
rarity=C
type=Creature
subtype=Siren
cost={3}{U}
pt=3/2
ability=Flying;\
        Whenever you cast a spell that targets SN, scry 1.
timing=main
oracle=Flying\nHeroic — Whenever you cast a spell that targets Chorus of the Tides, scry 1.
