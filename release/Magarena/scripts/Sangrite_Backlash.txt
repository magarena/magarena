name=Sangrite Backlash
image=https://cards.scryfall.io/border_crop/front/f/a/fa56b15e-d02d-4306-82a6-ced778fee90f.jpg?1562645217
value=2.889
rarity=C
type=Enchantment
subtype=Aura
cost={B/G}{R}
ability=Enchant creature;\
        Enchanted creature gets +3/-3.
timing=aura
enchant=weaken +3/-3,creature
oracle=Enchant creature\nEnchanted creature gets +3/-3.
