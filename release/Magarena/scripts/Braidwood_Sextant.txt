name=Braidwood Sextant
image=https://cards.scryfall.io/border_crop/front/1/6/16dc7634-8ef5-4a03-8276-7e1dae4244c2.jpg?1562443331
value=2.667
rarity=U
type=Artifact
cost={1}
ability={2}, {T}, Sacrifice SN: Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.
timing=artifact
oracle={2}, {T}, Sacrifice Braidwood Sextant: Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.
