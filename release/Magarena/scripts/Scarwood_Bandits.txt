name=Scarwood Bandits
image=https://cards.scryfall.io/border_crop/front/7/c/7ca0e682-22c1-4cd9-9283-fb91ef29a752.jpg?1562924394
value=3.361
rarity=R
type=Creature
subtype=Human,Rogue
cost={2}{G}{G}
pt=2/2
ability=Forestwalk
timing=main
requires_groovy_code
oracle=Forestwalk\n{2}{G}, {T}: Unless an opponent pays {2}, gain control of target artifact for as long as Scarwood Bandits remains on the battlefield.
