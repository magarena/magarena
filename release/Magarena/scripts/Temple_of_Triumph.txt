name=Temple of Triumph
image=https://cards.scryfall.io/border_crop/front/4/f/4f7eb4fa-8950-42e5-8695-b9c3bba91f7d.jpg?1682210760
value=3.518
rarity=R
type=Land
mana=r3w3
ability=SN enters the battlefield tapped.;\
        When SN enters the battlefield, scry 1.;\
        {T}: Add {R} or {W}.
timing=tapland
oracle=Temple of Triumph enters the battlefield tapped.\nWhen Temple of Triumph enters the battlefield, scry 1.\n{T}: Add {R} or {W}.
