name=Disciple of Malice
image=https://cards.scryfall.io/border_crop/front/7/4/74cc7ab0-a5db-4ae9-af9a-89fd5aaaab57.jpg?1562922559
value=3.000
rarity=C
type=Creature
subtype=Human,Cleric
cost={1}{B}
pt=1/2
ability=Protection from white;\
        Cycling {2}
timing=main
oracle=Protection from white\nCycling {2}
