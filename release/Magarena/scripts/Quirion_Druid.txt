name=Quirion Druid
image=https://cards.scryfall.io/border_crop/front/8/c/8ca5319a-5c26-487f-ba87-d317633122ba.jpg?1562278148
value=3.161
rarity=R
type=Creature
subtype=Elf,Druid
cost={2}{G}
pt=1/2
ability={G}, {T}: Target land becomes a 2/2 green creature that's still a land.
timing=main
oracle={G}, {T}: Target land becomes a 2/2 green creature that's still a land.
