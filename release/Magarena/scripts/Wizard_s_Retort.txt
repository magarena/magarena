name=Wizard's Retort
image=https://cards.scryfall.io/border_crop/front/9/7/979cd394-80ce-4559-9f3a-57cd84299182.jpg?1600699298
value=2.500
rarity=U
type=Instant
cost={1}{U}{U}
ability=This spell costs {1} less to cast if you control a Wizard.
effect=Counter target spell.
timing=counter
oracle=This spell costs {1} less to cast if you control a Wizard.\nCounter target spell.
