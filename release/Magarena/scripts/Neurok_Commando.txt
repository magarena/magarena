name=Neurok Commando
image=https://cards.scryfall.io/border_crop/front/a/a/aa7084f3-9335-401f-9a62-6f131351338d.jpg?1562613766
value=3.133
rarity=U
type=Creature
subtype=Human,Rogue
cost={1}{U}{U}
pt=2/1
ability=Shroud;\
        Whenever SN deals combat damage to a player, you may draw a card.
timing=main
oracle=Shroud\nWhenever Neurok Commando deals combat damage to a player, you may draw a card.
