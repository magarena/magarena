name=Dimir Infiltrator
image=https://cards.scryfall.io/border_crop/front/b/9/b94cc636-de31-41b2-9c35-0bdfcdd0c1c1.jpg?1562929536
value=3.712
rarity=C
type=Creature
subtype=Spirit
cost={U}{B}
pt=1/3
ability=SN can't be blocked.;\
        Transmute {1}{U}{B}
timing=main
oracle=Dimir Infiltrator can't be blocked.\nTransmute {1}{U}{B}
