name=Sift
image=https://cards.scryfall.io/border_crop/front/4/f/4f9fb948-86fb-497b-98bf-1f69eac9901e.jpg?1598304307
image_updated=2018-08-07
value=3.170
rarity=U
type=Sorcery
cost={3}{U}
effect=Draw three cards, then discard a card.
timing=draw
oracle=Draw three cards, then discard a card.
