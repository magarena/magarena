name=Stinging Barrier
image=https://cards.scryfall.io/border_crop/front/c/a/ca7f7cd5-4e91-474a-9f60-a66f3f462b1c.jpg?1562383032
value=3.333
rarity=C
type=Creature
subtype=Wall
cost={2}{U}{U}
pt=0/4
ability=Defender;\
        {U}, {T}: SN deals 1 damage to target creature or player.
timing=smain
oracle=Defender\n{U}, {T}: Stinging Barrier deals 1 damage to target creature or player.
