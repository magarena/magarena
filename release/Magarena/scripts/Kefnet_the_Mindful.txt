name=Kefnet the Mindful
image=https://cards.scryfall.io/border_crop/front/c/b/cbda3b24-f2a6-4bcf-9a0a-bab69ffa8069.jpg?1543675019
value=2.500
rarity=M
type=Legendary,Creature
subtype=God
cost={2}{U}
pt=5/5
ability=Flying, indestructible;\
        SN can't attack or block unless you have seven or more cards in hand.
timing=main
requires_groovy_code
oracle=Flying, indestructible\nKefnet the Mindful can't attack or block unless you have seven or more cards in hand.\n{3}{U}: Draw a card, then you may return a land you control to its owner's hand.
