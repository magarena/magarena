name=Dirgur Nemesis
image=https://cards.scryfall.io/border_crop/front/5/4/54c388de-c2c9-4975-9eac-86a8030a9884.jpg?1562786421
value=2.500
rarity=C
type=Creature
subtype=Serpent
cost={5}{U}
pt=6/5
ability=Defender;\
        Megamorph {6}{U}
timing=smain
oracle=Defender\nMegamorph {6}{U}
