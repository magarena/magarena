name=Bog Wreckage
image=https://cards.scryfall.io/border_crop/front/1/8/189b4925-b34b-43bd-869e-1b1db99450e6.jpg?1562899643
value=2.229
rarity=C
type=Land
mana=w1u1b3r1g1
ability=SN enters the battlefield tapped.;\
        {T}: Add {B}.;\
        {T}, Sacrifice SN: Add one mana of any color.
timing=tapland
oracle=Bog Wreckage enters the battlefield tapped.\n{T}: Add {B}.\n{T}, Sacrifice Bog Wreckage: Add one mana of any color.
