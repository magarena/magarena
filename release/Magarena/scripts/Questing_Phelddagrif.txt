name=Questing Phelddagrif
image=https://cards.scryfall.io/border_crop/front/c/e/cea4cfef-6736-42a5-9f3e-10de8d0cd8d3.jpg?1562938708
value=4.243
rarity=R
type=Creature
subtype=Phelddagrif
cost={1}{G}{W}{U}
pt=4/4
timing=main
requires_groovy_code
oracle={G}: Questing Phelddagrif gets +1/+1 until end of turn. Target opponent creates a 1/1 green Hippo creature token.\n{W}: Questing Phelddagrif gains protection from black and from red until end of turn. Target opponent gains 2 life.\n{U}: Questing Phelddagrif gains flying until end of turn. Target opponent may draw a card.
