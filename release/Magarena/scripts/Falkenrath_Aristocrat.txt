name=Falkenrath Aristocrat
image=https://cards.scryfall.io/border_crop/front/5/7/57ac7f19-bf6f-4a87-ad39-6eb2553d2202.jpg?1599707911
image_updated=2017-08-08
value=3.856
rarity=R
type=Creature
subtype=Vampire
cost={2}{B}{R}
pt=4/1
ability=Flying, haste
timing=fmain
requires_groovy_code
oracle=Flying, haste\nSacrifice a creature: Falkenrath Aristocrat gains indestructible until end of turn. If the sacrificed creature was a Human, put a +1/+1 counter on Falkenrath Aristocrat.
