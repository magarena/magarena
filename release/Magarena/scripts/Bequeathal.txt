name=Bequeathal
image=https://cards.scryfall.io/border_crop/front/2/0/20aae577-9683-4d9b-bfd5-52702b38d3a7.jpg?1562087361
value=4.034
rarity=C
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant creature;\
        When enchanted creature dies, you draw two cards.
timing=aura
enchant=pump,creature
oracle=Enchant creature\nWhen enchanted creature dies, you draw two cards.
