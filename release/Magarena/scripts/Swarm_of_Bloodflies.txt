name=Swarm of Bloodflies
image=https://cards.scryfall.io/border_crop/front/d/9/d9fb56fc-47a4-44ba-8b55-4d0ceb8ce62f.jpg?1600701631
value=3.000
rarity=U
type=Creature
subtype=Insect
cost={4}{B}
pt=0/0
ability=Flying;\
        SN enters the battlefield with two +1/+1 counters on it.;\
        Whenever another creature dies, put a +1/+1 counter on SN.
timing=main
oracle=Flying\nSwarm of Bloodflies enters the battlefield with two +1/+1 counters on it.\nWhenever another creature dies, put a +1/+1 counter on Swarm of Bloodflies.
