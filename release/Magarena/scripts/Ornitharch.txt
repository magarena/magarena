name=Ornitharch
image=https://cards.scryfall.io/border_crop/front/f/9/f95a0bf4-10c7-4afa-8cf6-e31196cc2bd5.jpg?1593091541
value=3.888
rarity=U
type=Creature
subtype=Archon
cost={3}{W}{W}
pt=3/3
ability=Flying;\
        Tribute 2 effect create two 1/1 white Bird creature tokens with flying.
timing=main
oracle=Flying\nTribute 2\nWhen Ornitharch enters the battlefield, if tribute wasn't paid, create two 1/1 white Bird creature tokens with flying.
