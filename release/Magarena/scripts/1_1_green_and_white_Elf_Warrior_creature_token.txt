name=1/1 green and white Elf Warrior creature token
token=Elf Warrior
image=https://api.scryfall.com/cards/tshm/12/en?format=image&version=border_crop
value=2
type=Creature
subtype=Elf,Warrior
color=gw
pt=1/1
oracle=NONE
