name=Geier Reach Bandit
image=https://cards.scryfall.io/border_crop/front/4/5/4570117d-c04b-4bdc-b804-21d49154721b.jpg?1576384759
value=2.500
rarity=R
type=Creature
subtype=Human,Rogue,Werewolf
cost={2}{R}
pt=3/2
ability=Haste;\
        At the beginning of each upkeep, if no spells were cast last turn, transform SN.
transform=Vildin-Pack Alpha
timing=fmain
oracle=Haste\nAt the beginning of each upkeep, if no spells were cast last turn, transform Geier Reach Bandit.
