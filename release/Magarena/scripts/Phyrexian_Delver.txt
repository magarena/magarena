name=Phyrexian Delver
image=https://cards.scryfall.io/border_crop/front/8/6/86f6f3e9-b594-49da-b0af-75a672590da1.jpg?1682209185
value=4.237
rarity=R
type=Creature
subtype=Zombie
cost={3}{B}{B}
pt=3/2
timing=main
requires_groovy_code
oracle=When Phyrexian Delver enters the battlefield, return target creature card from your graveyard to the battlefield. You lose life equal to that card's converted mana cost.
