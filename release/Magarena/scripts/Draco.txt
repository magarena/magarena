name=Draco
image=https://cards.scryfall.io/border_crop/front/2/1/212e3edb-62f1-4680-884f-70323547f8ad.jpg?1562902012
value=4.235
rarity=R
type=Artifact,Creature
subtype=Dragon
cost={16}
pt=9/9
ability=Flying
timing=main
oracle=Domain — Draco costs {2} less to cast for each basic land type among lands you control.\nFlying\nDomain — At the beginning of your upkeep, sacrifice Draco unless you pay {10}. This cost is reduced by {2} for each basic land type among lands you control.
requires_groovy_code
