name=Dross Golem
image=https://cards.scryfall.io/border_crop/front/1/d/1df2f4fd-b96a-4537-93a4-38141b0fa577.jpg?1562635714
value=3.774
rarity=C
type=Artifact,Creature
subtype=Golem
cost={5}
pt=3/2
ability=Affinity for Swamps;\
        Fear
timing=main
oracle=Affinity for Swamps\nFear
