name=Anointer Priest
image=https://cards.scryfall.io/border_crop/front/4/f/4f922231-bf41-47d3-8fa3-13b9baf4a0f3.jpg?1543674572
value=2.500
rarity=C
type=Creature
subtype=Human,Cleric
cost={1}{W}
pt=1/3
ability=Whenever a creature token enters the battlefield under your control, you gain 1 life.;\
        Embalm {3}{W}
timing=main
oracle=Whenever a creature token enters the battlefield under your control, you gain 1 life.\nEmbalm {3}{W}
