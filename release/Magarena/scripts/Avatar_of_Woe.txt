name=Avatar of Woe
image=https://cards.scryfall.io/border_crop/front/7/e/7e8e34de-4b0a-48fe-bb13-c7e181d37e6c.jpg?1562274562
image_updated=2017-08-29
value=4.158
rarity=M
type=Creature
subtype=Avatar
cost={6}{B}{B}
pt=6/5
ability=Fear;\
        {T}: Destroy target creature. It can't be regenerated.
timing=main
oracle=If there are ten or more creature cards total in all graveyards, this spell costs {6} less to cast.\nFear\n{T}: Destroy target creature. It can't be regenerated.
requires_groovy_code
