name=Cao Ren, Wei Commander
image=https://cards.scryfall.io/border_crop/front/4/f/4f7e8366-82ba-4df0-b9df-7d0aa9b972eb.jpg?1562256092
value=2.080
rarity=R
type=Legendary,Creature
subtype=Human,Soldier,Warrior
cost={2}{B}{B}
pt=3/3
ability=Horsemanship;\
        When SN enters the battlefield, you lose 3 life.
timing=main
oracle=Horsemanship\nWhen Cao Ren, Wei Commander enters the battlefield, you lose 3 life.
