name=Kithkin Rabble
image=https://cards.scryfall.io/border_crop/front/7/f/7f9a0afa-50e6-4bce-b261-4559fd31295e.jpg?1562832303
value=3.357
rarity=U
type=Creature
subtype=Kithkin
cost={3}{W}
pt=*/*
ability=Vigilance;\
        SN's power and toughness are each equal to the number of white permanents you control.
timing=main
oracle=Vigilance\nKithkin Rabble's power and toughness are each equal to the number of white permanents you control.
