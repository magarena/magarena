name=Gorilla Pack
image=https://cards.scryfall.io/border_crop/front/0/4/046f6b76-5f17-4728-aa34-72b7eff1d4c9.jpg?1587912012
value=1.875
rarity=C
type=Creature
subtype=Ape
cost={2}{G}
pt=3/3
ability=SN can't attack unless defending player controls a Forest.;\
        When you control no Forests, sacrifice SN.
timing=main
oracle=Gorilla Pack can't attack unless defending player controls a Forest.\nWhen you control no Forests, sacrifice Gorilla Pack.
