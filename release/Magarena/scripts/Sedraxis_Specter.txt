name=Sedraxis Specter
image=https://cards.scryfall.io/border_crop/front/8/9/89a0fe75-cfaf-483e-8412-9ffb5d96ae25.jpg?1673149115
image_updated=2017-08-08
value=4.117
rarity=U
type=Creature
subtype=Specter
cost={U}{B}{R}
pt=3/2
ability=Flying;\
        Whenever SN deals combat damage to a player, that player discards a card.;\
        Unearth {1}{B}
timing=main
oracle=Flying\nWhenever Sedraxis Specter deals combat damage to a player, that player discards a card.\nUnearth {1}{B}
