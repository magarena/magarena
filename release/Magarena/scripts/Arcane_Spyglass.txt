name=Arcane Spyglass
image=https://cards.scryfall.io/border_crop/front/d/1/d10d85c9-859a-4ff5-9a41-bf20622a3ff5.jpg?1562639716
value=1.664
rarity=C
type=Artifact
cost={4}
ability={2}, {T}, Sacrifice a land: Draw a card.~Put a charge counter on SN.;\
        Remove three charge counters from SN: Draw a card.
timing=artifact
oracle={2}, {T}, Sacrifice a land: Draw a card and put a charge counter on Arcane Spyglass.\nRemove three charge counters from Arcane Spyglass: Draw a card.
