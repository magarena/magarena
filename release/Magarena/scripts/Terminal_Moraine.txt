name=Terminal Moraine
image=https://cards.scryfall.io/border_crop/front/3/5/353a8ea8-3f1f-4f77-95bc-b09b96996285.jpg?1562906215
value=3.347
rarity=U
type=Land
ability={T}: Add {C}.;\
        {2}, {T}, Sacrifice SN: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.
timing=land
oracle={T}: Add {C}.\n{2}, {T}, Sacrifice Terminal Moraine: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.
