name=Dungeon Shade
image=https://cards.scryfall.io/border_crop/front/3/e/3e2f317a-578c-4f7d-a185-b22bf7c32624.jpg?1562428902
value=3.534
rarity=C
type=Creature
subtype=Shade,Spirit
cost={3}{B}
pt=1/1
ability=Flying;\
        {B}: SN gets +1/+1 until end of turn.
timing=main
oracle=Flying\n{B}: Dungeon Shade gets +1/+1 until end of turn.
