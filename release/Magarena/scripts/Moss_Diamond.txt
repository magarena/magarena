name=Moss Diamond
image=https://cards.scryfall.io/border_crop/front/9/6/96fcae3a-d58c-4832-8280-3f65b5dfd853.jpg?1674137954
value=2.545
rarity=U
type=Artifact
cost={2}
ability=SN enters the battlefield tapped.;\
        {T}: Add {G}.
timing=artifact
oracle=Moss Diamond enters the battlefield tapped.\n{T}: Add {G}.
