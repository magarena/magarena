name=Paragon of Gathering Mists
image=https://cards.scryfall.io/border_crop/front/5/a/5afc32ed-c1a9-4c37-bfe0-562818c046fc.jpg?1562787493
value=3.167
rarity=U
type=Creature
subtype=Human,Wizard
cost={3}{U}
pt=2/2
ability=Other blue creatures you control get +1/+1.;\
        {U}, {T}: Another target blue creature you control gains flying until end of turn.
timing=main
oracle=Other blue creatures you control get +1/+1.\n{U}, {T}: Another target blue creature you control gains flying until end of turn.
