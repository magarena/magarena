name=Dominus of Fealty
image=https://cards.scryfall.io/border_crop/front/9/1/91433559-ad01-4bde-b0c0-f9cbb3589e89.jpg?1592714018
value=4.090
rarity=R
type=Creature
subtype=Spirit,Avatar
cost={U/R}{U/R}{U/R}{U/R}{U/R}
pt=4/4
ability=Flying;\
        At the beginning of your upkeep, you may gain control of target permanent until end of turn.~If you do, untap *it*~and *it* gains haste until end of turn.
timing=main
oracle=Flying\nAt the beginning of your upkeep, you may gain control of target permanent until end of turn. If you do, untap it and it gains haste until end of turn.
