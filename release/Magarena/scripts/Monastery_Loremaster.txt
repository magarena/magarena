name=Monastery Loremaster
image=https://cards.scryfall.io/border_crop/front/1/9/19b9d8de-5746-4af0-9353-a19fa977df41.jpg?1562783165
value=2.500
rarity=C
type=Creature
subtype=Djinn,Wizard
cost={3}{U}
pt=3/2
ability=Megamorph {5}{U};\
        When SN is turned face up, return target noncreature, nonland card from your graveyard to your hand.
timing=main
oracle=Megamorph {5}{U}\nWhen Monastery Loremaster is turned face up, return target noncreature, nonland card from your graveyard to your hand.
