name=Hellkite Igniter
image=https://cards.scryfall.io/border_crop/front/d/c/dc03ad3e-8113-432c-bff6-3ced73af8728.jpg?1682209373
image_updated=2017-08-17
value=3.439
rarity=R
type=Creature
subtype=Dragon
cost={5}{R}{R}
pt=5/5
ability=Flying, haste;\
        {1}{R}: SN gets +X/+0 until end of turn, where X is the number of artifacts you control.
timing=main
oracle=Flying, haste\n{1}{R}: Hellkite Igniter gets +X/+0 until end of turn, where X is the number of artifacts you control.
