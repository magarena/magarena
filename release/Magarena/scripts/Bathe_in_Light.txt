name=Bathe in Light
image=https://cards.scryfall.io/border_crop/front/d/1/d1d6dc01-278e-44cc-a488-7e3c2f2e0861.jpg?1592672330
value=3.000
rarity=U
type=Instant
cost={1}{W}
timing=pump
requires_groovy_code
oracle=Radiance — Choose a color. Target creature and each other creature that shares a color with it gain protection from the chosen color until end of turn.
