name=Wild Research
image=https://cards.scryfall.io/border_crop/front/8/f/8f00e6f1-e854-40b0-855d-7e0d7d233850.jpg?1562928707
value=3.839
rarity=R
type=Enchantment
cost={2}{R}
ability={1}{W}: Search your library for an enchantment card, reveal it, and put it into your hand, then shuffle your library~and discard a card at random.;\
        {1}{U}: Search your library for an instant card, reveal it, and put it into your hand, then shuffle your library~and discard a card at random.
timing=enchantment
oracle={1}{W}: Search your library for an enchantment card and reveal that card. Put it into your hand, then discard a card at random. Then shuffle your library.\n{1}{U}: Search your library for an instant card and reveal that card. Put it into your hand, then discard a card at random. Then shuffle your library.
