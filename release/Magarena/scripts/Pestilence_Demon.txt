name=Pestilence Demon
image=https://cards.scryfall.io/border_crop/front/0/5/058eb32f-2ae2-4276-ae1a-242bbb150418.jpg?1562229264
value=4.098
rarity=R
type=Creature
subtype=Demon
cost={5}{B}{B}{B}
pt=7/6
ability=Flying;\
        {B}: SN deals 1 damage to each creature and each player.
timing=main
oracle=Flying\n{B}: Pestilence Demon deals 1 damage to each creature and each player.
