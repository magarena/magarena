name=Spiraling Embers
image=https://cards.scryfall.io/border_crop/front/7/3/73503423-30d8-42bc-a697-6d986e4945c8.jpg?1562494235
value=3.085
rarity=C
type=Sorcery
subtype=Arcane
cost={3}{R}
effect=SN deals damage to target creature or player equal to the number of cards in your hand.
timing=main
oracle=Spiraling Embers deals damage to target creature or player equal to the number of cards in your hand.
