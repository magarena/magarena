name=2/2 black Vampire creature token with flying
token=Vampire
image=https://api.scryfall.com/cards/tktk/5/en?format=image&version=border_crop
value=2
type=Creature
subtype=Vampire
color=b
pt=2/2
ability=Flying
oracle=Flying
