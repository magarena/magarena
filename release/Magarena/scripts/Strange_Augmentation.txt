name=Strange Augmentation
image=https://cards.scryfall.io/border_crop/front/8/d/8d9b9847-7ef7-4fcc-ba16-8f2859d53fe6.jpg?1576384420
image_updated=2016-09-05
value=2.500
rarity=C
type=Enchantment
subtype=Aura
cost={B}
ability=Enchant creature;\
        Enchanted creature gets +1/+1.;\
        Enchanted creature gets +2/+2 as long as there are four or more card types among cards in your graveyard.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +1/+1.\nDelirium — Enchanted creature gets an additional +2/+2 as long as there are four or more card types among cards in your graveyard.
