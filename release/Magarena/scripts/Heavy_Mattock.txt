name=Heavy Mattock
image=https://cards.scryfall.io/border_crop/front/8/b/8b09df01-0b3e-463e-bf00-0a9f1822261a.jpg?1562927762
value=1.174
rarity=C
type=Artifact
subtype=Equipment
cost={3}
ability=Equipped creature gets +1/+1.;\
        As long as equipped creature is a Human, equipped creature gets +1/+1.;\
        Equip {2}
timing=equipment
oracle=Equipped creature gets +1/+1.\nAs long as equipped creature is a Human, it gets an additional +1/+1.\nEquip {2}
