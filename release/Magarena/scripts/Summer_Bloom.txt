name=Summer Bloom
image=https://cards.scryfall.io/border_crop/front/7/3/7383f71d-3f51-4632-9ea0-27d47fa2fa50.jpg?1673148198
value=3.891
rarity=U
type=Sorcery
cost={1}{G}
timing=main
requires_groovy_code
oracle=You may play up to three additional lands this turn.
