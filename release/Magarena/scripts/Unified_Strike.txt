name=Unified Strike
image=https://cards.scryfall.io/border_crop/front/2/9/29906eca-0823-4cd6-890f-e5b93cc50a11.jpg?1562904834
value=3.135
rarity=C
type=Instant
cost={W}
ability=Cast SN with AI only if there is a Soldier on the battlefield.
timing=attack
requires_groovy_code
oracle=Exile target attacking creature if its power is less than or equal to the number of Soldiers on the battlefield.
