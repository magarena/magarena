name=Narcissism
image=https://cards.scryfall.io/border_crop/front/e/e/eea658dd-7567-4b93-88a4-08b4ffb3dad7.jpg?1562632604
value=3.212
rarity=U
type=Enchantment
cost={2}{G}
ability={G}, Discard a card: Target creature gets +2/+2 until end of turn.;\
        {G}, Sacrifice SN: Target creature gets +2/+2 until end of turn.
timing=enchantment
oracle={G}, Discard a card: Target creature gets +2/+2 until end of turn.\n{G}, Sacrifice Narcissism: Target creature gets +2/+2 until end of turn.
