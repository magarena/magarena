name=Yellow Scarves General
image=https://cards.scryfall.io/border_crop/front/0/6/06e98691-6227-41f2-a3f4-3131b07a3a6f.jpg?1562255823
value=1.900
rarity=R
type=Creature
subtype=Human,Soldier
cost={3}{R}
pt=2/2
ability=Horsemanship;\
        SN can't block.
timing=main
oracle=Horsemanship\nYellow Scarves General can't block.
