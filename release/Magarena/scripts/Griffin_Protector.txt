name=Griffin Protector
image=https://cards.scryfall.io/border_crop/front/1/0/10d1d701-841c-4bf3-9122-b01842a22ac6.jpg?1673306440
image_updated=2018-04-27
value=2.806
rarity=C
type=Creature
subtype=Griffin
cost={3}{W}
pt=2/3
ability=Flying;\
        Whenever another creature enters the battlefield under your control, SN gets +1/+1 until end of turn.
timing=main
oracle=Flying\nWhenever another creature enters the battlefield under your control, Griffin Protector gets +1/+1 until end of turn.
