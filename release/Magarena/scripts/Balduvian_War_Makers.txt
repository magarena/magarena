name=Balduvian War-Makers
image=https://cards.scryfall.io/border_crop/front/1/2/12fd561e-6a26-4140-a033-1204f5dda5f3.jpg?1562767778
value=2.438
rarity=C
type=Creature
subtype=Human,Barbarian
cost={4}{R}
pt=3/3
ability=Haste;\
        Rampage 1
timing=fmain
oracle=Haste\nRampage 1
