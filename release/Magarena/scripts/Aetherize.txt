name=Aetherize
image=https://cards.scryfall.io/border_crop/front/a/0/a022eacd-1cdd-42d7-8a4c-88e8a6527144.jpg?1604197364
image_updated=2017-04-12
value=3.752
rarity=U
type=Instant
cost={3}{U}
ability=Cast SN with AI only if there is an attacking creature on the battlefield.
effect=Return all attacking creatures to their owner's hand.
timing=removal
oracle=Return all attacking creatures to their owner's hand.
