name=3/3 green Ooze creature token
token=Ooze
image=https://cards.scryfall.io/border_crop/front/6/2/623a7905-3a10-4d23-96fa-4960cac8f4e3.jpg?1562086871
value=3
type=Creature
subtype=Ooze
color=g
pt=3/3
