name=Trigon of Mending
image=https://cards.scryfall.io/border_crop/front/2/4/241142e0-3a79-4bce-8535-18ae7e392f5e.jpg?1562815529
value=1.787
rarity=U
type=Artifact
cost={2}
ability=SN enters the battlefield with three charge counters on it.;\
        {W}{W}, {T}: Put a charge counter on SN.;\
        {2}, {T}, Remove a charge counter from SN: Target player gains 3 life.
timing=artifact
oracle=Trigon of Mending enters the battlefield with three charge counters on it.\n{W}{W}, {T}: Put a charge counter on Trigon of Mending.\n{2}, {T}, Remove a charge counter from Trigon of Mending: Target player gains 3 life.
