name=Ancient Ooze
image=https://cards.scryfall.io/border_crop/front/3/b/3b57b41c-f99c-4525-8541-f025b7e31974.jpg?1562527613
value=3.736
rarity=R
type=Creature
subtype=Ooze
cost={5}{G}{G}
pt=*/*
timing=main
requires_groovy_code
oracle=Ancient Ooze's power and toughness are each equal to the total converted mana cost of other creatures you control.
