name=Lyzolda, the Blood Witch
image=https://cards.scryfall.io/border_crop/front/1/8/18df285f-4cb9-4998-bd26-eefbe28f80c7.jpg?1593273653
value=3.854
rarity=R
type=Legendary,Creature
subtype=Human,Cleric
cost={1}{B}{R}
pt=3/1
timing=main
requires_groovy_code
oracle={2}, Sacrifice a creature: Lyzolda, the Blood Witch deals 2 damage to target creature or player if the sacrificed creature was red. Draw a card if the sacrificed creature was black.
