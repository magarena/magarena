name=Vessel of Endless Rest
image=https://cards.scryfall.io/border_crop/front/9/3/93ec86a5-7d2f-4413-919e-b4f7c70457df.jpg?1547518540
value=3.511
rarity=U
type=Artifact
cost={3}
ability=When SN enters the battlefield, put target card from a graveyard on the bottom of its owner's library.;\
        {T}: Add one mana of any color.
timing=artifact
oracle=When Vessel of Endless Rest enters the battlefield, put target card from a graveyard on the bottom of its owner's library.\n{T}: Add one mana of any color.
