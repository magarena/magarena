name=Thornweald Archer
image=https://cards.scryfall.io/border_crop/front/f/6/f61b7811-ff6c-4767-921d-e8e88913e820.jpg?1619403587
value=4.197
rarity=C
type=Creature
subtype=Elf,Archer
cost={1}{G}
pt=2/1
ability=Reach;\
        Deathtouch
timing=main
oracle=Reach\nDeathtouch
