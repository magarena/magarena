name=Dusk Charger
image=https://cards.scryfall.io/border_crop/front/c/2/c2ce54a3-f3eb-48a0-bfa2-5f4b90e413f0.jpg?1555040200
value=2.500
rarity=C
type=Creature
subtype=Horse
cost={3}{B}
pt=3/3
ability=Ascend;\
        SN gets +2/+2 as long as you have the city's blessing.
timing=main
oracle=Ascend\nDusk Charger gets +2/+2 as long as you have the city's blessing.
