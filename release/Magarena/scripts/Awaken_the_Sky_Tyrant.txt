name=Awaken the Sky Tyrant
image=https://cards.scryfall.io/border_crop/front/5/c/5c3d57d1-1031-45e0-97d1-8e8e3e6ab516.jpg?1562704774
image_updated=2016-02-29
value=2.500
rarity=R
type=Enchantment
cost={3}{R}
timing=enchantment
oracle=When a source an opponent controls deals damage to you, sacrifice Awaken the Sky Tyrant. If you do, create a 5/5 red Dragon creature token with flying.
requires_groovy_code
