name=Chain Lightning
image=https://cards.scryfall.io/border_crop/front/b/7/b7cef88c-0ad6-47c4-b6c8-f989586aa635.jpg?1601077393
value=4.000
rarity=U
type=Sorcery
cost={R}
timing=main
requires_groovy_code
oracle=Chain Lightning deals 3 damage to target creature or player. Then that player or that creature's controller may pay {R}{R}. If the player does, he or she may copy this spell and may choose a new target for that copy.
