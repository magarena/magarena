name=Wall of Tanglecord
image=https://cards.scryfall.io/border_crop/front/7/9/792e2aed-ce6e-4fa1-a31c-a4574e5cf1f5.jpg?1562819262
value=3.764
rarity=C
type=Artifact,Creature
subtype=Wall
cost={2}
pt=0/6
ability=Defender;\
        {G}: SN gains reach until end of turn.
timing=smain
oracle=Defender\n{G}: Wall of Tanglecord gains reach until end of turn.
