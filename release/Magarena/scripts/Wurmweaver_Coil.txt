name=Wurmweaver Coil
image=https://cards.scryfall.io/border_crop/front/6/6/661580bc-b5e5-48ce-b855-c177f1c61742.jpg?1593272552
value=3.326
rarity=R
type=Enchantment
subtype=Aura
cost={4}{G}{G}
ability=Enchant green creature;\
        Enchanted creature gets +6/+6.;\
        {G}{G}{G}, Sacrifice SN: Create a 6/6 green Wurm creature token.
timing=aura
enchant=pump,pos creature
oracle=Enchant green creature\nEnchanted creature gets +6/+6.\n{G}{G}{G}, Sacrifice Wurmweaver Coil: Create a 6/6 green Wurm creature token.
