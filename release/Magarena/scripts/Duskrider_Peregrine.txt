name=Duskrider Peregrine
image=https://cards.scryfall.io/border_crop/front/3/2/321ff631-54f0-4501-852a-569790d05cf1.jpg?1619392843
value=2.700
rarity=U
type=Creature
subtype=Bird
cost={5}{W}
pt=3/3
ability=Flying, protection from black;\
        Suspend 3—{1}{W}
timing=main
oracle=Flying, protection from black\nSuspend 3—{1}{W}
