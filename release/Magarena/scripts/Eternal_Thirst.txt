name=Eternal Thirst
image=https://cards.scryfall.io/border_crop/front/5/b/5bbc405d-0cfb-45c6-baa1-75845d82e713.jpg?1600714433
image_updated=2017-04-12
value=2.750
rarity=C
type=Enchantment
subtype=Aura
cost={1}{B}
ability=Enchant creature;\
        Enchanted creature has lifelink and "Whenever a creature an opponent controls dies, put a +1/+1 counter on this creature."
timing=aura
enchant=lifelink,pos creature
oracle=Enchant creature\nEnchanted creature has lifelink and "Whenever a creature an opponent controls dies, put a +1/+1 counter on this creature."
