name=Aboroth
image=https://cards.scryfall.io/border_crop/front/8/c/8c72ac67-e4fb-49a1-b1e5-cd2e414bec28.jpg?1562802060
value=3.258
rarity=R
type=Creature
subtype=Elemental
cost={4}{G}{G}
pt=9/9
timing=main
requires_groovy_code
oracle=Cumulative upkeep—Put a -1/-1 counter on Aboroth.
