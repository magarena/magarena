name=Stern Mentor
image=https://cards.scryfall.io/border_crop/front/f/f/ffe4d34f-68f0-4d79-9aab-58c5304224d9.jpg?1592708704
value=2.515
rarity=U
type=Creature
subtype=Human,Wizard
cost={3}{U}
pt=2/2
ability=Soulbond;\
        As long as SN is paired with another creature, each of those creatures has "{T}: Target player puts the top two cards of his or her library into his or her graveyard."
timing=main
oracle=Soulbond\nAs long as Stern Mentor is paired with another creature, each of those creatures has "{T}: Target player puts the top two cards of his or her library into his or her graveyard."
