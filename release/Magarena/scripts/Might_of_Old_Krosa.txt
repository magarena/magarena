name=Might of Old Krosa
image=https://cards.scryfall.io/border_crop/front/e/d/ed0c7c36-2b15-463b-a023-683972303a0a.jpg?1673148146
image_updated=2017-08-08
value=4.134
rarity=U
type=Instant
cost={G}
timing=pump
oracle=Target creature gets +2/+2 until end of turn. If you cast this spell during your main phase, that creature gets +4/+4 until end of turn instead.
requires_groovy_code
