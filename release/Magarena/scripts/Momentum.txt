name=Momentum
image=https://cards.scryfall.io/border_crop/front/1/0/10bd11a2-7cab-4d3f-b52b-f5bb66fbbec6.jpg?1562443324
value=2.819
rarity=U
type=Enchantment
subtype=Aura
cost={2}{G}
ability=Enchant creature;\
        At the beginning of your upkeep, you may put a growth counter on SN.;\
        Enchanted creature gets +1/+1 for each growth counter on SN.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nAt the beginning of your upkeep, you may put a growth counter on Momentum.\nEnchanted creature gets +1/+1 for each growth counter on Momentum.
