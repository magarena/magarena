name=Hell's Caretaker
image=https://cards.scryfall.io/border_crop/front/8/4/84a65ccf-37b6-48a3-9873-7a4cafef27cb.jpg?1562437592
image_updated=2018-04-27
value=4.376
rarity=R
type=Creature
subtype=Horror
cost={3}{B}
pt=1/1
ability={T}, Sacrifice a creature: Return target creature card from your graveyard to the battlefield. Activate this ability only during your upkeep.
timing=main
oracle={T}, Sacrifice a creature: Return target creature card from your graveyard to the battlefield. Activate this ability only during your upkeep.
