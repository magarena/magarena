name=Urza's Guilt
image=https://cards.scryfall.io/border_crop/front/d/4/d429233e-1cf9-4f87-b191-894a73e7a876.jpg?1562939878
value=3.630
rarity=R
type=Sorcery
cost={2}{U}{B}
timing=main
requires_groovy_code
oracle=Each player draws two cards, then discards three cards, then loses 4 life.
