name=Resilient Khenra
image=https://cards.scryfall.io/border_crop/front/0/3/033a69fb-2af9-426c-b1d6-3e5b18c78c9a.jpg?1562787658
value=2.500
rarity=R
type=Creature
subtype=Jackal,Wizard
cost={1}{G}
pt=2/2
ability=When SN enters the battlefield, you may have target creature get +X/+X until end of turn, where X is SN's power.;\
        Eternalize {4}{G}{G}
timing=main
oracle=When Resilient Khenra enters the battlefield, you may have target creature get +X/+X until end of turn, where X is Resilient Khenra's power.\nEternalize {4}{G}{G}
