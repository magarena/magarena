name=Frightcrawler
image=https://cards.scryfall.io/border_crop/front/d/7/d79129e7-6cc4-4060-8b76-3afbd2e0d456.jpg?1562935312
value=3.058
rarity=C
type=Creature
subtype=Horror
cost={1}{B}
pt=1/1
ability=Fear;\
        As long as seven or more cards are in your graveyard, SN gets +2/+2 and can't block.
timing=main
oracle=Fear\nThreshold — As long as seven or more cards are in your graveyard, Frightcrawler gets +2/+2 and can't block.
