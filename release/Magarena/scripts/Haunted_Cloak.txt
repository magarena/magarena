name=Haunted Cloak
image=https://cards.scryfall.io/border_crop/front/9/f/9fc9a637-352a-45bd-a43a-6406f9da56af.jpg?1608911454
value=2.500
rarity=U
type=Artifact
subtype=Equipment
cost={3}
ability=Equipped creature has vigilance, trample, and haste.;\
        Equip {1}
timing=equipment
oracle=Equipped creature has vigilance, trample, and haste.\nEquip {1}
