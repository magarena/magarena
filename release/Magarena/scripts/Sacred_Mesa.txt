name=Sacred Mesa
image=https://cards.scryfall.io/border_crop/front/3/7/377b2bc1-c710-439d-96d2-beef72ae80a7.jpg?1561938614
value=4.075
rarity=R
type=Enchantment
cost={2}{W}
ability=At the beginning of your upkeep, sacrifice SN unless you sacrifice a Pegasus.;\
        {1}{W}: Create a 1/1 white Pegasus creature token with flying.
timing=enchantment
oracle=At the beginning of your upkeep, sacrifice Sacred Mesa unless you sacrifice a Pegasus.\n{1}{W}: Create a 1/1 white Pegasus creature token with flying.
