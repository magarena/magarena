name=Trusted Advisor
image=https://cards.scryfall.io/border_crop/front/e/e/ee63cba4-ab12-4adb-8463-d42edbf5794d.jpg?1562496591
value=3.661
rarity=U
type=Creature
subtype=Human,Advisor
cost={U}
pt=1/2
ability=At the beginning of your upkeep, return a blue creature you control to its owner's hand.
timing=main
requires_groovy_code
oracle=Your maximum hand size is increased by two.\nAt the beginning of your upkeep, return a blue creature you control to its owner's hand.
