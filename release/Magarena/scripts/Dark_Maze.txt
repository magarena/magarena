name=Dark Maze
image=https://cards.scryfall.io/border_crop/front/7/4/74fe0349-a114-4bb8-a9ae-97d0f8f46ddb.jpg?1562591836
value=2.429
rarity=C
type=Creature
subtype=Wall
cost={4}{U}
pt=4/5
ability=Defender
timing=smain
requires_groovy_code
oracle=Defender\n{0}: Dark Maze can attack this turn as though it didn't have defender. Exile it at the beginning of the next end step.
