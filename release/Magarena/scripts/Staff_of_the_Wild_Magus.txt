name=Staff of the Wild Magus
image=https://cards.scryfall.io/border_crop/front/e/6/e64f31e8-0b49-4510-a4df-2e0bea793d3e.jpg?1562796077
value=0.929
rarity=U
type=Artifact
cost={3}
ability=Whenever a Forest enters the battlefield under your control, you gain 1 life.;\
        Whenever you cast a green spell, you gain 1 life.
timing=artifact
oracle=Whenever you cast a green spell or a Forest enters the battlefield under your control, you gain 1 life.
