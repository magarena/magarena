name=Oros, the Avenger
image=https://cards.scryfall.io/border_crop/front/f/c/fc4d7aa2-ee1b-435c-8876-44111fafc97a.jpg?1592673434
value=4.026
rarity=R
type=Legendary,Creature
subtype=Dragon
cost={3}{W}{B}{R}
pt=6/6
ability=Flying;\
        Whenever SN deals combat damage to a player, you may pay {2}{W}. If you do, SN deals 3 damage to each nonwhite creature.
timing=main
oracle=Flying\nWhenever Oros, the Avenger deals combat damage to a player, you may pay {2}{W}. If you do, Oros deals 3 damage to each nonwhite creature.
