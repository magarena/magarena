name=Hell-Bent Raider
image=https://cards.scryfall.io/border_crop/front/7/f/7f41ac1c-2603-4234-8963-cd47bb894024.jpg?1562630610
value=3.786
rarity=R
type=Creature
subtype=Human,Barbarian
cost={1}{R}{R}
pt=2/2
ability=First strike, haste;\
        Discard a card at random: SN gains protection from white until end of turn.
timing=main
oracle=First strike, haste\nDiscard a card at random: Hell-Bent Raider gains protection from white until end of turn.
