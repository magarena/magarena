name=Guul Draz Assassin
image=https://cards.scryfall.io/border_crop/front/7/2/72de01ef-cdf2-44ad-bf2f-a927d82a4f72.jpg?1562705055
value=3.917
rarity=R
type=Creature
subtype=Vampire,Assassin
cost={B}
pt=1/1
ability=level up {1}{B} 4
timing=main
requires_groovy_code
oracle=Level up {1}{B}\nLEVEL 2-3\n2/2\n{B}, {T}: Target creature gets -2/-2 until end of turn.\nLEVEL 4+\n4/4\n{B}, {T}: Target creature gets -4/-4 until end of turn.
