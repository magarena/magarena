name=Ashling, the Extinguisher
image=https://cards.scryfall.io/border_crop/front/3/5/350580d8-45db-4428-a292-cc4802bf1e4d.jpg?1562906171
value=4.033
rarity=R
type=Legendary,Creature
subtype=Elemental,Shaman
cost={2}{B}{B}
pt=4/4
timing=main
requires_groovy_code
oracle=Whenever Ashling, the Extinguisher deals combat damage to a player, choose target creature that player controls. He or she sacrifices that creature.
