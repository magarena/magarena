name=Kingpin's Pet
image=https://cards.scryfall.io/border_crop/front/3/4/3465cf63-4f10-4b53-9703-69746364dbc7.jpg?1561822414
value=3.507
rarity=C
type=Creature
subtype=Thrull
cost={1}{W}{B}
pt=2/2
ability=Flying;\
        Extort
timing=main
oracle=Flying\nExtort
