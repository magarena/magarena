name=Torpid Moloch
image=https://cards.scryfall.io/border_crop/front/7/9/7900ff91-0e47-4903-a680-9031f4a23cb4.jpg?1598915932
value=3.613
rarity=C
type=Creature
subtype=Lizard
cost={R}
pt=3/2
ability=Defender;\
        Sacrifice three lands: SN loses defender until end of turn.
timing=smain
oracle=Defender\nSacrifice three lands: Torpid Moloch loses defender until end of turn.
