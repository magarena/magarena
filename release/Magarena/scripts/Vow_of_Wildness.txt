name=Vow of Wildness
image=https://cards.scryfall.io/border_crop/front/7/6/764fa7f1-b92b-42cc-983e-e0b5457369a7.jpg?1608910977
value=3.606
rarity=U
type=Enchantment
subtype=Aura
cost={2}{G}
ability=Enchant creature;\
        Enchanted creature gets +3/+3 and has trample.;\
        As long as it's not your turn, enchanted creature can't attack.
timing=aura
enchant=default,creature
oracle=Enchant creature\nEnchanted creature gets +3/+3, has trample, and can't attack you or a planeswalker you control.
