name=Tormented Thoughts
image=https://cards.scryfall.io/border_crop/front/6/b/6be2be08-5bad-4813-b6f3-45b564e10b2f.jpg?1562024490
image_updated=2015-11-05
value=2.500
rarity=U
type=Sorcery
cost={2}{B}
ability=As an additional cost to cast SN, sacrifice a creature.
timing=main
requires_groovy_code
oracle=As an additional cost to cast Tormented Thoughts, sacrifice a creature.\nTarget player discards a number of cards equal to the sacrificed creature's power.
