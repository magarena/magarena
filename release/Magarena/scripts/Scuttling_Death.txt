name=Scuttling Death
image=https://cards.scryfall.io/border_crop/front/7/f/7f68a90c-1afb-43d3-aed2-4704f4599e75.jpg?1562264271
value=1.141
rarity=C
type=Creature
subtype=Spirit
cost={4}{B}
pt=4/2
ability=Sacrifice SN: Target creature gets -1/-1 until end of turn.;\
        Soulshift 4
timing=main
oracle=Sacrifice Scuttling Death: Target creature gets -1/-1 until end of turn.\nSoulshift 4
