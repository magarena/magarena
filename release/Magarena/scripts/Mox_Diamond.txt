name=Mox Diamond
image=https://cards.scryfall.io/border_crop/front/b/f/bf9fecfd-d122-422f-bd0a-5bf69b434dfe.jpg?1562431287
value=2.500
rarity=R
type=Artifact
cost={0}
ability={T}: Add one mana of any color.
timing=artifact
oracle=If Mox Diamond would enter the battlefield, you may discard a land card instead. If you do, put Mox Diamond onto the battlefield. If you don't, put it into its owner's graveyard.\n{T}: Add one mana of any color.
requires_groovy_code
