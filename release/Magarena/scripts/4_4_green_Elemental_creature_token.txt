name=4/4 green Elemental creature token
token=Elemental
image=https://cards.scryfall.io/border_crop/front/f/e/fea0857b-0f9e-4a87-83d7-85723e33f26c.jpg?1560081229
value=4
type=Creature
subtype=Elemental
color=g
pt=4/4
