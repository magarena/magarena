name=Drinker of Sorrow
image=https://cards.scryfall.io/border_crop/front/2/b/2bc8758b-68cc-45ab-85d0-b870cef7dd85.jpg?1562903921
value=2.905
rarity=R
type=Creature
subtype=Horror
cost={2}{B}
pt=5/3
ability=SN can't block.;\
        Whenever SN deals combat damage, sacrifice a permanent.
timing=main
oracle=Drinker of Sorrow can't block.\nWhenever Drinker of Sorrow deals combat damage, sacrifice a permanent.
