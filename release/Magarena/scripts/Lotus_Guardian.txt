name=Lotus Guardian
image=https://cards.scryfall.io/border_crop/front/d/d/ddfc6396-5377-4ab3-9c10-8abcdeae2aa1.jpg?1562939672
value=2.199
rarity=R
type=Artifact,Creature
subtype=Dragon
cost={7}
pt=4/4
ability=Flying;\
        {T}: Add one mana of any color.
timing=main
oracle=Flying\n{T}: Add one mana of any color.
