name=Keeper of the Flame
image=https://cards.scryfall.io/border_crop/front/9/b/9bf246ca-9dfc-400f-8883-acc80ac016e1.jpg?1562088327
value=3.138
rarity=U
type=Creature
subtype=Human,Wizard
cost={R}{R}
pt=1/2
timing=main
requires_groovy_code
oracle={R}, {T}: Choose target opponent who had more life than you did as you activated this ability. Keeper of the Flame deals 2 damage to him or her.
