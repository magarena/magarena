name=Order of Yawgmoth
image=https://cards.scryfall.io/border_crop/front/4/9/49e5ff9f-9ec9-4c96-b28f-938a2abd15bf.jpg?1562908507
image_updated=2017-01-03
value=4.114
rarity=U
type=Creature
subtype=Zombie,Knight
cost={2}{B}{B}
pt=2/2
ability=Fear;\
        Whenever SN deals damage to a player, that player discards a card.
timing=main
oracle=Fear\nWhenever Order of Yawgmoth deals damage to a player, that player discards a card.
