name=Dictate of Kruphix
image=https://cards.scryfall.io/border_crop/front/a/a/aa9f176b-6d33-4510-9ba2-98bdd12ba34e.jpg?1592710552
value=3.788
rarity=R
type=Enchantment
cost={1}{U}{U}
ability=Flash;\
        At the beginning of each player's draw step, that player draws an additional card.
timing=flash
oracle=Flash\nAt the beginning of each player's draw step, that player draws an additional card.
