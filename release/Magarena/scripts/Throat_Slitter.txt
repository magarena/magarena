name=Throat Slitter
image=https://cards.scryfall.io/border_crop/front/8/7/87126165-2cba-4a24-a3fc-3b5487e89d13.jpg?1562920020
value=3.115
rarity=U
type=Creature
subtype=Rat,Ninja
cost={4}{B}
pt=2/2
ability=Ninjutsu {2}{B};\
        Whenever SN deals combat damage to a player, destroy target nonblack creature that player controls.
timing=main
oracle=Ninjutsu {2}{B}\nWhenever Throat Slitter deals combat damage to a player, destroy target nonblack creature that player controls.
