name=Viashino Sandscout
image=https://cards.scryfall.io/border_crop/front/1/0/102f139d-1baf-4ca6-a940-70b7efdc29f2.jpg?1562543863
value=3.114
rarity=C
type=Creature
subtype=Viashino,Scout
cost={1}{R}
pt=2/1
ability=Haste;\
        At the beginning of the end step, return SN to its owner's hand.
timing=fmain
oracle=Haste\nAt the beginning of the end step, return Viashino Sandscout to its owner's hand.
