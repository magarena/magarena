name=Giant Strength
image=https://cards.scryfall.io/border_crop/front/e/a/eaa8fde3-d887-4b52-85ec-0100e7ccdb03.jpg?1562942158
value=2.906
rarity=C
type=Enchantment
subtype=Aura
cost={R}{R}
ability=Enchant creature;\
        Enchanted creature gets +2/+2.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +2/+2.
