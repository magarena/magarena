name=Runewing
image=https://cards.scryfall.io/border_crop/front/7/4/749961e6-b135-4629-ae9d-124de0d70db9.jpg?1562788308
value=2.337
rarity=C
type=Creature
subtype=Bird
cost={3}{U}
pt=2/2
ability=Flying;\
        When SN dies, draw a card.
timing=main
oracle=Flying\nWhen Runewing dies, draw a card.
