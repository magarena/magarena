name=Moldgraf Monstrosity
image=https://cards.scryfall.io/border_crop/front/b/2/b20a1d49-699d-413e-985b-7890f3e7115e.jpg?1592710957
value=3.888
rarity=R
type=Creature
subtype=Insect
cost={4}{G}{G}{G}
pt=8/8
ability=Trample
timing=main
requires_groovy_code
oracle=Trample\nWhen Moldgraf Monstrosity dies, exile it, then return two creature cards at random from your graveyard to the battlefield.
