name=Swift Spinner
image=https://cards.scryfall.io/border_crop/front/2/6/264f78d8-d554-45f3-867e-19dba38e4644.jpg?1576384898
image_updated=2016-09-05
value=2.500
rarity=C
type=Creature
subtype=Spider
cost={3}{G}
pt=2/3
ability=Flash;\
        Reach
timing=flash
oracle=Flash\nReach
