name=Weatherseed Treefolk
image=https://cards.scryfall.io/border_crop/front/f/4/f42cce45-3b6a-43e2-8329-68c30135c5c1.jpg?1562864248
value=4.399
rarity=R
type=Creature
subtype=Treefolk
cost={2}{G}{G}{G}
pt=5/3
ability=Trample;\
        When SN dies, return SN from the graveyard to its owner's hand.
timing=main
oracle=Trample\nWhen Weatherseed Treefolk dies, return it to its owner's hand.
