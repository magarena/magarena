name=Syphon Soul
image=https://cards.scryfall.io/border_crop/front/d/c/dc308882-1c12-4da0-8eae-caf4cc3d43e1.jpg?1562866895
value=3.667
rarity=C
type=Sorcery
cost={2}{B}
timing=main
requires_groovy_code
oracle=Syphon Soul deals 2 damage to each other player. You gain life equal to the damage dealt this way.
