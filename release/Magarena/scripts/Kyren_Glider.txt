name=Kyren Glider
image=https://cards.scryfall.io/border_crop/front/0/b/0bc55e01-342e-4856-937e-14561b8d165b.jpg?1562378966
value=1.817
rarity=C
type=Creature
subtype=Goblin
cost={1}{R}
pt=1/1
ability=Flying;\
        SN can't block.
timing=main
oracle=Flying\nKyren Glider can't block.
