name=Elephant Resurgence
image=https://cards.scryfall.io/border_crop/front/2/2/22147f72-7ff8-40c4-9bdd-df41dce17dad.jpg?1562901295
value=3.438
rarity=R
type=Sorcery
cost={1}{G}
ability=Cast SN with AI only if you have a creature card in your graveyard.
timing=main
requires_groovy_code
oracle=Each player creates a green Elephant creature token. Those creatures have "This creature's power and toughness are each equal to the number of creature cards in its controller's graveyard."
