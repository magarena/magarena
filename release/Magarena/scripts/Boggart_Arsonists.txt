name=Boggart Arsonists
image=https://cards.scryfall.io/border_crop/front/d/6/d6a837b6-c8b0-4971-ae95-0b49d38bc830.jpg?1562836588
value=2.363
rarity=C
type=Creature
subtype=Goblin,Rogue
cost={2}{R}
pt=2/1
ability=Plainswalk;\
        {2}{R}, Sacrifice SN: Destroy target Scarecrow or Plains.
timing=main
oracle=Plainswalk\n{2}{R}, Sacrifice Boggart Arsonists: Destroy target Scarecrow or Plains.
