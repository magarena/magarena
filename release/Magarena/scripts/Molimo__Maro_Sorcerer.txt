name=Molimo, Maro-Sorcerer
image=https://cards.scryfall.io/border_crop/front/3/2/321f68b5-42ad-4f71-8df4-78075bf68ce5.jpg?1608917482
image_updated=2016-12-23
value=4.043
rarity=R
type=Legendary,Creature
subtype=Elemental
cost={4}{G}{G}{G}
pt=*/*
ability=Trample;\
        SN's power and toughness are each equal to the number of lands you control.
timing=main
oracle=Trample\nMolimo, Maro-Sorcerer's power and toughness are each equal to the number of lands you control.
