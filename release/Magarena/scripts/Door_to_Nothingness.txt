name=Door to Nothingness
image=https://cards.scryfall.io/border_crop/front/5/7/57877b1c-e91d-4941-81bd-008dff1272ed.jpg?1562554053
value=3.924
rarity=R
type=Artifact
cost={5}
ability=SN enters the battlefield tapped.;\
        {W}{W}{U}{U}{B}{B}{R}{R}{G}{G}, {T}, Sacrifice SN: Target player loses the game.
timing=artifact
oracle=Door to Nothingness enters the battlefield tapped.\n{W}{W}{U}{U}{B}{B}{R}{R}{G}{G}, {T}, Sacrifice Door to Nothingness: Target player loses the game.
