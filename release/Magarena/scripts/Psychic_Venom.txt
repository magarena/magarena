name=Psychic Venom
image=https://cards.scryfall.io/border_crop/front/9/8/984ae881-9b43-42ce-885e-2eb5e683eed7.jpg?1559592351
value=3.717
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}
ability=Enchant land;\
        Whenever enchanted land becomes tapped, SN deals 2 damage to that land's controller.
timing=aura
enchant=default,neg land
oracle=Enchant land\nWhenever enchanted land becomes tapped, Psychic Venom deals 2 damage to that land's controller.
