name=Cruel Ultimatum
image=https://cards.scryfall.io/border_crop/front/d/7/d71a8ea5-199e-473e-a2b1-e2f7cc804ffc.jpg?1592765938
image_updated=2017-08-08
value=3.975
rarity=R
type=Sorcery
cost={U}{U}{B}{B}{B}{R}{R}
timing=main
requires_groovy_code
oracle=Target opponent sacrifices a creature, discards three cards, then loses 5 life. You return a creature card from your graveyard to your hand, draw three cards, then gain 5 life.
