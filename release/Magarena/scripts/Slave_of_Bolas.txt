name=Slave of Bolas
image=https://cards.scryfall.io/border_crop/front/b/3/b3947ce7-0a14-41d0-9b8f-ee6d62c460fa.jpg?1592765971
value=3.622
removal=5
rarity=U
type=Sorcery
cost={3}{U/R}{B}
effect=Gain control of target creature.~Untap *that creature*.~*It* gains haste until end of turn.~Sacrifice *it* at the beginning of the next end step.
timing=removal
oracle=Gain control of target creature. Untap that creature. It gains haste until end of turn. Sacrifice it at the beginning of the next end step.
