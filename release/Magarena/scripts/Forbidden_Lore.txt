name=Forbidden Lore
image=https://cards.scryfall.io/border_crop/front/3/6/366069fb-3322-4992-aa1e-858951a8c492.jpg?1562868076
value=2.775
rarity=R
type=Enchantment
subtype=Aura
cost={2}{G}
ability=Enchant land;\
        Enchanted land has "{T}: Target creature gets +2/+1 until end of turn."
enchant=pump,pos land
timing=aura
oracle=Enchant land\nEnchanted land has "{T}: Target creature gets +2/+1 until end of turn."
