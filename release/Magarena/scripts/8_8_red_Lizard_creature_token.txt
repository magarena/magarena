name=8/8 red Lizard creature token
token=Lizard
image=https://api.scryfall.com/cards/tcn2/9/en?format=image&version=border_crop
value=1
type=Creature
subtype=Lizard
color=r
pt=8/8
