name=Price of Progress
image=https://cards.scryfall.io/border_crop/front/b/1/b1342144-7a15-438b-a848-3196238a79e8.jpg?1580014614
value=4.570
rarity=U
type=Instant
cost={1}{R}
ability=Cast SN with AI only if an opponent controls a nonbasic land.
timing=removal
requires_groovy_code
oracle=Price of Progress deals damage to each player equal to twice the number of nonbasic lands that player controls.
