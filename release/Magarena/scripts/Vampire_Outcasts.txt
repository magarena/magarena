name=Vampire Outcasts
image=https://cards.scryfall.io/border_crop/front/3/2/32c1d966-b3c5-4480-be58-054a88141e44.jpg?1562260638
value=2.962
rarity=U
type=Creature
subtype=Vampire
cost={2}{B}{B}
pt=2/2
ability=Bloodthirst 2;\
        Lifelink
timing=main
oracle=Bloodthirst 2\nLifelink
