name=Ogre's Cleaver
image=https://cards.scryfall.io/border_crop/front/e/c/ec319380-bede-49e8-9d0c-473688033601.jpg?1562709979
value=2.892
rarity=U
type=Artifact
subtype=Equipment
cost={2}
ability=Equipped creature gets +5/+0.;\
        Equip {5}
timing=equipment
oracle=Equipped creature gets +5/+0.\nEquip {5}
