name=Briarberry Cohort
image=https://cards.scryfall.io/border_crop/front/c/a/caf9d87a-533c-4641-9738-e7b98d92b015.jpg?1562835967
value=2.608
rarity=C
type=Creature
subtype=Faerie,Soldier
cost={1}{U}
pt=1/1
ability=Flying;\
        SN gets +1/+1 as long as you control another blue creature.
timing=main
oracle=Flying\nBriarberry Cohort gets +1/+1 as long as you control another blue creature.
