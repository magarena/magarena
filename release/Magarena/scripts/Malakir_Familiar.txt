name=Malakir Familiar
image=https://cards.scryfall.io/border_crop/front/2/4/24fe5c92-7da2-47b5-ad13-f95590e93ac2.jpg?1600700814
image_updated=2015-11-08
value=2.500
rarity=U
type=Creature
subtype=Bat
cost={2}{B}
pt=2/1
ability=Flying, deathtouch;\
        Whenever you gain life, SN gets +1/+1 until end of turn.
timing=main
oracle=Flying, deathtouch\nWhenever you gain life, Malakir Familiar gets +1/+1 until end of turn.
