name=Mazirek, Kraul Death Priest
image=https://cards.scryfall.io/border_crop/front/e/7/e7fb8520-1bc4-40e7-a4cc-2933ed7e0c00.jpg?1599708195
image_updated=2016-02-29
value=2.500
rarity=M
type=Legendary,Creature
subtype=Insect,Shaman
cost={3}{B}{G}
pt=2/2
ability=Flying;\
        Whenever a player sacrifices another permanent, put a +1/+1 counter on each creature you control.
timing=main
oracle=Flying\nWhenever a player sacrifices another permanent, put a +1/+1 counter on each creature you control.
