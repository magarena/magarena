name=Silklash Spider
image=https://cards.scryfall.io/border_crop/front/0/e/0edd614b-f85f-476c-bf55-ad2db5c30cd7.jpg?1592673182
value=2.500
rarity=R
type=Creature
subtype=Spider
cost={3}{G}{G}
pt=2/7
ability=Reach;\
        {X}{G}{G}: SN deals X damage to each creature with flying.
timing=main
oracle=Reach\n{X}{G}{G}: Silklash Spider deals X damage to each creature with flying.
