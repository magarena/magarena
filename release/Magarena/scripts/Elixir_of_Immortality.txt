name=Elixir of Immortality
image=https://cards.scryfall.io/border_crop/front/6/7/6741ab27-9e1f-4aa5-96b9-b450eda7c5c2.jpg?1625978606
value=3.516
rarity=U
type=Artifact
cost={1}
timing=artifact
requires_groovy_code
oracle={2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into their owner's library.
