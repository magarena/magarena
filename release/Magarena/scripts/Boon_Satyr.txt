name=Boon Satyr
image=https://cards.scryfall.io/border_crop/front/4/7/477a30f3-3cbe-42a5-9fae-fe5885a8cf25.jpg?1592710816
value=3.535
rarity=R
type=Enchantment,Creature
subtype=Satyr
cost={1}{G}{G}
pt=4/2
ability=Flash;\
        Bestow {3}{G}{G};\
        Enchanted creature gets +4/+2.
timing=pumpFlash
oracle=Flash\nBestow {3}{G}{G}\nEnchanted creature gets +4/+2.
