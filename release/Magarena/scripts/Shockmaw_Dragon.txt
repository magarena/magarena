name=Shockmaw Dragon
image=https://cards.scryfall.io/border_crop/front/e/4/e4810919-b379-40db-a68b-a769dd3ee32d.jpg?1562830882
value=2.500
rarity=U
type=Creature
subtype=Dragon
cost={4}{R}{R}
pt=4/4
ability=Flying;\
        Whenever SN deals combat damage to a player, SN deals 1 damage to each creature that player controls.
timing=main
oracle=Flying\nWhenever Shockmaw Dragon deals combat damage to a player, it deals 1 damage to each creature that player controls.
