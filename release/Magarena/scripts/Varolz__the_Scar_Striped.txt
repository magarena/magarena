name=Varolz, the Scar-Striped
image=https://cards.scryfall.io/border_crop/front/4/c/4c3ae3db-c14a-4ffc-805c-a3a51da9370d.jpg?1562908954
value=3.908
rarity=R
type=Legendary,Creature
subtype=Troll,Warrior
cost={1}{B}{G}
pt=2/2
ability=Sacrifice another creature: Regenerate SN.
timing=main
requires_groovy_code
oracle=Each creature card in your graveyard has scavenge. The scavenge cost is equal to its mana cost.\nSacrifice another creature: Regenerate Varolz, the Scar-Striped.
#Scavenge should function as a static ability
