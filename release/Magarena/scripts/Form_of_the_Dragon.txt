name=Form of the Dragon
image=https://cards.scryfall.io/border_crop/front/1/c/1cfc77f1-290a-4394-bc16-76f3659810f0.jpg?1562732212
value=4.547
rarity=R
type=Enchantment
cost={4}{R}{R}{R}
ability=At the beginning of your upkeep, SN deals 5 damage to target creature or player.;\
        At the beginning of each end step, your life total becomes 5.;\
        Creatures without flying you don't control can't attack.
timing=enchantment
oracle=At the beginning of your upkeep, Form of the Dragon deals 5 damage to target creature or player.\nAt the beginning of each end step, your life total becomes 5.\nCreatures without flying can't attack you.
