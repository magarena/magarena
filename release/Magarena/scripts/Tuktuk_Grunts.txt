name=Tuktuk Grunts
image=https://cards.scryfall.io/border_crop/front/e/6/e6f37982-d2ae-4ff1-b5da-3936733f8108.jpg?1562617804
value=1.927
rarity=C
type=Creature
subtype=Goblin,Warrior,Ally
cost={4}{R}
pt=2/2
ability=Haste;\
        Whenever SN or another Ally enters the battlefield under your control, you may put a +1/+1 counter on SN.
timing=fmain
oracle=Haste\nWhenever Tuktuk Grunts or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Tuktuk Grunts.
