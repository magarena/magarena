name=Eye of Singularity
image=https://cards.scryfall.io/border_crop/front/f/a/fa84e4ad-738a-4d23-a84c-06c39ff4200b.jpg?1562279120
value=4.186
rarity=R
type=World,Enchantment
cost={3}{W}
timing=enchantment
requires_groovy_code
oracle=When Eye of Singularity enters the battlefield, destroy each permanent with the same name as another permanent, except for basic lands. They can't be regenerated.\nWhenever a permanent other than a basic land enters the battlefield, destroy all other permanents with that name. They can't be regenerated.
