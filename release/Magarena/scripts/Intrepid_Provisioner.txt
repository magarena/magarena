name=Intrepid Provisioner
image=https://cards.scryfall.io/border_crop/front/d/a/da3ac63b-8386-4175-9555-84c54566f5b4.jpg?1576385134
value=2.500
rarity=C
type=Creature
subtype=Human,Scout
cost={3}{G}
pt=3/3
ability=Trample;\
        When SN enters the battlefield, another target Human you control gets +2/+2 until end of turn.
timing=main
oracle=Trample\nWhen Intrepid Provisioner enters the battlefield, another target Human you control gets +2/+2 until end of turn.
