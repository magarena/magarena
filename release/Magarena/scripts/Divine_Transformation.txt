name=Divine Transformation
image=https://cards.scryfall.io/border_crop/front/7/b/7b733fb5-3ece-452c-a360-330fd202bb0f.jpg?1559592582
value=2.574
rarity=U
type=Enchantment
subtype=Aura
cost={2}{W}{W}
ability=Enchant creature;\
        Enchanted creature gets +3/+3.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +3/+3.
