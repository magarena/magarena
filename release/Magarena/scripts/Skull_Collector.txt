name=Skull Collector
image=https://cards.scryfall.io/border_crop/front/0/d/0da4b1bf-eef5-4a86-a861-be448845a743.jpg?1562492477
value=3.234
rarity=U
type=Creature
subtype=Ogre,Warrior
cost={1}{B}{B}
pt=3/3
ability=At the beginning of your upkeep, return a black creature you control to its owner's hand.;\
        {1}{B}: Regenerate SN.
timing=main
oracle=At the beginning of your upkeep, return a black creature you control to its owner's hand.\n{1}{B}: Regenerate Skull Collector.
