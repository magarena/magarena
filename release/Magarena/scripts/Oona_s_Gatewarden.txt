name=Oona's Gatewarden
image=https://cards.scryfall.io/border_crop/front/9/7/972bb6e4-300c-49f5-8305-459a3ba67baa.jpg?1562833464
value=3.669
rarity=C
type=Creature
subtype=Faerie,Soldier
cost={U/B}
pt=2/1
ability=Defender, flying;\
        Wither
timing=smain
oracle=Defender, flying\nWither
