name=Dirty Wererat
image=https://cards.scryfall.io/border_crop/front/9/7/97d15534-310f-4f9d-be0d-59b0ed8a5f53.jpg?1562923205
value=2.823
rarity=C
type=Creature
subtype=Human,Rat,Minion
cost={3}{B}
pt=2/3
ability={B}, Discard a card: Regenerate SN.;\
        As long as seven or more cards are in your graveyard, SN gets +2/+2 and can't block.
timing=main
oracle={B}, Discard a card: Regenerate Dirty Wererat.\nThreshold — As long as seven or more cards are in your graveyard, Dirty Wererat gets +2/+2 and can't block.
