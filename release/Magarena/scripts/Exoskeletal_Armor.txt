name=Exoskeletal Armor
image=https://cards.scryfall.io/border_crop/front/e/1/e111fcab-17f7-4a02-b4eb-606ba18812b3.jpg?1562632336
value=3.806
rarity=U
type=Enchantment
subtype=Aura
cost={1}{G}
ability=Enchant creature;\
        Enchanted creature gets +X/+X, where X is the number of creature cards in all graveyards.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +X/+X, where X is the number of creature cards in all graveyards.
