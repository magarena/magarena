name=Wall of Fire
image=https://cards.scryfall.io/border_crop/front/6/f/6f8ac968-3d00-40d8-80b5-e6fe08025de2.jpg?1562788853
value=1.900
rarity=C
type=Creature
subtype=Wall
cost={1}{R}{R}
pt=0/5
ability=Defender;\
        {R}: SN gets +1/+0 until end of turn.
timing=smain
oracle=Defender\n{R}: Wall of Fire gets +1/+0 until end of turn.
