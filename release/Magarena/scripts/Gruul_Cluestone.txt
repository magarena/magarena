name=Gruul Cluestone
image=https://cards.scryfall.io/border_crop/front/b/c/bc47d1fe-8ab2-42f6-bcab-4bc2084ceba7.jpg?1562930185
value=2.719
rarity=C
type=Artifact
cost={3}
ability={T}: Add {R} or {G}.;\
        {R}{G}, {T}, Sacrifice SN: Draw a card.
timing=artifact
oracle={T}: Add {R} or {G}.\n{R}{G}, {T}, Sacrifice Gruul Cluestone: Draw a card.
