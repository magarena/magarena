name=Deathspore Thallid
image=https://cards.scryfall.io/border_crop/front/7/4/74c1b368-6fb0-4430-807e-d124f3077361.jpg?1619395285
value=3.877
rarity=C
type=Creature
subtype=Zombie,Fungus
cost={1}{B}
pt=1/1
ability=At the beginning of your upkeep, put a spore counter on SN.;\
        Remove three spore counters from SN: Create a 1/1 green Saproling creature token.;\
        Sacrifice a Saproling: Target creature gets -1/-1 until end of turn.
timing=main
oracle=At the beginning of your upkeep, put a spore counter on Deathspore Thallid.\nRemove three spore counters from Deathspore Thallid: Create a 1/1 green Saproling creature token.\nSacrifice a Saproling: Target creature gets -1/-1 until end of turn.
