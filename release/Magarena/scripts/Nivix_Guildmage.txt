name=Nivix Guildmage
image=https://cards.scryfall.io/border_crop/front/3/6/3669d445-197d-41ad-9ff4-1d5895378fb6.jpg?1562605063
value=3.900
rarity=U
type=Creature
subtype=Human,Wizard
cost={U}{R}
pt=2/2
ability={1}{U}{R}: Draw a card, then discard a card.;\
        {2}{U}{R}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.
timing=main
oracle={1}{U}{R}: Draw a card, then discard a card.\n{2}{U}{R}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.
