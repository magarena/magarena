name=Claim of Erebos
image=https://cards.scryfall.io/border_crop/front/a/5/a51db556-b3e8-4628-9e39-6353023b0696.jpg?1593091901
value=2.942
rarity=C
type=Enchantment
subtype=Aura
cost={1}{B}
ability=Enchant creature;\
        Enchanted creature has "{1}{B}, {T}: Target player loses 2 life."
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature has "{1}{B}, {T}: Target player loses 2 life."
