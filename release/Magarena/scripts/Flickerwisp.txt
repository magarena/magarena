name=Flickerwisp
image=https://cards.scryfall.io/border_crop/front/f/6/f6cccf30-2025-49bb-9b1e-240bbef03f27.jpg?1673146975
image_updated=2017-08-08
value=3.737
rarity=U
type=Creature
subtype=Elemental
cost={1}{W}{W}
pt=3/1
ability=Flying;\
        When SN enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner's control at the beginning of the next end step.
timing=main
oracle=Flying\nWhen Flickerwisp enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner's control at the beginning of the next end step.
