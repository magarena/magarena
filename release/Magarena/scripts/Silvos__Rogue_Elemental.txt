name=Silvos, Rogue Elemental
image=https://cards.scryfall.io/border_crop/front/f/1/f1175357-01fe-438c-93bc-09e29ed8c9cb.jpg?1580014919
value=4.500
rarity=R
type=Legendary,Creature
subtype=Elemental
cost={3}{G}{G}{G}
pt=8/5
ability=Trample;\
        {G}: Regenerate SN.
timing=main
oracle=Trample\n{G}: Regenerate Silvos, Rogue Elemental.
