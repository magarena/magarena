name=Rise of the Dark Realms
image=https://cards.scryfall.io/border_crop/front/7/c/7c0e1064-47c3-4f03-a1f2-3bcb356db82b.jpg?1600701321
value=3.625
rarity=M
type=Sorcery
cost={7}{B}{B}
timing=main
requires_groovy_code
oracle=Put all creature cards from all graveyards onto the battlefield under your control.
