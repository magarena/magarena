name=Briarpack Alpha
image=https://cards.scryfall.io/border_crop/front/5/8/585c11e2-4c30-436c-9dbd-354b154f6def.jpg?1562829524
value=3.588
rarity=U
type=Creature
subtype=Wolf
cost={3}{G}
pt=3/3
ability=Flash;\
        When SN enters the battlefield, target creature gets +2/+2 until end of turn.
timing=pumpflash
oracle=Flash\nWhen Briarpack Alpha enters the battlefield, target creature gets +2/+2 until end of turn.
