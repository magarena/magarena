name=Karn, Silver Golem
image=https://cards.scryfall.io/border_crop/front/0/5/055a00a2-0df2-4f49-ac51-e33dfaf7ee85.jpg?1562896049
value=4.500
rarity=R
type=Legendary,Artifact,Creature
subtype=Golem
cost={5}
pt=4/4
ability=Whenever SN blocks or becomes blocked, SN gets -4/+4 until end of turn.
timing=fmain
requires_groovy_code
oracle=Whenever Karn, Silver Golem blocks or becomes blocked, it gets -4/+4 until end of turn.\n{1}: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn.
