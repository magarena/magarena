name=Soul of Zendikar
image=https://cards.scryfall.io/border_crop/front/1/0/108a4330-d9dd-429d-a2f8-c355ff914883.jpg?1568004676
value=3.786
rarity=M
type=Creature
subtype=Avatar
cost={4}{G}{G}
pt=6/6
ability=Reach;\
        {3}{G}{G}: Create a 3/3 green Beast creature token.;\
        {3}{G}{G}, Exile SN from your graveyard: Create a 3/3 green Beast creature token.
timing=main
oracle=Reach\n{3}{G}{G}: Create a 3/3 green Beast creature token.\n{3}{G}{G}, Exile Soul of Zendikar from your graveyard: Create a 3/3 green Beast creature token.
