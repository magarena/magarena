name=Elfhame Palace
image=https://cards.scryfall.io/border_crop/front/1/d/1ddd0084-349d-4d5f-86af-a3a65dcce73a.jpg?1562601695
image_updated=2017-10-03
value=3.315
rarity=U
type=Land
mana=w3g3
ability=SN enters the battlefield tapped.;\
        {T}: Add {G} or {W}.
timing=tapland
oracle=Elfhame Palace enters the battlefield tapped.\n{T}: Add {G} or {W}.
