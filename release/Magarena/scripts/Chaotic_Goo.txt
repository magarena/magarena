name=Chaotic Goo
image=https://cards.scryfall.io/border_crop/front/0/e/0e9881a4-3078-4fe0-be09-54ddad1d18a0.jpg?1562052777
value=3.403
rarity=R
type=Creature
subtype=Ooze
cost={2}{R}{R}
pt=0/0
ability=SN enters the battlefield with three +1/+1 counters on it.;\
        At the beginning of your upkeep, you may flip a coin. If you win the flip, put a +1/+1 counter on SN. If you lose the flip, remove a +1/+1 counter from SN.
timing=main
oracle=Chaotic Goo enters the battlefield with three +1/+1 counters on it.\nAt the beginning of your upkeep, you may flip a coin. If you win the flip, put a +1/+1 counter on Chaotic Goo. If you lose the flip, remove a +1/+1 counter from Chaotic Goo.
