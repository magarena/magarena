name=Preferred Selection
image=https://cards.scryfall.io/border_crop/front/5/9/59ce9668-78ef-44a5-ba9c-49fa740b8cb5.jpg?1562719281
value=3.594
rarity=R
type=Enchantment
cost={2}{G}{G}
timing=enchantment
requires_groovy_code
oracle=At the beginning of your upkeep, look at the top two cards of your library. You may sacrifice Preferred Selection and pay {2}{G}{G}. If you do, put one of those cards into your hand. If you don't, put one of those cards on the bottom of your library.
