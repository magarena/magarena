name=Goblin Warchief
image=https://cards.scryfall.io/border_crop/front/5/b/5bac033c-dc4e-40a0-b103-4892e4b50249.jpg?1562736294
image_updated=2018-08-07
value=2.500
rarity=U
type=Creature
subtype=Goblin,Warrior
cost={1}{R}{R}
pt=2/2
ability=Goblin spells you cast cost {1} less to cast.;\
        Goblin creatures you control have haste.
timing=main
oracle=Goblin spells you cast cost {1} less to cast.\nGoblin creatures you control have haste.
