name=Dawnbringer Charioteers
image=https://cards.scryfall.io/border_crop/front/e/c/eca7b7d9-8b69-411a-8a1d-9b7d0492e7d0.jpg?1593095201
value=3.763
rarity=R
type=Creature
subtype=Human,Soldier
cost={2}{W}{W}
pt=2/4
ability=Flying, lifelink;\
        Whenever you cast a spell that targets SN, put a +1/+1 counter on SN.
timing=main
oracle=Flying, lifelink\nHeroic — Whenever you cast a spell that targets Dawnbringer Charioteers, put a +1/+1 counter on Dawnbringer Charioteers.
