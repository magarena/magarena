name=Neko-Te
image=https://cards.scryfall.io/border_crop/front/1/3/13884a26-1e30-40d7-ae38-494154a5cf85.jpg?1562875557
value=3.850
rarity=R
type=Artifact
subtype=Equipment
cost={3}
ability=Whenever equipped creature deals damage to a player, that player loses 1 life.;\
        Equip {2}
timing=equipment
requires_groovy_code
oracle=Whenever equipped creature deals damage to a creature, tap that creature. That creature doesn't untap during its controller's untap step for as long as Neko-Te remains on the battlefield.\nWhenever equipped creature deals damage to a player, that player loses 1 life.\nEquip {2}
