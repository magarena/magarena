name=Spitting Spider
image=https://cards.scryfall.io/border_crop/front/b/f/bfeeb515-7d3d-4678-a480-32485b197afb.jpg?1562933585
value=3.375
rarity=U
type=Creature
subtype=Spider
cost={3}{G}{G}
pt=3/5
ability=Reach;\
        Sacrifice a land: SN deals 1 damage to each creature with flying.
timing=main
oracle=Reach\nSacrifice a land: Spitting Spider deals 1 damage to each creature with flying.
