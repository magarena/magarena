name=Viridian Harvest
image=https://cards.scryfall.io/border_crop/front/6/6/666eb9a5-b105-45c1-be3e-7ac5cc650338.jpg?1562878314
value=1.777
rarity=C
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant artifact
timing=aura
enchant=default,artifact
requires_groovy_code
oracle=Enchant artifact\nWhen enchanted artifact is put into a graveyard, you gain 6 life.
