name=Baleful Eidolon
image=https://cards.scryfall.io/border_crop/front/8/5/853a89c6-63f9-4b12-8a5b-2c382e22b226.jpg?1562820831
value=3.047
rarity=C
type=Enchantment,Creature
subtype=Spirit
cost={1}{B}
pt=1/1
ability=Bestow {4}{B};\
        Deathtouch;\
        Enchanted creature gets +1/+1 and has deathtouch.
timing=main
oracle=Bestow {4}{B}\nDeathtouch\nEnchanted creature gets +1/+1 and has deathtouch.
