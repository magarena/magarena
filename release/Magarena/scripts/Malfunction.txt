name=Malfunction
image=https://cards.scryfall.io/border_crop/front/c/4/c41f907f-4512-4c5d-827b-fef04613d641.jpg?1576381303
image_updated=2016-10-05
value=2.500
rarity=C
type=Enchantment
subtype=Aura
cost={3}{U}
ability=Enchant artifact or creature;\
        When SN enters the battlefield, tap enchanted permanent.;\
        Enchanted permanent doesn't untap during its controller's untap step.
timing=aura
enchant=can't attack or block,neg artifact or creature
oracle=Enchant artifact or creature\nWhen Malfunction enters the battlefield, tap enchanted permanent.\nEnchanted permanent doesn't untap during its controller's untap step.
