name=Anathemancer
image=https://cards.scryfall.io/border_crop/front/a/b/ab106855-bd85-4c00-b596-c46d34f8cdd0.jpg?1562643508
value=3.478
rarity=U
type=Creature
subtype=Zombie,Wizard
cost={1}{B}{R}
pt=2/2
ability=Unearth {5}{B}{R}
timing=main
requires_groovy_code
oracle=When Anathemancer enters the battlefield, it deals damage to target player equal to the number of nonbasic lands that player controls.\nUnearth {5}{B}{R}
