name=Form of the Dinosaur
image=https://cards.scryfall.io/border_crop/front/4/1/41e4fe86-281d-4174-bb72-ac5bb9560b7e.jpg?1555040432
value=2.500
rarity=R
type=Enchantment
cost={4}{R}{R}
ability=When SN enters the battlefield, your life total becomes 15.
timing=enchantment
requires_groovy_code
oracle=When Form of the Dinosaur enters the battlefield, your life total becomes 15.\nAt the beginning of your upkeep, Form of the Dinosaur deals 15 damage to target creature an opponent controls and that creature deals damage equal to its power to you.
