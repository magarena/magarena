name=Tormented Angel
image=https://cards.scryfall.io/border_crop/front/0/0/00d4d751-50df-4d8f-a6d9-4e76797c429a.jpg?1562443313
value=2.350
rarity=C
type=Creature
subtype=Angel
cost={3}{W}
pt=1/5
ability=Flying
timing=main
oracle=Flying
