name=Ivory Giant
image=https://cards.scryfall.io/border_crop/front/c/1/c1b1b580-4a2c-41a4-a1e0-85b659b3a30e.jpg?1619392953
value=3.417
rarity=C
type=Creature
subtype=Giant
cost={5}{W}{W}
pt=3/4
ability=When SN enters the battlefield, tap all nonwhite creatures.;\
        Suspend 5—{W}
timing=main
oracle=When Ivory Giant enters the battlefield, tap all nonwhite creatures.\nSuspend 5—{W}
