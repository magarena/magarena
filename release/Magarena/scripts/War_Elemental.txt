name=War Elemental
image=https://cards.scryfall.io/border_crop/front/9/c/9cc32bfc-7d87-46d9-a424-eac64eefd7ea.jpg?1562152211
value=4.194
rarity=R
type=Creature
subtype=Elemental
cost={R}{R}{R}
pt=1/1
ability=Cast SN with AI only if an opponent was dealt damage this turn.
timing=main
requires_groovy_code
oracle=When War Elemental enters the battlefield, sacrifice it unless an opponent was dealt damage this turn.\nWhenever an opponent is dealt damage, put that many +1/+1 counters on War Elemental.
