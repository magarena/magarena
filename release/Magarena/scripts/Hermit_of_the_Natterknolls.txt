name=Hermit of the Natterknolls
image=https://cards.scryfall.io/border_crop/front/d/5/d5e84d80-b5a6-4cd1-9f31-f6ca46bf2c0c.jpg?1576385105
value=2.500
rarity=U
type=Creature
subtype=Human,Werewolf
cost={2}{G}
pt=2/3
ability=Whenever an opponent casts a spell, if it's your turn, draw a card.;\
        At the beginning of each upkeep, if no spells were cast last turn, transform SN.
transform=Lone Wolf of the Natterknolls
timing=main
oracle=Whenever an opponent casts a spell during your turn, draw a card.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Hermit of the Natterknolls.
