name=Abrupt Decay
image=https://cards.scryfall.io/border_crop/front/a/8/a8e328c6-3a84-49cf-a1a3-1d1e5373d274.jpg?1593814035
image_updated=2017-08-08
value=4.394
rarity=R
type=Instant
cost={B}{G}
ability=This spell can't be countered.
effect=Destroy target nonland permanent with converted mana cost 3 or less.
timing=removal
oracle=This spell can't be countered.\nDestroy target nonland permanent with converted mana cost 3 or less.
