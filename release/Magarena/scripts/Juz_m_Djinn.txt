name=Juzám Djinn
image=https://cards.scryfall.io/border_crop/front/b/0/b03bc922-782b-4254-897c-90d202b4cda4.jpg?1559592285
value=4.324
rarity=R
type=Creature
subtype=Djinn
cost={2}{B}{B}
pt=5/5
ability=At the beginning of your upkeep, SN deals 1 damage to you.
timing=main
oracle=At the beginning of your upkeep, Juzám Djinn deals 1 damage to you.
