name=Body Snatcher
image=https://cards.scryfall.io/border_crop/front/c/7/c7d4c858-5a11-485d-a514-12a6d80459f0.jpg?1562445201
value=4.534
rarity=R
type=Creature
subtype=Minion
cost={2}{B}{B}
pt=2/2
ability=When SN enters the battlefield, you may discard a creature card. If you don't, exile SN.;\
        When SN dies, return target creature card from your graveyard to the battlefield~and exile SN.
timing=main
oracle=When Body Snatcher enters the battlefield, exile it unless you discard a creature card.\nWhen Body Snatcher dies, exile Body Snatcher and return target creature card from your graveyard to the battlefield.
