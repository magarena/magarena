name=Caravan Escort
image=https://cards.scryfall.io/border_crop/front/8/1/817602f9-0844-4ba2-bd7c-380fcd0c15ff.jpg?1593095358
value=3.081
rarity=C
type=Creature
subtype=Human,Knight
cost={W}
pt=1/1
ability=level up {2} 5
timing=main
requires_groovy_code
oracle=Level up {2}\nLEVEL 1-4\n2/2\nLEVEL 5+\n5/5\nFirst strike
