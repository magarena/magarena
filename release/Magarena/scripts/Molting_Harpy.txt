name=Molting Harpy
image=https://cards.scryfall.io/border_crop/front/d/d/ddfe33fb-71d5-4552-bcd3-f07e4e3847e1.jpg?1562383497
value=2.092
rarity=U
type=Creature
subtype=Harpy,Mercenary
cost={B}
pt=2/1
ability=Flying;\
        At the beginning of your upkeep, sacrifice SN unless you pay {2}.
timing=main
oracle=Flying\nAt the beginning of your upkeep, sacrifice Molting Harpy unless you pay {2}.
