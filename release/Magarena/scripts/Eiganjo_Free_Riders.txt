name=Eiganjo Free-Riders
image=https://cards.scryfall.io/border_crop/front/f/e/fe47bbc5-6f7f-4135-9e83-9e2a428c97f5.jpg?1562497050
value=2.605
rarity=U
type=Creature
subtype=Human,Soldier
cost={3}{W}
pt=3/4
ability=Flying;\
        At the beginning of your upkeep, return a white creature you control to its owner's hand.
timing=main
oracle=Flying\nAt the beginning of your upkeep, return a white creature you control to its owner's hand.
