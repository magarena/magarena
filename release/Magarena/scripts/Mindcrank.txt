name=Mindcrank
image=https://cards.scryfall.io/border_crop/front/8/8/8858a941-8175-476d-8177-2db4ffdcb3ad.jpg?1674142661
image_updated=2017-04-12
value=3.521
rarity=U
type=Artifact
cost={2}
timing=artifact
requires_groovy_code
oracle=Whenever an opponent loses life, that player puts that many cards from the top of his or her library into his or her graveyard.
