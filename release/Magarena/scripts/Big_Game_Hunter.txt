name=Big Game Hunter
image=https://cards.scryfall.io/border_crop/front/2/c/2cec1014-375c-4877-95e3-093d24c02bc8.jpg?1619395072
value=4.333
rarity=U
type=Creature
subtype=Human,Rebel,Assassin
cost={1}{B}{B}
pt=1/1
ability=When SN enters the battlefield, destroy target creature with power 4 or greater. It can't be regenerated.;\
        Madness {B}
timing=main
oracle=When Big Game Hunter enters the battlefield, destroy target creature with power 4 or greater. It can't be regenerated.\nMadness {B}
