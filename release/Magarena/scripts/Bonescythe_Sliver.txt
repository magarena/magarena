name=Bonescythe Sliver
image=https://cards.scryfall.io/border_crop/front/a/2/a26bb68b-1830-470a-8cea-91edc7db0c57.jpg?1562833120
value=3.623
rarity=R
type=Creature
subtype=Sliver
cost={3}{W}
pt=2/2
ability=Sliver creatures you control have double strike.
static=player
timing=main
oracle=Sliver creatures you control have double strike.
