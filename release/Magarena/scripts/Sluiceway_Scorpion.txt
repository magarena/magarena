name=Sluiceway Scorpion
image=https://cards.scryfall.io/border_crop/front/7/b/7b6dbadf-a6f7-4876-9c3f-44e4a33b2bee.jpg?1562788611
value=3.246
rarity=C
type=Creature
subtype=Scorpion
cost={2}{B}{G}
pt=2/2
ability=Deathtouch;\
        Scavenge {1}{B}{G}
timing=main
oracle=Deathtouch\nScavenge {1}{B}{G}
