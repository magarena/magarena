name=Opalescence
image=https://cards.scryfall.io/border_crop/front/3/c/3c0071fb-afa5-47b5-b266-2b10a4f5a98a.jpg?1562443752
value=4.283
rarity=R
type=Enchantment
cost={2}{W}{W}
timing=enchantment
oracle=Each other non-Aura enchantment is a creature in addition to its other types and has base power and base toughness each equal to its converted mana cost.
requires_groovy_code
