name=Helium Squirter
image=https://cards.scryfall.io/border_crop/front/3/5/35a9a786-8d15-4186-8030-4151bddcc0f7.jpg?1562260681
value=2.872
rarity=C
type=Creature
subtype=Beast,Mutant
cost={4}{U}
pt=0/0
ability=Graft 3;\
        {1}: Target creature with a +1/+1 counter on it gains flying until end of turn.
timing=main
oracle=Graft 3\n{1}: Target creature with a +1/+1 counter on it gains flying until end of turn.
