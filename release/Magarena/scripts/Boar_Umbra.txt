name=Boar Umbra
image=https://cards.scryfall.io/border_crop/front/4/a/4a8ce72b-605c-4a05-93d2-64fa65308d96.jpg?1547517504
value=3.042
rarity=U
type=Enchantment
subtype=Aura
cost={2}{G}
ability=Enchant creature;\
        Enchanted creature gets +3/+3.;\
        Totem armor
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +3/+3.\nTotem armor
