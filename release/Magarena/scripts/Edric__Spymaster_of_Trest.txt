name=Edric, Spymaster of Trest
image=https://cards.scryfall.io/border_crop/front/2/1/2183cfb3-6bf5-4fbc-b535-a42ba954e8a3.jpg?1574767851
image_updated=2017-08-17
value=4.538
rarity=R
type=Legendary,Creature
subtype=Elf,Rogue
cost={1}{G}{U}
pt=2/2
timing=fmain
requires_groovy_code
oracle=Whenever a creature deals combat damage to one of your opponents, its controller may draw a card.
