name=Ishkanah, Grafwidow
image=https://cards.scryfall.io/border_crop/front/2/a/2a6c5a98-2fe5-4671-a6a8-98a47c09c5a7.jpg?1576384813
image_updated=2016-09-05
value=2.500
rarity=M
type=Legendary,Creature
subtype=Spider
cost={4}{G}
pt=3/5
ability=Reach;\
        When SN enters the battlefield, if there are four or more card types among cards in your graveyard, create three 1/2 green Spider creature tokens with reach.;\
        {6}{B}: Target opponent loses 1 life for each Spider you control.
timing=main
oracle=Reach\nDelirium — When Ishkanah, Grafwidow enters the battlefield, if there are four or more card types among cards in your graveyard, create three 1/2 green Spider creature tokens with reach.\n{6}{B}: Target opponent loses 1 life for each Spider you control.
