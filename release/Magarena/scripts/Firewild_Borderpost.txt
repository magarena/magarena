name=Firewild Borderpost
image=https://cards.scryfall.io/border_crop/front/e/b/eb37a8bd-6b6c-47f3-b7a0-5a20fa2d507c.jpg?1562644892
value=3.032
rarity=C
type=Artifact
cost={1}{R}{G}
ability=You may pay {1} and return a basic land you control to its owner's hand rather than pay this spell's mana cost.;\
        SN enters the battlefield tapped.;\
        {T}: Add {R} or {G}.
timing=artifact
oracle=You may pay {1} and return a basic land you control to its owner's hand rather than pay this spell's mana cost.\nFirewild Borderpost enters the battlefield tapped.\n{T}: Add {R} or {G}.
