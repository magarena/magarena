name=Stalking Assassin
image=https://cards.scryfall.io/border_crop/front/f/f/ff8cc71f-3070-497f-908f-35aa13a8a857.jpg?1562946631
value=1.873
rarity=R
type=Creature
subtype=Human,Assassin
cost={1}{U}{B}
pt=1/1
ability={3}{U}, {T}: Tap target creature.;\
        {3}{B}, {T}: Destroy target tapped creature.
timing=main
oracle={3}{U}, {T}: Tap target creature.\n{3}{B}, {T}: Destroy target tapped creature.
