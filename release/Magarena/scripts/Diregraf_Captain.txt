name=Diregraf Captain
image=https://cards.scryfall.io/border_crop/front/1/5/158407ad-35a9-40e1-9762-1c0572b56ecb.jpg?1637631384
value=3.861
rarity=U
type=Creature
subtype=Zombie,Soldier
cost={1}{U}{B}
pt=2/2
ability=Deathtouch;\
        Other Zombie creatures you control get +1/+1.;\
        Whenever another Zombie you control dies, target opponent loses 1 life.
static=player
timing=main
oracle=Deathtouch\nOther Zombie creatures you control get +1/+1.\nWhenever another Zombie you control dies, target opponent loses 1 life.
