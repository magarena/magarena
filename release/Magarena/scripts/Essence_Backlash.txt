name=Essence Backlash
image=https://cards.scryfall.io/border_crop/front/a/9/a98609dc-ea90-4c7e-a191-5e5d0ba16847.jpg?1562791298
value=3.000
rarity=C
type=Instant
cost={2}{U}{R}
timing=counter
requires_groovy_code
oracle=Counter target creature spell. Essence Backlash deals damage equal to that spell's power to its controller.
