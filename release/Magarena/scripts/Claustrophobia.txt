name=Claustrophobia
image=https://cards.scryfall.io/border_crop/front/0/5/05846389-4ee3-4e41-a6dd-0809bfc299f8.jpg?1611940472
image_updated=2017-04-12
value=2.500
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}{U}
ability=Enchant creature;\
        When SN enters the battlefield, tap enchanted creature.;\
        Enchanted creature doesn't untap during its controller's untap step.
timing=aura
enchant=can't attack or block,neg creature
oracle=Enchant creature\nWhen Claustrophobia enters the battlefield, tap enchanted creature.\nEnchanted creature doesn't untap during its controller's untap step.
