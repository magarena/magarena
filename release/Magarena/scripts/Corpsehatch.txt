name=Corpsehatch
image=https://cards.scryfall.io/border_crop/front/0/f/0f8f9dc0-67c3-4ded-bf95-02452d4ee635.jpg?1593095745
value=3.050
removal=3
rarity=U
type=Sorcery
cost={3}{B}{B}
effect=Destroy target nonblack creature.~Create two 0/1 colorless Eldrazi Spawn creature tokens.
timing=removal
oracle=Destroy target nonblack creature. Create two 0/1 colorless Eldrazi Spawn creature tokens. They have "Sacrifice this creature: Add {C}."
