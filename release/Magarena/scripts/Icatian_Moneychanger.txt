name=Icatian Moneychanger
image=https://cards.scryfall.io/border_crop/front/b/3/b3d502d4-4a96-47b3-ae26-8b2c9f36623d.jpg?1562928496
value=2.512
rarity=C
type=Creature
subtype=Human
cost={W}
pt=0/2
ability=SN enters the battlefield with three credit counters on it.;\
        When SN enters the battlefield, SN deals 3 damage to you.;\
        At the beginning of your upkeep, put a credit counter on SN.;\
        Sacrifice SN: You gain 1 life for each credit counter on SN. Activate this ability only during your upkeep.
timing=main
oracle=Icatian Moneychanger enters the battlefield with three credit counters on it.\nWhen Icatian Moneychanger enters the battlefield, it deals 3 damage to you.\nAt the beginning of your upkeep, put a credit counter on Icatian Moneychanger.\nSacrifice Icatian Moneychanger: You gain 1 life for each credit counter on Icatian Moneychanger. Activate this ability only during your upkeep.
