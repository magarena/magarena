name=Yuki-Onna
image=https://cards.scryfall.io/border_crop/front/6/3/63c6e1cd-e25d-41ff-a918-c495998376d8.jpg?1562494183
value=2.871
rarity=U
type=Creature
subtype=Spirit
cost={3}{R}
pt=3/1
ability=When SN enters the battlefield, destroy target artifact.;\
        Whenever you cast a Spirit or Arcane spell, you may return SN to its owner's hand.
timing=main
oracle=When Yuki-Onna enters the battlefield, destroy target artifact.\nWhenever you cast a Spirit or Arcane spell, you may return Yuki-Onna to its owner's hand.
