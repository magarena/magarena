name=Razorverge Thicket
image=https://cards.scryfall.io/border_crop/front/6/5/65b26f68-3a25-4c4e-bc76-a199ab479a50.jpg?1675957278
value=4.275
rarity=R
type=Land
mana=g3w3
ability=SN enters the battlefield tapped unless you control two or fewer other lands.;\
        {T}: Add {G} or {W}.
timing=land
oracle=Razorverge Thicket enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {G} or {W}.
