name=Goblin Taskmaster
image=https://cards.scryfall.io/border_crop/front/f/e/feff65ca-aedf-4434-b701-590d600d1a0b.jpg?1562955378
value=3.033
rarity=C
type=Creature
subtype=Goblin
cost={R}
pt=1/1
ability={1}{R}: Target Goblin creature gets +1/+0 until end of turn.;\
        Morph {R}
timing=main
oracle={1}{R}: Target Goblin creature gets +1/+0 until end of turn.\nMorph {R}
