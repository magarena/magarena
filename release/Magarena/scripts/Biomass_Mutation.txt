name=Biomass Mutation
image=https://cards.scryfall.io/border_crop/front/8/0/80fc2879-c73d-41c7-987c-cbeaca02cc45.jpg?1625977389
value=3.487
rarity=R
type=Instant
cost={X}{G/U}{G/U}
timing=pump
oracle=Creatures you control have base power and toughness X/X until end of turn.
requires_groovy_code
