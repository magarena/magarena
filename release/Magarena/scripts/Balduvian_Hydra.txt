name=Balduvian Hydra
image=https://cards.scryfall.io/border_crop/front/6/c/6c8128f0-7fbf-401e-b185-f85f2794f626.jpg?1562869049
value=2.519
rarity=R
type=Creature
subtype=Hydra
cost={X}{R}{R}
pt=0/1
ability=SN enters the battlefield with X +1/+0 counters on it.;\
        Remove a +1/+0 counter from SN: Prevent the next 1 damage that would be dealt to SN this turn.;\
        {R}{R}{R}: Put a +1/+0 counter on SN. Activate this ability only during your upkeep.
timing=main
oracle=Balduvian Hydra enters the battlefield with X +1/+0 counters on it.\nRemove a +1/+0 counter from Balduvian Hydra: Prevent the next 1 damage that would be dealt to Balduvian Hydra this turn.\n{R}{R}{R}: Put a +1/+0 counter on Balduvian Hydra. Activate this ability only during your upkeep.
