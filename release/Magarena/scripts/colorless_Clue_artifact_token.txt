name=colorless Clue artifact token
image=https://api.scryfall.com/cards/tc18/19/en?format=image&version=border_crop
token=Clue
type=Artifact
subtype=Clue
color=
value=1
ability={2}, Sacrifice SN: Draw a card.
oracle={2}, Sacrifice this artifact: Draw a card.
