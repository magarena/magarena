name=Rohgahh of Kher Keep
image=https://cards.scryfall.io/border_crop/front/1/4/1429cc3d-f6a2-4011-b716-5c3e0cf251d0.jpg?1562899257
value=3.090
rarity=R
type=Legendary,Creature
subtype=Kobold
cost={2}{B}{B}{R}{R}
pt=5/5
ability=Creatures you control named Kobolds of Kher Keep get +2/+2.
timing=main
requires_groovy_code
oracle=At the beginning of your upkeep, you may pay {R}{R}{R}. If you don't, tap Rohgahh of Kher Keep and all creatures named Kobolds of Kher Keep, then an opponent gains control of them.\nCreatures you control named Kobolds of Kher Keep get +2/+2.
