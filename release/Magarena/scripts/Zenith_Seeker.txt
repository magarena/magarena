name=Zenith Seeker
image=https://cards.scryfall.io/border_crop/front/4/7/47b711b5-aeec-47c4-a718-6bf43e561944.jpg?1543675171
value=2.500
rarity=U
type=Creature
subtype=Bird,Wizard
cost={3}{U}
pt=2/2
ability=Flying;\
        Whenever you cycle or discard a card, target creature gains flying until end of turn.
timing=main
oracle=Flying\nWhenever you cycle or discard a card, target creature gains flying until end of turn.
