name=Cantivore
image=https://cards.scryfall.io/border_crop/front/b/5/b5243fc3-176b-44a3-9f1a-ab069a08757a.jpg?1562928735
value=3.061
rarity=R
type=Creature
subtype=Lhurgoyf
cost={1}{W}{W}
pt=*/*
ability=Vigilance;\
        SN's power and toughness are each equal to the number of enchantment cards in all graveyards.
timing=main
oracle=Vigilance\nCantivore's power and toughness are each equal to the number of enchantment cards in all graveyards.
