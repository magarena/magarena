name=Quickling
image=https://cards.scryfall.io/border_crop/front/2/7/278fbdb9-51e3-4ed6-bcc5-c5b86f06c216.jpg?1562784199
value=3.625
rarity=U
type=Creature
subtype=Faerie,Rogue
cost={1}{U}
pt=2/2
ability=Flash;\
        Flying;\
        When SN enters the battlefield, sacrifice SN unless you return another creature you control to its owner's hand.
timing=flash
oracle=Flash\nFlying\nWhen Quickling enters the battlefield, sacrifice it unless you return another creature you control to its owner's hand.
