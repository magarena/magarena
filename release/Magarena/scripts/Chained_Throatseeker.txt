name=Chained Throatseeker
image=https://cards.scryfall.io/border_crop/front/3/a/3a7bb447-c2b0-429e-bf82-02d6a966fe73.jpg?1562876738
value=3.180
rarity=C
type=Creature
subtype=Horror
cost={5}{U}
pt=5/5
ability=Infect;\
        SN can't attack unless defending player is poisoned.
timing=main
oracle=Infect\nChained Throatseeker can't attack unless defending player is poisoned.
