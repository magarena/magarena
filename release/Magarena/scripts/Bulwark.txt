name=Bulwark
image=https://cards.scryfall.io/border_crop/front/a/2/a2041b95-ec5b-4512-b012-f875ca686669.jpg?1562929180
value=2.589
rarity=R
type=Enchantment
cost={3}{R}{R}
timing=enchantment
requires_groovy_code
oracle=At the beginning of your upkeep, Bulwark deals X damage to target opponent, where X is the number of cards in your hand minus the number of cards in that player's hand.
