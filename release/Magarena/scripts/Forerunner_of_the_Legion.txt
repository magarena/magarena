name=Forerunner of the Legion
image=https://cards.scryfall.io/border_crop/front/1/0/10718d37-63d4-44b7-9450-5d49cffce944.jpg?1555039699
value=2.500
rarity=U
type=Creature
subtype=Vampire,Knight
cost={2}{W}
pt=2/2
ability=When SN enters the battlefield, you may search your library for a Vampire card, reveal it, then shuffle your library and put that card on top of it.;\
        Whenever another Vampire enters the battlefield under your control, target creature gets +1/+1 until end of turn.
timing=main
oracle=When Forerunner of the Legion enters the battlefield, you may search your library for a Vampire card, reveal it, then shuffle your library and put that card on top of it.\nWhenever another Vampire enters the battlefield under your control, target creature gets +1/+1 until end of turn.
