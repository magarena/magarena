name=Hoofprints of the Stag
image=https://cards.scryfall.io/border_crop/front/4/7/47f70c41-25b2-468c-b679-8bbc7eaf913b.jpg?1673483714
image_updated=2017-08-17
value=4.088
rarity=R
type=Tribal,Enchantment
subtype=Elemental
cost={1}{W}
ability=Whenever you draw a card, you may put a hoofprint counter on SN.;\
        {2}{W}, Remove four hoofprint counters from SN: Create a 4/4 white Elemental creature token with flying. Activate this ability only during your turn.
timing=enchantment
oracle=Whenever you draw a card, you may put a hoofprint counter on Hoofprints of the Stag.\n{2}{W}, Remove four hoofprint counters from Hoofprints of the Stag: Create a 4/4 white Elemental creature token with flying. Activate this ability only during your turn.
