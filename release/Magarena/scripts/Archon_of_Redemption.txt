name=Archon of Redemption
image=https://cards.scryfall.io/border_crop/front/a/9/a9e8eb41-3800-4553-843b-ec27a44e8d55.jpg?1600716710
value=3.521
rarity=R
type=Creature
subtype=Archon
cost={3}{W}{W}
pt=3/4
ability=Flying
timing=main
oracle=Flying\nWhenever Archon of Redemption or another creature with flying enters the battlefield under your control, you may gain life equal to that creature's power.
requires_groovy_code
