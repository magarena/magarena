name=Fabled Hero
image=https://cards.scryfall.io/border_crop/front/6/4/642d9a5c-5697-4747-adce-5a71bcf1c086.jpg?1562819107
value=3.690
rarity=R
type=Creature
subtype=Human,Soldier
cost={1}{W}{W}
pt=2/2
ability=Double strike;\
        Whenever you cast a spell that targets SN, put a +1/+1 counter on SN.
timing=main
oracle=Double strike\nHeroic — Whenever you cast a spell that targets Fabled Hero, put a +1/+1 counter on Fabled Hero.
