name=Sandstone Warrior
image=https://cards.scryfall.io/border_crop/front/0/c/0c8a607b-b79a-4dc5-bbf5-f6de556f1c7d.jpg?1562428181
value=3.355
rarity=C
type=Creature
subtype=Human,Soldier,Warrior
cost={2}{R}{R}
pt=1/3
ability=First strike;\
        {R}: SN gets +1/+0 until end of turn.
timing=main
oracle=First strike\n{R}: Sandstone Warrior gets +1/+0 until end of turn.
