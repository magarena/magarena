name=That Which Was Taken
image=https://cards.scryfall.io/border_crop/front/d/8/d80c84ad-171f-42c3-94b4-a9fe25b877e7.jpg?1562880091
value=4.101
rarity=R
type=Legendary,Artifact
cost={5}
ability={4}, {T}: Put a divinity counter on another target permanent.
timing=artifact
requires_groovy_code
oracle={4}, {T}: Put a divinity counter on target permanent other than That Which Was Taken.\nEach permanent with a divinity counter on it has indestructible.
