name=Thundercloud Shaman
image=https://cards.scryfall.io/border_crop/front/1/7/17676da2-1d9a-4181-bc6c-1f75750f7578.jpg?1562272455
image_updated=2016-02-29
value=3.533
rarity=U
type=Creature
subtype=Giant,Shaman
cost={3}{R}{R}
pt=4/4
ability=When SN enters the battlefield, SN deals damage equal to the number of Giants you control to each non-Giant creature.
timing=main
oracle=When Thundercloud Shaman enters the battlefield, it deals damage equal to the number of Giants you control to each non-Giant creature.
