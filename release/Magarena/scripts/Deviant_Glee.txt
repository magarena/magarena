name=Deviant Glee
image=https://cards.scryfall.io/border_crop/front/e/1/e150896e-8745-42ac-894b-8f42a92bd7a7.jpg?1562794464
value=3.380
rarity=C
type=Enchantment
subtype=Aura
cost={B}
ability=Enchant creature;\
        Enchanted creature gets +2/+1 and has "{R}: This creature gains trample until end of turn."
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +2/+1 and has "{R}: This creature gains trample until end of turn."
