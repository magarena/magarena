name=Krosan Archer
image=https://cards.scryfall.io/border_crop/front/e/1/e188562f-8219-4fb0-ac2c-5618cfb00bca.jpg?1562937148
value=2.083
rarity=C
type=Creature
subtype=Centaur,Archer
cost={3}{G}
pt=2/3
ability=Reach;\
        {G}, Discard a card: SN gets +0/+2 until end of turn.
timing=main
oracle=Reach\n{G}, Discard a card: Krosan Archer gets +0/+2 until end of turn.
