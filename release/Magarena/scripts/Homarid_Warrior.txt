name=Homarid Warrior
image=https://cards.scryfall.io/border_crop/front/8/0/80f20d8e-bbdf-4af8-9505-d5604b04ad72.jpg?1562591855
value=1.864
rarity=C
type=Creature
subtype=Homarid,Warrior
cost={4}{U}
pt=3/3
ability={U}: SN gains shroud until end of turn.~SN doesn't untap during your next untap step.~Tap SN.
timing=main
oracle={U}: Homarid Warrior gains shroud until end of turn and doesn't untap during your next untap step. Tap Homarid Warrior.
