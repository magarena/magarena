name=Squall Drifter
image=https://cards.scryfall.io/border_crop/front/b/3/b3ecb728-4b54-47d0-bbef-d39e5f7d172f.jpg?1593274818
value=3.513
rarity=C
type=Snow,Creature
subtype=Elemental
cost={1}{W}
pt=1/1
ability=Flying;\
        {W}, {T}: Tap target creature.
timing=main
oracle=Flying\n{W}, {T}: Tap target creature.
