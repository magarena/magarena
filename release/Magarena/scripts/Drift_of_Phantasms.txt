name=Drift of Phantasms
image=https://cards.scryfall.io/border_crop/front/c/1/c1096ce5-f776-4028-b231-e6eaee35014b.jpg?1598914152
value=3.946
rarity=C
type=Creature
subtype=Spirit
cost={2}{U}
pt=0/5
ability=Defender;\
        Flying;\
        Transmute {1}{U}{U}
timing=smain
oracle=Defender\nFlying\nTransmute {1}{U}{U}
