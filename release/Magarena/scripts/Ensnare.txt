name=Ensnare
image=https://cards.scryfall.io/border_crop/front/0/5/055b344a-4eb1-4579-ac50-973b18e12fad.jpg?1562628470
value=4.000
rarity=U
type=Instant
cost={3}{U}
ability=Cast SN with AI only if an opponent controls an untapped creature.;\
        You may return two Islands you control to their owner's hand rather than pay this spell's mana cost.
effect=Tap all creatures.
timing=tapping
oracle=You may return two Islands you control to their owner's hand rather than pay this spell's mana cost.\nTap all creatures.
