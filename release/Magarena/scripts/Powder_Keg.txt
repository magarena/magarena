name=Powder Keg
image=https://cards.scryfall.io/border_crop/front/4/d/4d9715c2-9036-4ae2-a5b4-1b190d50c963.jpg?1562443764
value=4.411
rarity=R
type=Artifact
cost={2}
ability=At the beginning of your upkeep, you may put a fuse counter on SN.
timing=artifact
requires_groovy_code
oracle=At the beginning of your upkeep, you may put a fuse counter on Powder Keg.\n{T}, Sacrifice Powder Keg: Destroy each artifact and creature with converted mana cost equal to the number of fuse counters on Powder Keg.
