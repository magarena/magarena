name=Paragon of the Amesha
image=https://cards.scryfall.io/border_crop/front/d/2/d21c4772-fa03-4b71-a95e-1ad8466d25da.jpg?1562803948
value=2.973
rarity=U
type=Creature
subtype=Human,Knight
cost={2}{W}
pt=2/2
ability=First strike;\
        {W}{U}{B}{R}{G}: Until end of turn, SN becomes an Angel.~SN gets +3/+3 and gains flying and lifelink until end of turn.
timing=main
oracle=First strike\n{W}{U}{B}{R}{G}: Until end of turn, Paragon of the Amesha becomes an Angel, gets +3/+3, and gains flying and lifelink.
