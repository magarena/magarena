name=Pulse of the Grid
image=https://cards.scryfall.io/border_crop/front/b/6/b6805341-fa4f-4d89-8003-b27280fadfbd.jpg?1562639131
value=3.462
rarity=R
type=Instant
cost={1}{U}{U}
timing=draw
requires_groovy_code
oracle=Draw two cards, then discard a card. Then if an opponent has more cards in hand than you, return Pulse of the Grid to its owner's hand.
