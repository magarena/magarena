name=Boros Guildmage
image=https://cards.scryfall.io/border_crop/front/4/1/412cecdc-d6aa-4bda-947f-5c959cb76f41.jpg?1592673503
value=3.750
rarity=U
type=Creature
subtype=Human,Wizard
cost={R/W}{R/W}
pt=2/2
ability={1}{R}: Target creature gains haste until end of turn.;\
        {1}{W}: Target creature gains first strike until end of turn.
timing=main
oracle={1}{R}: Target creature gains haste until end of turn.\n{1}{W}: Target creature gains first strike until end of turn.
