name=Vital Splicer
image=https://cards.scryfall.io/border_crop/front/d/1/d13cf68c-b54c-46e1-a720-af57aac9e570.jpg?1593814023
image_updated=2017-08-08
value=3.218
rarity=U
type=Creature
subtype=Human,Artificer
cost={3}{G}
pt=1/1
ability=When SN enters the battlefield, create a 3/3 colorless Golem artifact creature token.;\
        {1}: Regenerate target Golem you control.
timing=main
oracle=When Vital Splicer enters the battlefield, create a 3/3 colorless Golem artifact creature token.\n{1}: Regenerate target Golem you control.
