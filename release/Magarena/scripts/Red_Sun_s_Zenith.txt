name=Red Sun's Zenith
image=https://cards.scryfall.io/border_crop/front/3/7/373eb109-0e30-41c1-b2df-6bc78d968890.jpg?1562610602
value=3.717
rarity=R
type=Sorcery
cost={X}{R}
effect=SN deals X damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.~Shuffle SN into its owner's library.
timing=main
oracle=Red Sun's Zenith deals X damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead. Shuffle Red Sun's Zenith into its owner's library.
