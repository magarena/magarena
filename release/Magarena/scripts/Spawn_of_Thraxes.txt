name=Spawn of Thraxes
image=https://cards.scryfall.io/border_crop/front/e/1/e13aa40e-c626-486d-8abc-10aa68542c5b.jpg?1593096108
value=3.364
rarity=R
type=Creature
subtype=Dragon
cost={5}{R}{R}
pt=5/5
ability=Flying;\
        When SN enters the battlefield, SN deals damage to target creature or player equal to the number of Mountains you control.
timing=main
oracle=Flying\nWhen Spawn of Thraxes enters the battlefield, it deals damage to target creature or player equal to the number of Mountains you control.
