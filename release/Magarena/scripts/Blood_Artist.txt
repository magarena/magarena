name=Blood Artist
image=https://cards.scryfall.io/border_crop/front/6/9/693dd112-d04a-4404-8fce-74f7e5497312.jpg?1673147465
value=3.955
rarity=U
type=Creature
subtype=Vampire
cost={1}{B}
pt=0/1
ability=Whenever SN or another creature dies, target player loses 1 life and you gain 1 life.
timing=main
oracle=Whenever Blood Artist or another creature dies, target player loses 1 life and you gain 1 life.
