name=Path to Exile
image=https://cards.scryfall.io/border_crop/front/5/c/5c3a6ce7-1ec7-414f-b6bb-cc604c6d529c.jpg?1682208534
image_updated=2017-08-08
value=4.300
removal=5
rarity=U
type=Instant
cost={W}
timing=removal
requires_groovy_code
oracle=Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.
