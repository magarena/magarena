name=Docent of Perfection
image=https://cards.scryfall.io/border_crop/front/3/0/30c3d4c1-dc3d-4529-9d6e-8c16149cf6da.jpg?1576384090
image_updated=2016-09-05
value=2.500
rarity=R
type=Creature
subtype=Insect,Horror
cost={3}{U}{U}
pt=5/4
ability=Flying;\
        Whenever you cast an instant or sorcery spell, create a 1/1 blue Human Wizard creature token.~Then if you control three or more Wizards, transform SN.
transform=Final Iteration
timing=main
oracle=Flying\nWhenever you cast an instant or sorcery spell, create a 1/1 blue Human Wizard creature token. Then if you control three or more Wizards, transform Docent of Perfection.
