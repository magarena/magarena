name=Forced Adaptation
image=https://cards.scryfall.io/border_crop/front/a/6/a6527d61-e9d3-44d0-833e-19c072309270.jpg?1561840465
value=3.170
rarity=C
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant creature;\
        At the beginning of your upkeep, put a +1/+1 counter on enchanted creature.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nAt the beginning of your upkeep, put a +1/+1 counter on enchanted creature.
