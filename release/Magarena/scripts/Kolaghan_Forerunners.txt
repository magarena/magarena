name=Kolaghan Forerunners
image=https://cards.scryfall.io/border_crop/front/2/c/2c56a8de-8ae8-4672-8119-9e6a1f4cf5cd.jpg?1562784173
value=2.500
rarity=U
type=Creature
subtype=Human,Berserker
cost={2}{R}
pt=*/3
ability=Trample;\
        SN's power is equal to the number of creatures you control.;\
        Dash {2}{R}
timing=main
oracle=Trample\nKolaghan Forerunners's power is equal to the number of creatures you control.\nDash {2}{R}
