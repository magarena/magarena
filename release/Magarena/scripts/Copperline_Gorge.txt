name=Copperline Gorge
image=https://cards.scryfall.io/border_crop/front/7/8/78b0f36b-7d8c-4e77-adc2-a4dad93a81d5.jpg?1675957264
value=4.270
rarity=R
type=Land
mana=r3g3
ability=SN enters the battlefield tapped unless you control two or fewer other lands.;\
        {T}: Add {R} or {G}.
timing=land
oracle=Copperline Gorge enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {R} or {G}.
