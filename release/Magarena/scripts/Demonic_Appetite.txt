name=Demonic Appetite
image=https://cards.scryfall.io/border_crop/front/c/a/ca51786a-d58c-455f-910d-01efa5ef8470.jpg?1562708607
value=2.688
rarity=C
type=Enchantment
subtype=Aura
cost={B}
ability=Enchant creature you control;\
        Enchanted creature gets +3/+3.;\
        At the beginning of your upkeep, sacrifice a creature.
timing=aura
enchant=pump,creature you control
oracle=Enchant creature you control\nEnchanted creature gets +3/+3.\nAt the beginning of your upkeep, sacrifice a creature.
