name=Pincer Spider
image=https://cards.scryfall.io/border_crop/front/2/3/23271658-19ae-420d-beeb-4bed4fdbb891.jpg?1562902046
value=2.631
rarity=C
type=Creature
subtype=Spider
cost={2}{G}
pt=2/3
ability=Kicker {3};\
        Reach
timing=main
requires_groovy_code=Ardent Soldier
oracle=Kicker {3}\nReach\nIf Pincer Spider was kicked, it enters the battlefield with a +1/+1 counter on it.
