name=Cartouche of Strength
image=https://cards.scryfall.io/border_crop/front/d/6/d6bb237c-4e39-4879-90b4-2f507a90d3d7.jpg?1543675766
value=2.500
rarity=C
type=Enchantment
subtype=Aura,Cartouche
cost={2}{G}
ability=Enchant creature you control;\
        Enchanted creature gets +1/+1 and has trample.
timing=aura
enchant=trample,creature you control
requires_groovy_code
oracle=Enchant creature you control\nWhen Cartouche of Strength enters the battlefield, you may have enchanted creature fight target creature an opponent controls.\nEnchanted creature gets +1/+1 and has trample.
