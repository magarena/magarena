name=Deathgrip
image=https://cards.scryfall.io/border_crop/front/f/c/fc86164d-24f1-4fad-a358-f61dbfd86bd9.jpg?1562954841
value=3.534
rarity=U
type=Enchantment
cost={B}{B}
ability={B}{B}: Counter target green spell.
timing=enchantment
oracle={B}{B}: Counter target green spell.
