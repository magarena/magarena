name=Bonds of Quicksilver
image=https://cards.scryfall.io/border_crop/front/f/0/f01c9fe7-35a4-4f64-b527-5311872b13fd.jpg?1576382359
image_updated=2017-08-29
value=2.240
removal=3
rarity=C
type=Enchantment
subtype=Aura
cost={3}{U}
ability=Flash;\
        Enchant creature;\
        Enchanted creature doesn't untap during its controller's untap step.
timing=tapping
enchant=can't attack or block,neg creature
oracle=Flash\nEnchant creature\nEnchanted creature doesn't untap during its controller's untap step.
