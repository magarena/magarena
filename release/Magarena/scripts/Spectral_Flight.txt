name=Spectral Flight
image=https://cards.scryfall.io/border_crop/front/f/7/f7149f2a-6917-4ad7-8035-c7a1babd4d4b.jpg?1562839858
value=3.700
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}
ability=Enchant creature;\
        Enchanted creature gets +2/+2 and has flying.
timing=aura
enchant=flying,pos creature
oracle=Enchant creature\nEnchanted creature gets +2/+2 and has flying.
