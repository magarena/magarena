name=Chromium
image=https://cards.scryfall.io/border_crop/front/1/5/15ec5a20-4e8f-40b2-9abf-c0bf1cf816c3.jpg?1562899599
value=3.188
rarity=R
type=Legendary,Creature
subtype=Elder,Dragon
cost={2}{W}{W}{U}{U}{B}{B}
pt=7/7
ability=Flying;\
        Rampage 2;\
        At the beginning of your upkeep, sacrifice SN unless you pay {W}{U}{B}.
timing=main
oracle=Flying\nRampage 2\nAt the beginning of your upkeep, sacrifice Chromium unless you pay {W}{U}{B}.
