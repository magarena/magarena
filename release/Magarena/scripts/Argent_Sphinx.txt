name=Argent Sphinx
image=https://cards.scryfall.io/border_crop/front/9/9/99d03f11-7448-47de-9fb4-3ceaf08dc1d4.jpg?1562265270
value=3.786
rarity=R
type=Creature
subtype=Sphinx
cost={2}{U}{U}
pt=4/3
ability=Flying
timing=main
requires_groovy_code
oracle=Flying\nMetalcraft — {U}: Exile Argent Sphinx. Return it to the battlefield under your control at the beginning of the next end step. Activate this ability only if you control three or more artifacts.
