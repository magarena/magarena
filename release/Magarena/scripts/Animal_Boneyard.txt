name=Animal Boneyard
image=https://cards.scryfall.io/border_crop/front/e/3/e3379317-be18-4252-84ad-b7ebb6e557ff.jpg?1562937487
value=2.732
rarity=U
type=Enchantment
subtype=Aura
cost={2}{W}
timing=aura
enchant=pump,pos land
requires_groovy_code
oracle=Enchant land\nEnchanted land has "{T}, Sacrifice a creature: You gain life equal to the sacrificed creature's toughness."
