name=Magma Vein
image=https://cards.scryfall.io/border_crop/front/6/9/69ed2b52-2862-423c-8ce3-6c8232d9d92c.jpg?1562914619
value=1.423
rarity=U
type=Enchantment
cost={2}{R}
ability={R}, Sacrifice a land: SN deals 1 damage to each creature without flying.
timing=enchantment
oracle={R}, Sacrifice a land: Magma Vein deals 1 damage to each creature without flying.
