name=Legacy Weapon
image=https://cards.scryfall.io/border_crop/front/a/e/ae2ef7bf-6e5b-40a0-8b25-478323b64bdc.jpg?1562553513
value=4.153
rarity=R
type=Legendary,Artifact
cost={7}
ability={W}{U}{B}{R}{G}: Exile target permanent.;\
        If SN would be put into a graveyard from anywhere, reveal SN and shuffle it into its owner's library instead.
timing=artifact
oracle={W}{U}{B}{R}{G}: Exile target permanent.\nIf Legacy Weapon would be put into a graveyard from anywhere, reveal Legacy Weapon and shuffle it into its owner's library instead.
