name=Brainstorm
image=https://cards.scryfall.io/border_crop/front/4/8/48070245-1370-4cf1-be15-d4e8a8b92ba8.jpg?1631586166
image_updated=2018-04-27
value=4.750
rarity=C
type=Instant
cost={U}
timing=draw
oracle=Draw three cards, then put two cards from your hand on top of your library in any order.
requires_groovy_code
