name=Encrust
image=https://cards.scryfall.io/border_crop/front/f/5/f531a657-9e60-453a-b72c-2ba04913698f.jpg?1562797054
value=2.700
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}{U}
ability=Enchant artifact or creature;\
        Enchanted permanent doesn't untap during its controller's untap step and its activated abilities can't be activated.
timing=aura
enchant=can't attack or block,neg artifact or creature
oracle=Enchant artifact or creature\nEnchanted permanent doesn't untap during its controller's untap step and its activated abilities can't be activated.
