name=Cloudchaser Eagle
image=https://cards.scryfall.io/border_crop/front/8/1/81cd5854-56ef-48ec-ad12-1690fa45b4a5.jpg?1562241673
value=3.078
rarity=C
type=Creature
subtype=Bird
cost={3}{W}
pt=2/2
ability=Flying;\
        When SN enters the battlefield, destroy target enchantment.
timing=main
oracle=Flying\nWhen Cloudchaser Eagle enters the battlefield, destroy target enchantment.
