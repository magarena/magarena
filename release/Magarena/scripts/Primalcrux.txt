name=Primalcrux
image=https://cards.scryfall.io/border_crop/front/1/f/1f805f7b-bb7e-436b-abd3-20a35e7ef71e.jpg?1562901612
value=4.346
rarity=R
type=Creature
subtype=Elemental
cost={G}{G}{G}{G}{G}{G}
pt=*/*
ability=Trample
timing=main
requires_groovy_code
oracle=Trample\nChroma — Primalcrux's power and toughness are each equal to the number of green mana symbols in the mana costs of permanents you control.
