name=Sky Scourer
image=https://cards.scryfall.io/border_crop/front/e/b/eb00fa72-bcda-4081-9048-e13752da8d46.jpg?1562942219
image_updated=2016-02-29
value=2.500
rarity=C
type=Creature
subtype=Eldrazi,Drone
cost={1}{B}
pt=1/2
ability=Devoid;\
        Flying;\
        Whenever you cast a colorless spell, SN gets +1/+0 until end of turn.
timing=main
oracle=Devoid\nFlying\nWhenever you cast a colorless spell, Sky Scourer gets +1/+0 until end of turn.
