name=Predator, Flagship
image=https://cards.scryfall.io/border_crop/front/4/9/4910ee8b-ec00-4a5e-859e-b865b23117da.jpg?1592673658
value=4.000
rarity=R
type=Legendary,Artifact
cost={5}
ability={2}: Target creature gains flying until end of turn.;\
        {5}, {T}: Destroy target creature with flying.
timing=artifact
oracle={2}: Target creature gains flying until end of turn.\n{5}, {T}: Destroy target creature with flying.
