name=Atzal, Cave of Eternity
image=https://cards.scryfall.io/border_crop/back/d/8/d81c4b3f-81c2-403b-8a5d-c9415f73a1f9.jpg?1572373636
value=2.500
rarity=R
type=Legendary,Land
ability={T}: Add one mana of any color.;\
        {3}{B}{G}, {T}: Return target creature card from your graveyard to the battlefield.
transform=Journey to Eternity
hidden
timing=land
oracle={T}: Add one mana of any color.\n{3}{B}{G}, {T}: Return target creature card from your graveyard to the battlefield.
