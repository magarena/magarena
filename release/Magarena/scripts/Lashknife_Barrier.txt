name=Lashknife Barrier
image=https://cards.scryfall.io/border_crop/front/2/4/2485c10d-de02-4be9-8119-afb2296e3317.jpg?1562902725
value=3.708
rarity=U
type=Enchantment
cost={2}{W}
ability=When SN enters the battlefield, draw a card.
timing=enchantment
requires_groovy_code
oracle=When Lashknife Barrier enters the battlefield, draw a card.\nIf a source would deal damage to a creature you control, it deals that much damage minus 1 to that creature instead.
