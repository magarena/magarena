name=Karrthus, Tyrant of Jund
image=https://cards.scryfall.io/border_crop/front/2/b/2bfb85e8-278b-48a4-970e-e65bad1c4c47.jpg?1599708104
value=4.101
rarity=M
type=Legendary,Creature
subtype=Dragon
cost={4}{B}{R}{G}
pt=7/7
ability=Flying, haste;\
        When SN enters the battlefield, gain control of all Dragons,~then untap all Dragons.;\
        Other Dragon creatures you control have haste.
timing=main
oracle=Flying, haste\nWhen Karrthus, Tyrant of Jund enters the battlefield, gain control of all Dragons, then untap all Dragons.\nOther Dragon creatures you control have haste.
