name=Vigilant Drake
image=https://cards.scryfall.io/border_crop/front/5/b/5bb4be1f-3e5b-4881-9fae-9ed022f8eeac.jpg?1562238541
value=2.589
rarity=C
type=Creature
subtype=Drake
cost={4}{U}
pt=3/3
ability=Flying;\
        {2}{U}: Untap SN.
timing=main
oracle=Flying\n{2}{U}: Untap Vigilant Drake.
