name=Backfire
image=https://cards.scryfall.io/border_crop/front/a/d/ad34094d-a7ec-4b04-a288-4d4f1a07fc6b.jpg?1559603813
value=3.558
rarity=U
type=Enchantment
subtype=Aura
cost={U}
timing=aura
enchant=can't attack,neg creature
requires_groovy_code
oracle=Enchant creature\nWhenever enchanted creature deals damage to you, Backfire deals that much damage to that creature's controller.
