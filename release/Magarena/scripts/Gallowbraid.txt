name=Gallowbraid
image=https://cards.scryfall.io/border_crop/front/8/d/8df86192-6374-42ac-94bc-95e2e8284bd6.jpg?1562802094
value=2.337
rarity=R
type=Legendary,Creature
subtype=Horror
cost={3}{B}{B}
pt=5/5
ability=Trample
timing=main
requires_groovy_code
oracle=Trample\nCumulative upkeep—Pay 1 life.
