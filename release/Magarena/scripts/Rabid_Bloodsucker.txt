name=Rabid Bloodsucker
image=https://cards.scryfall.io/border_crop/front/7/3/7335da62-3b59-4c73-a19e-b1733393ed88.jpg?1562025720
image_updated=2015-11-05
value=2.500
rarity=C
type=Creature
subtype=Vampire
cost={4}{B}
pt=3/2
ability=Flying;\
        When SN enters the battlefield, each player loses 2 life.
timing=main
oracle=Flying\nWhen Rabid Bloodsucker enters the battlefield, each player loses 2 life.
