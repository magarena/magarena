name=Meltdown
image=https://cards.scryfall.io/border_crop/front/9/e/9e7a967a-35a0-4e5c-a32b-123a9cfdb79e.jpg?1562928443
value=3.771
rarity=U
type=Sorcery
cost={X}{R}
timing=main
requires_groovy_code
oracle=Destroy each artifact with converted mana cost X or less.
