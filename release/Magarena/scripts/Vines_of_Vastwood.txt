name=Vines of Vastwood
image=https://cards.scryfall.io/border_crop/front/6/2/6203e3d4-8998-41d6-9f7e-b68af0f1f8b5.jpg?1562263070
value=4.176
rarity=C
type=Instant
cost={G}
timing=pump
ability=Kicker {G}
requires_groovy_code
oracle=Kicker {G}\nTarget creature can't be the target of spells or abilities your opponents control this turn. If Vines of Vastwood was kicked, that creature gets +4/+4 until end of turn.
