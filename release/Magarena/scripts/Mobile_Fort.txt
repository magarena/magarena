name=Mobile Fort
image=https://cards.scryfall.io/border_crop/front/5/f/5f642246-5b2a-46a6-9236-524a297d7608.jpg?1562914933
value=3.422
rarity=U
type=Artifact,Creature
subtype=Wall
cost={4}
pt=0/6
ability=Defender;\
        {3}: SN gets +3/-1 until end of turn.~SN can attack as though it didn't have defender this turn. Activate this ability only once each turn.
timing=smain
oracle=Defender\n{3}: Mobile Fort gets +3/-1 until end of turn and can attack this turn as though it didn't have defender. Activate this ability only once each turn.
