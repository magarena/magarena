name=Sea's Claim
image=https://cards.scryfall.io/border_crop/front/8/2/82e836c8-4371-402f-8f90-81325a9879b8.jpg?1562737796
value=3.162
rarity=C
type=Enchantment
subtype=Aura
cost={U}
ability=Enchant land
timing=aura
enchant=default,land
oracle=Enchant land\nEnchanted land is an Island.
requires_groovy_code
