name=Infected Vermin
image=https://cards.scryfall.io/border_crop/front/0/0/00615487-3526-4b4c-bb06-8f2af1f101d0.jpg?1562895063
value=2.609
rarity=U
type=Creature
subtype=Rat
cost={2}{B}
pt=1/1
ability={2}{B}: SN deals 1 damage to each creature and each player.;\
        {3}{B}: SN deals 3 damage to each creature and each player. Activate this ability only if seven or more cards are in your graveyard.
timing=main
oracle={2}{B}: Infected Vermin deals 1 damage to each creature and each player.\nThreshold — {3}{B}: Infected Vermin deals 3 damage to each creature and each player. Activate this ability only if seven or more cards are in your graveyard.
