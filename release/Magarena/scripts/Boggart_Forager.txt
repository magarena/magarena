name=Boggart Forager
image=https://cards.scryfall.io/border_crop/front/f/b/fbc26374-d8de-4740-b1ec-ac92cfedbebc.jpg?1562375843
value=2.054
rarity=C
type=Creature
subtype=Goblin,Rogue
cost={R}
pt=1/1
timing=main
oracle={R}, Sacrifice Boggart Forager: Target player shuffles his or her library.
requires_groovy_code
