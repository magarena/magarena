name=Grove of the Guardian
image=https://cards.scryfall.io/border_crop/front/3/c/3cf60ca0-e01f-499c-8d04-d59050f38c33.jpg?1562785187
value=3.909
rarity=R
type=Land
ability={T}: Add {C}.;\
        {3}{G}{W}, {T}, Tap another two untapped creatures you control, Sacrifice SN: Create an 8/8 green and white Elemental creature token with vigilance.
timing=main
oracle={T}: Add {C}.\n{3}{G}{W}, {T}, Tap two untapped creatures you control, Sacrifice Grove of the Guardian: Create an 8/8 green and white Elemental creature token with vigilance.
