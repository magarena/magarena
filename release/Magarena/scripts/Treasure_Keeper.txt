name=Treasure Keeper
image=https://cards.scryfall.io/border_crop/front/b/6/b68ab416-3394-4cb6-bfc6-33e62964dd1c.jpg?1674138077
image_updated=2018-04-27
value=2.500
rarity=U
type=Artifact,Creature
subtype=Construct
cost={4}
pt=3/3
timing=main
requires_groovy_code
oracle=When Treasure Keeper dies, reveal cards from the top of your library until you reveal a nonland card with converted mana cost 3 or less. You may cast that card without paying its mana cost. Put all revealed cards not cast this way on the bottom of your library in a random order.
