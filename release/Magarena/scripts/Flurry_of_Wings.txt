name=Flurry of Wings
image=https://cards.scryfall.io/border_crop/front/d/b/dbabaf1d-0220-438d-8263-e23d8010fe24.jpg?1562644569
value=3.259
rarity=U
type=Instant
cost={G}{W}{U}
ability=Cast SN with AI only if there is an attacking creature on the battlefield.
effect=Create X 1/1 white Bird Soldier creature tokens with flying, where X is the number of attacking creatures.
timing=token
oracle=Create X 1/1 white Bird Soldier creature tokens with flying, where X is the number of attacking creatures.
