name=Tezzeret, Master of Metal
image=https://cards.scryfall.io/border_crop/front/d/7/d7c6614c-cc2b-4e5b-9c0d-ce8e4b2d8ea7.jpg?1562946467
value=2.500
rarity=M
type=Legendary,Planeswalker
subtype=Tezzeret
cost={4}{U}{B}
loyalty=5
ability=−3: Target opponent loses life equal to the number of artifacts you control.;\
        −8: Gain control of all artifacts and creatures target opponent controls.
timing=main
requires_groovy_code
oracle=+1: Reveal cards from the top of your library until you reveal an artifact card. Put that card into your hand and the rest on the bottom of your library in a random order.\n−3: Target opponent loses life equal to the number of artifacts you control.\n−8: Gain control of all artifacts and creatures target opponent controls.
