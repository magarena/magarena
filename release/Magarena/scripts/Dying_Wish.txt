name=Dying Wish
image=https://cards.scryfall.io/border_crop/front/b/4/b46e83d3-c66d-42fb-8435-b6c448db01ae.jpg?1561842566
value=2.815
rarity=U
type=Enchantment
subtype=Aura
cost={1}{B}
ability=Enchant creature you control
timing=aura
enchant=power,creature you control
oracle=Enchant creature you control\nWhen enchanted creature dies, target player loses X life and you gain X life, where X is its power.
requires_groovy_code
