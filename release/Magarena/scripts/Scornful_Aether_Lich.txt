name=Scornful Aether-Lich
image=https://cards.scryfall.io/border_crop/front/7/5/75cdb2ff-6e30-4766-9166-9036a0bdb809.jpg?1562801790
image_updated=2016-09-27
value=2.921
rarity=U
type=Artifact,Creature
subtype=Zombie,Wizard
cost={3}{U}
pt=2/4
ability={W}{B}: SN gains fear and vigilance until end of turn.
timing=main
oracle={W}{B}: Scornful Aether-Lich gains fear and vigilance until end of turn.
