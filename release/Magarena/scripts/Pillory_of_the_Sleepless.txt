name=Pillory of the Sleepless
image=https://cards.scryfall.io/border_crop/front/1/b/1bc97379-91aa-4bba-80af-8980eeb57630.jpg?1562433445
image_updated=2018-04-27
value=4.091
removal=4
rarity=U
type=Enchantment
subtype=Aura
cost={1}{W}{B}
ability=Enchant creature;\
        Enchanted creature can't attack or block.;\
        Enchanted creature has "At the beginning of your upkeep, you lose 1 life."
timing=aura
enchant=can't attack or block,neg creature
oracle=Enchant creature\nEnchanted creature can't attack or block.\nEnchanted creature has "At the beginning of your upkeep, you lose 1 life."
