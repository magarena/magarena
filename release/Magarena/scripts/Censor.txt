name=Censor
image=https://cards.scryfall.io/border_crop/front/4/c/4cb4e315-1a77-479a-9f15-fb23575de805.jpg?1543674908
value=2.500
rarity=U
type=Instant
cost={1}{U}
ability=Cycling {U}
effect=Counter target spell unless its controller pays {1}.
timing=counter
oracle=Counter target spell unless its controller pays {1}.\nCycling {U}
