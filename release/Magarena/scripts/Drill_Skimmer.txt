name=Drill-Skimmer
image=https://cards.scryfall.io/border_crop/front/3/b/3b60dc6a-3b38-4dde-9dd4-aa49a53f29ae.jpg?1562636354
value=2.038
rarity=C
type=Artifact,Creature
subtype=Thopter
cost={4}
pt=2/1
ability=Flying;\
        SN has shroud as long as you control another artifact creature.
timing=main
oracle=Flying\nDrill-Skimmer has shroud as long as you control another artifact creature.
