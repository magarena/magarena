name=Tresserhorn Sinks
image=https://cards.scryfall.io/border_crop/front/4/b/4bd8fe2c-2297-4e19-8a87-01eb99b8c6dc.jpg?1593860376
value=3.476
rarity=U
type=Snow,Land
mana=b3r3
ability=SN enters the battlefield tapped.;\
        {T}: Add {B} or {R}.
timing=tapland
oracle=Tresserhorn Sinks enters the battlefield tapped.\n{T}: Add {B} or {R}.
