name=Skittish Valesk
image=https://cards.scryfall.io/border_crop/front/4/c/4cc8a6e6-ed62-4784-ba9a-b1f703fc6119.jpg?1562912967
value=1.079
rarity=U
type=Creature
subtype=Beast
cost={6}{R}
pt=5/5
ability=At the beginning of your upkeep, flip a coin. If you lose the flip, turn SN face down.;\
        Morph {5}{R}
timing=main
oracle=At the beginning of your upkeep, flip a coin. If you lose the flip, turn Skittish Valesk face down.\nMorph {5}{R}
