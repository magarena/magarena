name=Vodalian Mage
image=https://cards.scryfall.io/border_crop/front/c/1/c107e82b-134a-4f2b-98c2-6537fae6a50d.jpg?1587910890
value=3.547
rarity=C
type=Creature
subtype=Merfolk,Wizard
cost={2}{U}
pt=1/1
ability={U}, {T}: Counter target spell unless its controller pays {1}.
timing=main
oracle={U}, {T}: Counter target spell unless its controller pays {1}.
