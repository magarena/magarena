name=Inkmoth Nexus
image=https://cards.scryfall.io/border_crop/front/e/c/ec50c1c3-885e-47d3-ada7-cc0edbf09df1.jpg?1623098818
value=4.348
rarity=R
type=Land
ability={T}: Add {C}.;\
        {1}: SN becomes a 1/1 Blinkmoth artifact creature with flying and infect until end of turn. It's still a land.
timing=land
mana_or_combat
oracle={T}: Add {C}.\n{1}: Inkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying and infect until end of turn. It's still a land.
