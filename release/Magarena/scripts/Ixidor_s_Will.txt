name=Ixidor's Will
image=https://cards.scryfall.io/border_crop/front/1/b/1b713448-853a-41ee-a302-963e9c1c1c65.jpg?1562901464
value=2.022
rarity=C
type=Instant
cost={2}{U}
ability=Cast SN with AI only if there is a Wizard on the battlefield.
timing=counter
requires_groovy_code
oracle=Counter target spell unless its controller pays {2} for each Wizard on the battlefield.
