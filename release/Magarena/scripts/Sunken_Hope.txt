name=Sunken Hope
image=https://cards.scryfall.io/border_crop/front/7/3/73fe736d-9ffa-4e8b-a353-d591da749ec3.jpg?1562916522
value=4.235
rarity=R
type=Enchantment
cost={3}{U}{U}
timing=enchantment
requires_groovy_code
oracle=At the beginning of each player's upkeep, that player returns a creature he or she controls to its owner's hand.
