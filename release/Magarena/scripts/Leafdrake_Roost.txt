name=Leafdrake Roost
image=https://cards.scryfall.io/border_crop/front/5/f/5f74cf11-8deb-4623-9c1e-1e86e98bd831.jpg?1592673400
value=3.625
rarity=U
type=Enchantment
subtype=Aura
cost={3}{G}{U}
ability=Enchant land;\
        Enchanted land has "{G}{U}, {T}: Create a 2/2 green and blue Drake creature token with flying."
enchant=pump,pos land
timing=aura
oracle=Enchant land\nEnchanted land has "{G}{U}, {T}: Create a 2/2 green and blue Drake creature token with flying."
