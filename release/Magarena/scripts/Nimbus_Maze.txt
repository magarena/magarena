name=Nimbus Maze
image=https://cards.scryfall.io/border_crop/front/7/1/711dbdb1-7ec3-4fb4-8364-d34f5e143fd1.jpg?1631589989
image_updated=2017-04-12
value=4.216
rarity=R
type=Land
ability={T}: Add {C}.;\
        {T}: Add {W}. Activate this ability only if you control an Island.;\
        {T}: Add {U}. Activate this ability only if you control a Plains.
timing=land
oracle={T}: Add {C}.\n{T}: Add {W}. Activate this ability only if you control an Island.\n{T}: Add {U}. Activate this ability only if you control a Plains.
