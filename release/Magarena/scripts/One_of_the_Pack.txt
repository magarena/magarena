name=One of the Pack
image=https://cards.scryfall.io/border_crop/back/6/c/6c200ef1-ea08-48d4-ab50-77940a0e08c3.jpg?1576385243
value=2.500
rarity=C
type=Creature
subtype=Werewolf
color=g
pt=5/6
ability=At the beginning of each upkeep, if a player cast two or more spells last turn, transform SN.
transform=Solitary Hunter
timing=main
hidden
oracle=At the beginning of each upkeep, if a player cast two or more spells last turn, transform One of the Pack.
