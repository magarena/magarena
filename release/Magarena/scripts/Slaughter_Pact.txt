name=Slaughter Pact
image=https://cards.scryfall.io/border_crop/front/8/1/81e00341-030f-433e-b22b-aca42e0a88d2.jpg?1619396090
value=4.312
rarity=R
type=Instant
cost={0}
color=b
effect=Destroy target nonblack creature.~At the beginning of your next upkeep, pay {2}{B}. If you don't, you lose the game.
timing=removal
oracle=Destroy target nonblack creature.\nAt the beginning of your next upkeep, pay {2}{B}. If you don't, you lose the game.
