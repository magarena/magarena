name=Cloudhoof Kirin
image=https://cards.scryfall.io/border_crop/front/4/2/42aa0d70-8d66-47aa-9228-25dcdb4e7dad.jpg?1562493486
value=3.807
rarity=R
type=Legendary,Creature
subtype=Kirin,Spirit
cost={3}{U}{U}
pt=4/4
ability=Flying
timing=main
requires_groovy_code
oracle=Flying\nWhenever you cast a Spirit or Arcane spell, you may have target player put the top X cards of his or her library into his or her graveyard, where X is that spell's converted mana cost.
