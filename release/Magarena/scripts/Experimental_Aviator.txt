name=Experimental Aviator
image=https://cards.scryfall.io/border_crop/front/9/6/962dc573-612d-434b-82fb-af9c3e3c9aca.jpg?1576381190
image_updated=2016-10-05
value=2.500
rarity=U
type=Creature
subtype=Human,Artificer
cost={3}{U}{U}
pt=0/3
ability=Flying;\
        When SN enters the battlefield, create two 1/1 colorless Thopter artifact creature tokens with flying.
timing=main
oracle=Flying\nWhen Experimental Aviator enters the battlefield, create two 1/1 colorless Thopter artifact creature tokens with flying.
