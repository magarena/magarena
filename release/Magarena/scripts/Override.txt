name=Override
image=https://cards.scryfall.io/border_crop/front/3/5/35964fa6-800d-41d6-9f82-fb9c87deee56.jpg?1562140248
value=2.938
rarity=C
type=Instant
cost={2}{U}
timing=counter
requires_groovy_code
oracle=Counter target spell unless its controller pays {1} for each artifact you control.
