name=Zulaport Enforcer
image=https://cards.scryfall.io/border_crop/front/1/9/1975d18a-df3f-4c1f-8fde-d11dabbc4adc.jpg?1562701591
value=2.173
rarity=C
type=Creature
subtype=Human,Warrior
cost={B}
pt=1/1
ability=Level up {4} 3
timing=main
requires_groovy_code
oracle=Level up {4}\nLEVEL 1-2\n3/3\nLEVEL 3+\n5/5\nZulaport Enforcer can't be blocked except by black creatures.
