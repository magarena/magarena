name=Presence of the Master
image=https://cards.scryfall.io/border_crop/front/8/4/849adb29-61ad-4307-98b9-61e33aec6500.jpg?1587857394
value=3.696
rarity=U
type=Enchantment
cost={3}{W}
timing=enchantment
requires_groovy_code
oracle=Whenever a player casts an enchantment spell, counter it.
