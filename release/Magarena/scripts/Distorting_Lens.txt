name=Distorting Lens
image=https://cards.scryfall.io/border_crop/front/6/d/6d82d9b7-4b1c-447c-8044-9e114604017e.jpg?1562916925
value=3.852
rarity=R
type=Artifact
cost={2}
ability={T}: Target permanent becomes the color of your choice until end of turn.
timing=artifact
oracle={T}: Target permanent becomes the color of your choice until end of turn.
