name=Rishadan Footpad
image=https://cards.scryfall.io/border_crop/front/4/9/493ee964-1a44-46a1-8606-90e215805483.jpg?1562380435
value=3.033
rarity=U
type=Creature
subtype=Human,Pirate
cost={3}{U}
pt=2/2
timing=main
requires_groovy_code
oracle=When Rishadan Footpad enters the battlefield, each opponent sacrifices a permanent unless he or she pays {2}.
