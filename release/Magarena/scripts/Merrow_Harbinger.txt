name=Merrow Harbinger
image=https://cards.scryfall.io/border_crop/front/b/4/b47af11c-1090-4f0a-8eea-64a3b639e535.jpg?1562364093
value=3.194
rarity=U
type=Creature
subtype=Merfolk,Wizard
cost={3}{U}
pt=2/3
ability=Islandwalk;\
        When SN enters the battlefield, you may search your library for a Merfolk card, reveal it, then shuffle your library and put that card on top of it.
timing=main
oracle=Islandwalk\nWhen Merrow Harbinger enters the battlefield, you may search your library for a Merfolk card, reveal it, then shuffle your library and put that card on top of it.
