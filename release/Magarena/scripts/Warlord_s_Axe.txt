name=Warlord's Axe
image=https://cards.scryfall.io/border_crop/front/4/1/41ab6393-df29-475b-b56d-c56eb95de05d.jpg?1562457489
value=1.291
rarity=U
type=Artifact
subtype=Equipment
cost={3}
ability=Equipped creature gets +3/+1.;\
        Equip {4}
timing=equipment
oracle=Equipped creature gets +3/+1.\nEquip {4}
