name=Niblis of the Breath
image=https://cards.scryfall.io/border_crop/front/0/6/0686843d-6d1e-4488-8c17-7c986a154195.jpg?1562896555
value=3.233
rarity=U
type=Creature
subtype=Spirit
cost={2}{U}
pt=2/1
ability=Flying;\
        {U}, {T}: You may tap or untap target creature.
timing=main
oracle=Flying\n{U}, {T}: You may tap or untap target creature.
