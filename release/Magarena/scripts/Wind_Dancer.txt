name=Wind Dancer
image=https://cards.scryfall.io/border_crop/front/4/6/469d217a-0f8c-48f8-bfc0-cf5e202c0ddc.jpg?1562429347
value=3.750
rarity=U
type=Creature
subtype=Faerie
cost={1}{U}
pt=1/1
ability=Flying;\
        {T}: Target creature gains flying until end of turn.
timing=main
oracle=Flying\n{T}: Target creature gains flying until end of turn.
