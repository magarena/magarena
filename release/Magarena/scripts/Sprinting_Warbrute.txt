name=Sprinting Warbrute
image=https://cards.scryfall.io/border_crop/front/a/2/a215bde8-1680-4aea-9d0f-2d6d80a592e3.jpg?1562790827
value=2.500
rarity=C
type=Creature
subtype=Ogre,Berserker
cost={4}{R}
pt=5/4
ability=SN attacks each combat if able.;\
        Dash {3}{R}
timing=main
oracle=Sprinting Warbrute attacks each combat if able.\nDash {3}{R}
