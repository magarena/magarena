name=Cho-Manno, Revolutionary
image=https://cards.scryfall.io/border_crop/front/f/e/fe04dfe7-6376-4c12-9404-0e1ae0942917.jpg?1562558376
value=4.045
rarity=R
type=Legendary,Creature
subtype=Human,Rebel
cost={2}{W}{W}
pt=2/2
ability=Prevent all damage that would be dealt to SN.
timing=main
oracle=Prevent all damage that would be dealt to Cho-Manno, Revolutionary.
