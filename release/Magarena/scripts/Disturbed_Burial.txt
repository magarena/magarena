name=Disturbed Burial
image=https://cards.scryfall.io/border_crop/front/0/6/06254b6c-eb22-4ec9-9420-74e9ee15e072.jpg?1562052328
value=3.405
rarity=C
type=Sorcery
cost={1}{B}
ability=Buyback {3}
effect=Return target creature card from your graveyard to your hand.
timing=draw
oracle=Buyback {3}\nReturn target creature card from your graveyard to your hand.
