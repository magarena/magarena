name=Earth Surge
image=https://cards.scryfall.io/border_crop/front/3/2/325503ee-ea62-459b-a58a-53fb3c1ef027.jpg?1593272432
value=3.096
rarity=R
type=Enchantment
cost={3}{G}
timing=enchantment
requires_groovy_code
oracle=Each land gets +2/+2 as long as it's a creature.
