name=The Rack
image=https://cards.scryfall.io/border_crop/front/6/9/696d1e25-5c25-4522-b085-90d49fe23a18.jpg?1562777875
image_updated=2017-01-10
value=4.368
rarity=U
type=Artifact
cost={1}
ability=As SN enters the battlefield, choose an opponent.
timing=artifact
requires_groovy_code
oracle=As The Rack enters the battlefield, choose an opponent.\nAt the beginning of the chosen player's upkeep, The Rack deals X damage to that player, where X is 3 minus the number of cards in his or her hand.
