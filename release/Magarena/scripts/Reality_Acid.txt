name=Reality Acid
image=https://cards.scryfall.io/border_crop/front/a/8/a8510381-e05c-4cc4-98aa-c9327e18ec02.jpg?1619394529
value=3.636
removal=1
rarity=C
type=Enchantment
subtype=Aura
cost={2}{U}
ability=Enchant permanent;\
        Vanishing 3
timing=aura
enchant=sacrifice,neg permanent
oracle=Enchant permanent\nVanishing 3\nWhen Reality Acid leaves the battlefield, enchanted permanent's controller sacrifices it.
requires_groovy_code
