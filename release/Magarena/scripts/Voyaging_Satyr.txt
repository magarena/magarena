name=Voyaging Satyr
image=https://cards.scryfall.io/border_crop/front/1/8/180773da-dadc-414a-92c6-f4e13c753718.jpg?1576383133
image_updated=2017-01-11
value=3.531
rarity=C
type=Creature
subtype=Satyr,Druid
cost={1}{G}
pt=1/2
ability={T}: Untap target land.
timing=main
oracle={T}: Untap target land.
