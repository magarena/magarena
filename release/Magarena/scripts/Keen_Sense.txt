name=Keen Sense
image=https://cards.scryfall.io/border_crop/front/d/1/d16e927d-0f23-4fac-9b35-3cfcb22ebc26.jpg?1619398246
value=3.927
rarity=U
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant creature;\
        Whenever enchanted creature deals damage to an opponent, you may draw a card.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.
