name=Battle Rampart
image=https://cards.scryfall.io/border_crop/front/d/c/dca8dfb9-fda0-4eb8-a337-406689ece41a.jpg?1562939356
value=3.392
rarity=C
type=Creature
subtype=Wall
cost={2}{R}
pt=1/3
ability=Defender;\
        {T}: Target creature gains haste until end of turn.
timing=main
oracle=Defender\n{T}: Target creature gains haste until end of turn.
