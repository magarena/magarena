name=Bringer of the Red Dawn
image=https://cards.scryfall.io/border_crop/front/1/6/16a58bb1-4c4d-41f2-9c69-d3c3a8a8ab09.jpg?1562875616
value=4.196
rarity=R
type=Creature
subtype=Bringer
cost={7}{R}{R}
pt=5/5
ability=You may pay {W}{U}{B}{R}{G} rather than pay this spell's mana cost.;\
        Trample;\
        At the beginning of your upkeep, you may untap target creature.~Gain control of *it* until end of turn.~*That creature* gains haste until end of turn.
timing=main
oracle=You may pay {W}{U}{B}{R}{G} rather than pay this spell's mana cost.\nTrample\nAt the beginning of your upkeep, you may untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.
