name=Windwright Mage
image=https://cards.scryfall.io/border_crop/front/9/a/9aba0e7d-9146-402f-898e-16a0ff08d368.jpg?1562706980
value=2.971
rarity=C
type=Artifact,Creature
subtype=Human,Wizard
cost={W}{U}{B}
pt=2/2
ability=Lifelink;\
        SN has flying as long as an artifact card is in your graveyard.
timing=main
oracle=Lifelink\nWindwright Mage has flying as long as an artifact card is in your graveyard.
