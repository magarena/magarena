name=Defang
image=https://cards.scryfall.io/border_crop/front/f/1/f12fee0b-c237-4188-a718-1572f72c63ba.jpg?1592708261
value=2.103
rarity=C
type=Enchantment
subtype=Aura
cost={1}{W}
ability=Enchant creature
timing=aura
enchant=can't attack,neg creature
oracle=Enchant creature\nPrevent all damage that would be dealt by enchanted creature.
requires_groovy_code
