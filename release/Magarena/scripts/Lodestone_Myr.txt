name=Lodestone Myr
image=https://cards.scryfall.io/border_crop/front/6/1/617ddf01-3678-470d-b009-003d6a561dbf.jpg?1562263066
value=4.446
rarity=R
type=Artifact,Creature
subtype=Myr
cost={4}
pt=2/2
ability=Trample;\
        Tap an untapped artifact you control: SN gets +1/+1 until end of turn.
timing=main
oracle=Trample\nTap an untapped artifact you control: Lodestone Myr gets +1/+1 until end of turn.
