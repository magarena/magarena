name=Pulsating Illusion
image=https://cards.scryfall.io/border_crop/front/d/3/d3297f28-a15d-43dd-a96e-09701d5f9aed.jpg?1562934456
value=1.155
rarity=U
type=Creature
subtype=Illusion
cost={4}{U}
pt=0/1
ability=Flying;\
        Discard a card: SN gets +4/+4 until end of turn. Activate this ability only once each turn.
timing=main
oracle=Flying\nDiscard a card: Pulsating Illusion gets +4/+4 until end of turn. Activate this ability only once each turn.
