name=Dragonsoul Knight
image=https://cards.scryfall.io/border_crop/front/3/e/3ef2daf4-45ff-4604-b572-31dfaf1e70fe.jpg?1562261164
image_updated=2015-11-05
value=3.014
rarity=C
type=Creature
subtype=Human,Knight
cost={2}{R}
pt=2/2
ability=First strike;\
        {W}{U}{B}{R}{G}: Until end of turn, SN becomes a Dragon.~SN gets +5/+3 and gains flying and trample until end of turn.
timing=main
oracle=First strike\n{W}{U}{B}{R}{G}: Until end of turn, Dragonsoul Knight becomes a Dragon, gets +5/+3, and gains flying and trample.
