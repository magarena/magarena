name=Plague Spitter
image=https://cards.scryfall.io/border_crop/front/a/8/a82559e5-110b-4c6a-956a-7cba83788dc8.jpg?1674141759
value=4.246
rarity=U
type=Creature
subtype=Horror
cost={2}{B}
pt=2/2
ability=At the beginning of your upkeep, SN deals 1 damage to each creature and each player.;\
        When SN dies, SN deals 1 damage to each creature and each player.
timing=main
oracle=At the beginning of your upkeep, Plague Spitter deals 1 damage to each creature and each player.\nWhen Plague Spitter dies, Plague Spitter deals 1 damage to each creature and each player.
