name=Voracious Wurm
image=https://cards.scryfall.io/border_crop/front/d/a/da15100b-2934-438c-9917-84ad8bdc4181.jpg?1562835789
value=3.632
rarity=U
type=Creature
subtype=Wurm
cost={1}{G}
pt=2/2
timing=main
requires_groovy_code
oracle=Voracious Wurm enters the battlefield with X +1/+1 counters on it, where X is the amount of life you've gained this turn.
