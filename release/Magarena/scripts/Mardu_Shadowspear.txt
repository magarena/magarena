name=Mardu Shadowspear
image=https://cards.scryfall.io/border_crop/front/c/1/c1f55b50-b3b1-4336-914c-1cc2669b8fac.jpg?1562829538
value=2.500
rarity=U
type=Creature
subtype=Human,Warrior
cost={B}
pt=1/1
ability=Whenever SN attacks, each opponent loses 1 life.;\
        Dash {1}{B}
timing=main
oracle=Whenever Mardu Shadowspear attacks, each opponent loses 1 life.\nDash {1}{B}
