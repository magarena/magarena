name=Safe Haven
image=https://cards.scryfall.io/border_crop/front/d/1/d130205f-53e3-4c04-967c-4808d7945880.jpg?1562784189
value=3.500
rarity=R
type=Land
ability={2}, {T}: Exile target creature you control.
timing=land
requires_groovy_code
oracle={2}, {T}: Exile target creature you control.\nAt the beginning of your upkeep, you may sacrifice Safe Haven. If you do, return each card exiled with Safe Haven to the battlefield under its owner's control.
