name=Traumatize
image=https://cards.scryfall.io/border_crop/front/9/b/9b8784dd-83f9-41f8-aedc-f0f81073ffcb.jpg?1562832808
value=3.856
rarity=R
type=Sorcery
cost={3}{U}{U}
timing=main
requires_groovy_code
oracle=Target player puts the top half of his or her library, rounded down, into his or her graveyard.
