name=Aleatory
image=https://cards.scryfall.io/border_crop/front/7/3/7380af0a-8a8f-44fd-9456-14a68a2830d3.jpg?1562720221
value=1.157
rarity=U
type=Instant
cost={1}{R}
ability=Cast this spell only during combat after blockers are declared.
timing=removal
oracle=Cast this spell only during combat after blockers are declared.\nFlip a coin. If you win the flip, target creature gets +1/+1 until end of turn.\nDraw a card at the beginning of the next turn's upkeep.
requires_groovy_code
