name=Devouring Strossus
image=https://cards.scryfall.io/border_crop/front/0/6/064f013f-e74f-419d-8d17-7748bd91885e.jpg?1562896253
value=4.167
rarity=R
type=Creature
subtype=Horror
cost={5}{B}{B}{B}
pt=9/9
ability=Flying, trample;\
        At the beginning of your upkeep, sacrifice a creature.;\
        Sacrifice a creature: Regenerate SN. Activate this ability with AI only if you control another creature.
timing=main
oracle=Flying, trample\nAt the beginning of your upkeep, sacrifice a creature.\nSacrifice a creature: Regenerate Devouring Strossus.
