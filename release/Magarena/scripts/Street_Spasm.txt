name=Street Spasm
image=https://cards.scryfall.io/border_crop/front/9/2/92bc9bb0-e38f-48fb-8431-5e534fd0bba4.jpg?1562926021
value=3.250
rarity=U
type=Instant
cost={X}{R}
effect=SN deals X damage to target creature without flying you don't control.
ability=Cast SN with AI only if an opponent controls a creature without flying.
timing=removal
removal=1
requires_groovy_code
oracle=Street Spasm deals X damage to target creature without flying you don't control.\nOverload {X}{X}{R}{R}
