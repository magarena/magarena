name=Scalelord Reckoner
image=https://cards.scryfall.io/border_crop/front/c/9/c91c3606-d037-4b97-a7b6-ab1ed09bc976.jpg?1562623335
image_updated=2017-10-03
value=2.500
rarity=R
type=Creature
subtype=Dragon
cost={3}{W}{W}
pt=4/4
ability=Flying;\
        Whenever a Dragon you control becomes the target of a spell or ability an opponent controls, destroy target nonland permanent an opponent controls.
timing=main
oracle=Flying\nWhenever a Dragon you control becomes the target of a spell or ability an opponent controls, destroy target nonland permanent that player controls.
