name=Pious Interdiction
image=https://cards.scryfall.io/border_crop/front/6/7/670b9645-ce34-41eb-a527-4af3e13c3a54.jpg?1562556833
value=2.500
rarity=C
type=Enchantment
subtype=Aura
cost={3}{W}
ability=Enchant creature;\
        When SN enters the battlefield, you gain 2 life.;\
        Enchanted creature can't attack or block.
timing=aura
enchant=can't attack or block,neg creature
oracle=Enchant creature\nWhen Pious Interdiction enters the battlefield, you gain 2 life.\nEnchanted creature can't attack or block.
