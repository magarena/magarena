name=Wall of Hope
image=https://cards.scryfall.io/border_crop/front/b/4/b463b3e1-e314-4a65-a89e-0712f630b016.jpg?1562931313
value=3.663
rarity=C
type=Creature
subtype=Wall
cost={W}
pt=0/3
ability=Defender
timing=smain
oracle=Defender\nWhenever Wall of Hope is dealt damage, you gain that much life.
requires_groovy_code
