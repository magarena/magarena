name=Wolfbitten Captive
image=https://cards.scryfall.io/border_crop/front/1/3/1303e02a-ef69-4817-bca5-02c74774b811.jpg?1581395277
value=3.410
rarity=R
type=Creature
subtype=Human,Werewolf
cost={G}
pt=1/1
ability={1}{G}: SN gets +2/+2 until end of turn. Activate this ability only once each turn.;\
        At the beginning of each upkeep, if no spells were cast last turn, transform SN.
transform=Krallenhorde Killer
timing=main
oracle={1}{G}: Wolfbitten Captive gets +2/+2 until end of turn. Activate this ability only once each turn.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Wolfbitten Captive.
