name=Twinblade Slasher
image=https://cards.scryfall.io/border_crop/front/0/0/0092d7a0-bd00-45e5-a052-c0a9d970bc2e.jpg?1562895101
value=3.985
rarity=U
type=Creature
subtype=Elf,Warrior
cost={G}
pt=1/1
ability=Wither;\
        {1}{G}: SN gets +2/+2 until end of turn. Activate this ability only once each turn.
timing=main
oracle=Wither\n{1}{G}: Twinblade Slasher gets +2/+2 until end of turn. Activate this ability only once each turn.
