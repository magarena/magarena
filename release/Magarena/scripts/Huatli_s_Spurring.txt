name=Huatli's Spurring
image=https://cards.scryfall.io/border_crop/front/a/1/a19440e2-855f-4f60-a443-b99500394611.jpg?1562561243
value=2.500
rarity=U
type=Instant
cost={R}
timing=removal
requires_groovy_code
oracle=Target creature gets +2/+0 until end of turn. If you control a Huatli planeswalker, that creature gets +4/+0 until end of turn instead.
