name=Pariah's Shield
image=https://cards.scryfall.io/border_crop/front/c/c/cc316f1e-84ce-4013-a150-d537f964a604.jpg?1598918089
value=3.705
rarity=R
type=Artifact
subtype=Equipment
cost={5}
ability=Equip {3}
timing=equipment
requires_groovy_code
oracle=All damage that would be dealt to you is dealt to equipped creature instead.\nEquip {3}
