name=Putrid Cyclops
image=https://cards.scryfall.io/border_crop/front/1/4/14e82edf-8acc-4024-bb88-f727f632b061.jpg?1562899185
value=1.963
rarity=C
type=Creature
subtype=Zombie,Cyclops
cost={2}{B}
pt=3/3
timing=main
requires_groovy_code
oracle=When Putrid Cyclops enters the battlefield, scry 1, then reveal the top card of your library. Putrid Cyclops gets -X/-X until end of turn, where X is that card's converted mana cost.
