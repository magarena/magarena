name=Grixis Sojourners
image=https://cards.scryfall.io/border_crop/front/0/c/0cde5f43-0b91-45c9-9382-20ea6328266f.jpg?1562639621
value=2.510
rarity=C
type=Creature
subtype=Zombie,Ogre
cost={1}{U}{B}{R}
pt=4/3
ability=When you cycle SN, you may exile target card from a graveyard.;\
        When SN dies, you may exile target card from a graveyard.;\
        Cycling {2}{B}
timing=main
oracle=When you cycle Grixis Sojourners or it dies, you may exile target card from a graveyard.\nCycling {2}{B}
