name=Shadowed Caravel
image=https://cards.scryfall.io/border_crop/front/b/2/b28ab3ed-32cd-4168-9763-96884491aaa1.jpg?1562562453
value=2.500
rarity=R
type=Artifact
subtype=Vehicle
cost={2}
pt=2/2
ability=Whenever a creature you control explores, put a +1/+1 counter on SN.;\
        Crew 2
timing=artifact
oracle=Whenever a creature you control explores, put a +1/+1 counter on Shadowed Caravel.\nCrew 2
