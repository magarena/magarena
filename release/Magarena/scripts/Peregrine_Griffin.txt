name=Peregrine Griffin
image=https://cards.scryfall.io/border_crop/front/0/2/0296eaa6-f9fe-4fb8-af9c-04928d99e2e2.jpg?1562630555
value=2.305
rarity=C
type=Creature
subtype=Griffin
cost={4}{W}
pt=2/4
ability=Flying;\
        First strike
timing=main
oracle=Flying\nFirst strike
