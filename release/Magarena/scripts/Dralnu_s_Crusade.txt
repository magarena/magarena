name=Dralnu's Crusade
image=https://cards.scryfall.io/border_crop/front/6/a/6a35d227-4489-4a0b-8f81-eb8e5949e1fc.jpg?1562917197
value=4.043
rarity=R
type=Enchantment
cost={1}{B}{R}
ability=Goblin creatures get +1/+1.
timing=enchantment
oracle=Goblin creatures get +1/+1.\nAll Goblins are black and are Zombies in addition to their other creature types.
requires_groovy_code
