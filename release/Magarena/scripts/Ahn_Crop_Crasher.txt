name=Ahn-Crop Crasher
image=https://cards.scryfall.io/border_crop/front/d/d/dd5db68c-24d5-44ce-9809-e446f26fac5d.jpg?1543675472
value=2.500
rarity=U
type=Creature
subtype=Minotaur,Warrior
cost={2}{R}
pt=3/2
ability=Haste;\
        You may exert SN as it attacks. When you do, target creature can't block this turn.
timing=fmain
oracle=Haste\nYou may exert Ahn-Crop Crasher as it attacks. When you do, target creature can't block this turn.
