name=Cunning
image=https://cards.scryfall.io/border_crop/front/5/2/52f36bb8-5a97-4596-8ca3-707665770c76.jpg?1562087835
value=2.095
rarity=C
type=Enchantment
subtype=Aura
cost={1}{U}
ability=Enchant creature;\
        Enchanted creature gets +3/+3.;\
        When enchanted creature attacks or blocks, sacrifice SN at the beginning of the next end step.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature attacks or blocks, sacrifice Cunning at the beginning of the next cleanup step.
