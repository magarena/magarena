name=Indulgent Aristocrat
image=https://cards.scryfall.io/border_crop/front/3/7/37f6200d-caaf-49ca-b021-48186e404ace.jpg?1641602647
value=2.500
rarity=U
type=Creature
subtype=Vampire
cost={B}
pt=1/1
ability=Lifelink;\
        {2}, Sacrifice a creature: Put a +1/+1 counter on each Vampire you control.
timing=main
oracle=Lifelink\n{2}, Sacrifice a creature: Put a +1/+1 counter on each Vampire you control.
