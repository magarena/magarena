name=Skylasher
image=https://cards.scryfall.io/border_crop/front/4/f/4f4c2069-deb1-4e56-8069-170c4f495944.jpg?1562909545
value=3.829
rarity=R
type=Creature
subtype=Insect
cost={1}{G}
pt=2/2
ability=Flash;\
        This spell can't be countered.;\
        Reach, protection from blue
timing=flash
oracle=Flash\nThis spell can't be countered.\nReach, protection from blue
