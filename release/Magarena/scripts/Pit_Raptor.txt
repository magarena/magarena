name=Pit Raptor
image=https://cards.scryfall.io/border_crop/front/e/3/e37cd150-1064-43b7-919b-8922d8a18f21.jpg?1562937537
value=2.367
rarity=U
type=Creature
subtype=Bird,Mercenary
cost={2}{B}{B}
pt=4/3
ability=Flying, first strike;\
        At the beginning of your upkeep, sacrifice SN unless you pay {2}{B}{B}.
timing=main
oracle=Flying, first strike\nAt the beginning of your upkeep, sacrifice Pit Raptor unless you pay {2}{B}{B}.
