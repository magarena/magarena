name=Siren's Ruse
image=https://cards.scryfall.io/border_crop/front/8/f/8f389872-d1f4-4828-a527-e820a6f2ac21.jpg?1562559873
value=2.500
rarity=C
type=Instant
cost={1}{U}
timing=removal
requires_groovy_code
oracle=Exile target creature you control, then return that card to the battlefield under its owner's control. If a Pirate was exiled this way, draw a card.
