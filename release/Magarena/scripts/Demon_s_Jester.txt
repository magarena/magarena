name=Demon's Jester
image=https://cards.scryfall.io/border_crop/front/f/4/f4213f45-e8b6-4d90-a3b0-e3d37f745c97.jpg?1561771992
image_updated=2016-11-30
value=2.152
rarity=C
type=Creature
subtype=Imp
cost={3}{B}
pt=2/2
ability=Flying;\
        SN gets +2/+1 as long as you have no cards in hand.
timing=main
oracle=Flying\nHellbent — Demon's Jester gets +2/+1 as long as you have no cards in hand.
