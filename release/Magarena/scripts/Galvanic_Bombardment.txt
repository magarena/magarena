name=Galvanic Bombardment
image=https://cards.scryfall.io/border_crop/front/d/c/dc6a4367-bbee-43b8-b5dd-28498f8e2ede.jpg?1576384589
image_updated=2016-09-05
value=2.500
rarity=C
type=Instant
cost={R}
timing=removal
requires_groovy_code
oracle=Galvanic Bombardment deals X damage to target creature, where X is 2 plus the number of cards named Galvanic Bombardment in your graveyard.
