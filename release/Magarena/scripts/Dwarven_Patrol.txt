name=Dwarven Patrol
image=https://cards.scryfall.io/border_crop/front/0/3/03c08df5-f5e7-4498-ac80-25ccbe304b26.jpg?1562895948
value=2.735
rarity=U
type=Creature
subtype=Dwarf
cost={2}{R}
pt=4/2
ability=SN doesn't untap during your untap step.;\
        Whenever you cast a nonred spell, untap SN.
timing=main
oracle=Dwarven Patrol doesn't untap during your untap step.\nWhenever you cast a nonred spell, untap Dwarven Patrol.
