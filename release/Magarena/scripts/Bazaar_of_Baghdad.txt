name=Bazaar of Baghdad
image=https://cards.scryfall.io/border_crop/front/c/8/c88acaa8-ad4d-4321-a6f6-9361916e5b5e.jpg?1653966861
value=4.000
rarity=M
type=Land
ability={T}: Draw two cards, then discard three cards.
timing=land
oracle={T}: Draw two cards, then discard three cards.
