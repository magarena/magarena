name=Magefire Wings
image=https://cards.scryfall.io/border_crop/front/b/e/be18c2d9-60c4-454b-b6c7-7c22ed2985fb.jpg?1562644002
value=2.610
rarity=C
type=Enchantment
subtype=Aura
cost={U}{R}
ability=Enchant creature;\
        Enchanted creature gets +2/+0 and has flying.
timing=aura
enchant=flying,pos creature
oracle=Enchant creature\nEnchanted creature gets +2/+0 and has flying.
