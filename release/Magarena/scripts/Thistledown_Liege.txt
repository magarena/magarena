name=Thistledown Liege
image=https://cards.scryfall.io/border_crop/front/4/1/41a21a0c-fe3d-456b-a905-8d962c0b1e3c.jpg?1673149204
value=3.268
rarity=R
type=Creature
subtype=Kithkin,Knight
cost={1}{W/U}{W/U}{W/U}
pt=1/3
ability=Flash;\
        Other white creatures you control get +1/+1.;\
        Other blue creatures you control get +1/+1.
static=player
timing=flash
oracle=Flash\nOther white creatures you control get +1/+1.\nOther blue creatures you control get +1/+1.
