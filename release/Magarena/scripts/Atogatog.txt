name=Atogatog
image=https://cards.scryfall.io/border_crop/front/4/a/4a3e6eb5-6d0f-4f82-86f9-bbce8d27afbb.jpg?1562908561
value=3.532
rarity=R
type=Legendary,Creature
subtype=Atog
cost={W}{U}{B}{R}{G}
pt=5/5
timing=main
requires_groovy_code
oracle=Sacrifice an Atog creature: Atogatog gets +X/+X until end of turn, where X is the sacrificed creature's power.
