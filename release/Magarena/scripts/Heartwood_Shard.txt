name=Heartwood Shard
image=https://cards.scryfall.io/border_crop/front/b/1/b13328a9-fc03-4c1b-b1b7-61cd41766300.jpg?1562155015
value=1.732
rarity=U
type=Artifact
cost={3}
ability={3}, {T}: Target creature gains trample until end of turn.;\
        {G}, {T}: Target creature gains trample until end of turn.
timing=artifact
oracle={3}, {T} or {G}, {T}: Target creature gains trample until end of turn.
