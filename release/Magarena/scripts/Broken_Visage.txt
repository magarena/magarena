name=Broken Visage
image=https://cards.scryfall.io/border_crop/front/9/e/9e934eed-d37c-4ede-b867-87bd17269b78.jpg?1562869987
value=3.485
rarity=R
type=Instant
cost={4}{B}
timing=removal
oracle=Destroy target nonartifact attacking creature. It can't be regenerated. Create a black Spirit creature token. Its power is equal to that creature's power and its toughness is equal to that creature's toughness. Sacrifice the token at the beginning of the next end step.
requires_groovy_code
