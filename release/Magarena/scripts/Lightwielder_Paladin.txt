name=Lightwielder Paladin
image=https://cards.scryfall.io/border_crop/front/b/6/b61a3ca4-f311-412a-927f-148e614846f6.jpg?1592765356
value=3.662
rarity=R
type=Creature
subtype=Human,Knight
cost={3}{W}{W}
pt=4/4
ability=First strike;\
        Whenever SN deals combat damage to a player, you may exile target black or red permanent that player controls.
timing=main
oracle=First strike\nWhenever Lightwielder Paladin deals combat damage to a player, you may exile target black or red permanent that player controls.
