name=Kry Shield
image=https://cards.scryfall.io/border_crop/front/a/5/a558f23c-c2ce-40d0-b894-f8ccbff8f622.jpg?1562860621
value=2.222
rarity=U
type=Artifact
cost={2}
timing=artifact
requires_groovy_code
oracle={2}, {T}: Prevent all damage that would be dealt this turn by target creature you control. That creature gets +0/+X until end of turn, where X is its converted mana cost.
