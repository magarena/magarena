name=Furnace Spirit
image=https://cards.scryfall.io/border_crop/front/b/6/b6a79dc7-ce46-41f7-9375-8d12afe6355a.jpg?1562597360
value=2.086
rarity=C
type=Creature
subtype=Spirit
cost={2}{R}
pt=1/1
ability=Haste;\
        {R}: SN gets +1/+0 until end of turn.
timing=fmain
oracle=Haste\n{R}: Furnace Spirit gets +1/+0 until end of turn.
