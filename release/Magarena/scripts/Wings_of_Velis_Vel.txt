name=Wings of Velis Vel
image=https://cards.scryfall.io/border_crop/front/c/c/cc5f287b-beaa-4b51-8db7-91982e2c0bb0.jpg?1562267396
value=3.188
rarity=C
type=Tribal,Instant
subtype=Shapeshifter
cost={1}{U}
ability=Changeling
timing=pump
requires_groovy_code
oracle=Changeling\nUntil end of turn, target creature has base power and toughness 4/4, gains all creature types, and gains flying.
