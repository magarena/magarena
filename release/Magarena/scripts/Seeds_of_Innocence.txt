name=Seeds of Innocence
image=https://cards.scryfall.io/border_crop/front/f/9/f9c868f5-0f90-4f7e-bafb-c45d2372fe06.jpg?1614021484
value=3.024
rarity=R
type=Sorcery
cost={1}{G}{G}
timing=main
requires_groovy_code
oracle=Destroy all artifacts. They can't be regenerated. The controller of each of those artifacts gains life equal to its converted mana cost.
