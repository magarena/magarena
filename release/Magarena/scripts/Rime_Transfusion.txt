name=Rime Transfusion
image=https://cards.scryfall.io/border_crop/front/e/1/e18318fe-b429-459f-9b70-d444b48b8dc7.jpg?1593275154
value=3.538
rarity=U
type=Snow,Enchantment
subtype=Aura
cost={1}{B}
ability=Enchant creature;\
        Enchanted creature gets +2/+1 and has "{S}: This creature can't be blocked except by snow creatures this turn."
timing=aura
enchant=unblockable,pos creature
oracle=Enchant creature\nEnchanted creature gets +2/+1 and has "{S}: This creature can't be blocked this turn except by snow creatures."
