name=Genju of the Cedars
image=https://cards.scryfall.io/border_crop/front/1/2/1254929b-0be1-4068-af69-7e59840c8b98.jpg?1562899373
image_updated=2016-12-03
value=4.112
rarity=U
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant Forest
timing=aura
enchant=pump,pos forest
oracle=Enchant Forest\n{2}: Enchanted Forest becomes a 4/4 green Spirit creature until end of turn. It's still a land.\nWhen enchanted Forest is put into a graveyard, you may return Genju of the Cedars from your graveyard to your hand.
requires_groovy_code
