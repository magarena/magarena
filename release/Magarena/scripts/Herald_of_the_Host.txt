name=Herald of the Host
image=https://cards.scryfall.io/border_crop/front/5/7/57971e1c-6aff-45ea-a080-0dae642dbcf6.jpg?1562273922
image_updated=2016-02-29
value=2.500
rarity=U
type=Creature
subtype=Angel
cost={3}{W}{W}
pt=4/4
ability=Flying, vigilance;\
        Myriad
timing=main
oracle=Flying, vigilance\nMyriad
