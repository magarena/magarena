name=Aphotic Wisps
image=https://cards.scryfall.io/border_crop/front/5/2/52fd47e7-02c5-4f91-bb3b-697c4c53232a.jpg?1562830208
value=3.298
rarity=C
type=Instant
cost={B}
effect=Target creature becomes black and gains fear until end of turn.~Draw a card.
timing=pump
oracle=Target creature becomes black and gains fear until end of turn.\nDraw a card.
