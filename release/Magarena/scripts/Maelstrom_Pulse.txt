name=Maelstrom Pulse
image=https://cards.scryfall.io/border_crop/front/6/7/676ab73f-2759-43b6-9ae8-ca33a55ebf80.jpg?1599708139
value=4.556
removal=5
rarity=R
type=Sorcery
cost={1}{B}{G}
timing=removal
requires_groovy_code
oracle=Destroy target nonland permanent and all other permanents with the same name as that permanent.
