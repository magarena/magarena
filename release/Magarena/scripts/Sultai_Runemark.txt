name=Sultai Runemark
image=https://cards.scryfall.io/border_crop/front/9/8/98d73ae0-de01-4a70-a51c-b990687561a1.jpg?1562827948
value=2.500
rarity=C
type=Enchantment
subtype=Aura
cost={2}{B}
ability=Enchant creature;\
        Enchanted creature gets +2/+2.;\
        Enchanted creature has deathtouch as long as you control a green or blue permanent.
timing=aura
enchant=pump,pos creature
oracle=Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has deathtouch as long as you control a green or blue permanent.
