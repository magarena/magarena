name=Nevinyrral's Disk
image=https://cards.scryfall.io/border_crop/front/5/c/5c9e6af4-522c-4dfa-895a-6946fe983e3c.jpg?1626100951
image_updated=2017-08-17
value=4.500
rarity=R
type=Artifact
cost={4}
ability=SN enters the battlefield tapped.;\
        {1}, {T}: Destroy all artifacts, creatures, and enchantments.
timing=artifact
oracle=Nevinyrral's Disk enters the battlefield tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.
