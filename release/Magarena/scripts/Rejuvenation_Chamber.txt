name=Rejuvenation Chamber
image=https://cards.scryfall.io/border_crop/front/b/9/b97f86ce-4758-4ae3-af29-b08a4d771652.jpg?1562631696
value=1.672
rarity=U
type=Artifact
cost={3}
ability=Fading 2;\
        {T}: You gain 2 life.
timing=artifact
oracle=Fading 2\n{T}: You gain 2 life.
