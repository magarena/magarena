name=Wind-Scarred Crag
image=https://cards.scryfall.io/border_crop/front/7/f/7f2642cd-e3cc-4aab-8c00-4987284509b3.jpg?1682205924
value=3.000
rarity=C
type=Land
mana=r3w3
ability=SN enters the battlefield tapped.;\
        When SN enters the battlefield, you gain 1 life.;\
        {T}: Add {R} or {W}.
timing=tapland
oracle=Wind-Scarred Crag enters the battlefield tapped.\nWhen Wind-Scarred Crag enters the battlefield, you gain 1 life.\n{T}: Add {R} or {W}.
