name=Fervent Cathar
image=https://cards.scryfall.io/border_crop/front/9/c/9c00c27c-7826-4d6e-9d56-2844c59c8066.jpg?1580014528
value=2.564
rarity=C
type=Creature
subtype=Human,Knight
cost={2}{R}
pt=2/1
ability=Haste;\
        When SN enters the battlefield, target creature can't block this turn.
timing=fmain
oracle=Haste\nWhen Fervent Cathar enters the battlefield, target creature can't block this turn.
