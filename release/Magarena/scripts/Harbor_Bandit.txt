name=Harbor Bandit
image=https://cards.scryfall.io/border_crop/front/8/4/8422e109-de8d-46ea-a7f8-d5ccb6340497.jpg?1562556368
value=3.258
rarity=U
type=Creature
subtype=Human,Rogue
cost={2}{B}
pt=2/2
ability=SN gets +1/+1 as long as you control an Island.;\
        {1}{U}: SN can't be blocked this turn.
timing=main
oracle=Harbor Bandit gets +1/+1 as long as you control an Island.\n{1}{U}: Harbor Bandit can't be blocked this turn.
