name=Kor Skyfisher
image=https://cards.scryfall.io/border_crop/front/d/7/d7501662-1216-4e08-bd2b-e0a459057942.jpg?1593812836
image_updated=2017-08-08
value=3.828
rarity=C
type=Creature
subtype=Kor,Soldier
cost={1}{W}
pt=2/3
ability=Flying;\
        When SN enters the battlefield, return a permanent you control to its owner's hand.
timing=main
oracle=Flying\nWhen Kor Skyfisher enters the battlefield, return a permanent you control to its owner's hand.
