name=Illusionary Presence
image=https://cards.scryfall.io/border_crop/front/a/a/aa31efed-4a11-4f59-a623-bac45d20091d.jpg?1587911810
value=2.212
rarity=R
type=Creature
subtype=Illusion
cost={1}{U}{U}
pt=2/2
ability=Cumulative upkeep {U}
timing=main
requires_groovy_code
oracle=Cumulative upkeep {U}\nAt the beginning of your upkeep, choose a land type. Illusionary Presence gains landwalk of the chosen type until end of turn.
