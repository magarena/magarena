name=Razormane Masticore
image=https://cards.scryfall.io/border_crop/front/5/e/5e40c79e-3177-416f-b10e-036553d611d7.jpg?1562270229
image_updated=2017-01-06
value=4.509
rarity=R
type=Artifact,Creature
subtype=Masticore
cost={5}
pt=5/5
ability=First strike;\
        At the beginning of your upkeep, sacrifice SN unless you discard a card.;\
        At the beginning of your draw step, you may have SN deal 3 damage to target creature.
timing=main
oracle=First strike\nAt the beginning of your upkeep, sacrifice Razormane Masticore unless you discard a card.\nAt the beginning of your draw step, you may have Razormane Masticore deal 3 damage to target creature.
