name=Guul Draz Specter
image=https://cards.scryfall.io/border_crop/front/e/d/edb0546f-f893-460c-8af4-79b24c373ebe.jpg?1576382660
image_updated=2017-08-29
value=3.867
rarity=R
type=Creature
subtype=Specter
cost={2}{B}{B}
pt=2/2
ability=Flying;\
        SN gets +3/+3 as long as an opponent has no cards in hand.;\
        Whenever SN deals combat damage to a player, that player discards a card.
timing=main
oracle=Flying\nGuul Draz Specter gets +3/+3 as long as an opponent has no cards in hand.\nWhenever Guul Draz Specter deals combat damage to a player, that player discards a card.
