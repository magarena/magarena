name=Shuko
image=https://cards.scryfall.io/border_crop/front/a/4/a47456b8-cef8-4085-90b1-92788e16fd27.jpg?1562878892
value=4.169
rarity=U
type=Artifact
subtype=Equipment
cost={1}
ability=Equipped creature gets +1/+0.;\
        Equip {0}
timing=equipment
oracle=Equipped creature gets +1/+0.\nEquip {0}
