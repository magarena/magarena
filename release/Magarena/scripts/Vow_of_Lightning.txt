name=Vow of Lightning
image=https://cards.scryfall.io/border_crop/front/7/1/71218cff-7e57-40dd-83c2-d06b284a63fd.jpg?1608910481
value=3.485
rarity=U
type=Enchantment
subtype=Aura
cost={2}{R}
ability=Enchant creature;\
        Enchanted creature gets +2/+2 and has first strike.;\
        As long as it's not your turn, enchanted creature can't attack.
timing=aura
enchant=default,creature
oracle=Enchant creature\nEnchanted creature gets +2/+2, has first strike, and can't attack you or a planeswalker you control.
