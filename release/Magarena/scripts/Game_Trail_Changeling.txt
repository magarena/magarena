name=Game-Trail Changeling
image=https://cards.scryfall.io/border_crop/front/7/b/7bc76a7f-516f-4d0b-b024-39d5c8096a9f.jpg?1562879466
value=3.033
rarity=C
type=Creature
subtype=Shapeshifter
cost={3}{G}{G}
pt=4/4
ability=Changeling;\
        Trample
timing=main
oracle=Changeling\nTrample
