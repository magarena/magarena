name=Obzedat, Ghost Council
image=https://cards.scryfall.io/border_crop/front/4/0/4029c3a0-a999-453a-a838-7adb81e481ee.jpg?1593814495
image_updated=2017-08-08
value=4.035
rarity=R
type=Legendary,Creature
subtype=Spirit,Advisor
cost={1}{W}{W}{B}{B}
pt=5/5
ability=When SN enters the battlefield, target opponent loses 2 life and you gain 2 life.
timing=main
requires_groovy_code
oracle=When Obzedat, Ghost Council enters the battlefield, target opponent loses 2 life and you gain 2 life.\nAt the beginning of your end step, you may exile Obzedat. If you do, return it to the battlefield under its owner's control at the beginning of your next upkeep. It gains haste.
