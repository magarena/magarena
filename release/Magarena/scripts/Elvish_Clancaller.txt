name=Elvish Clancaller
image=https://cards.scryfall.io/border_crop/front/4/d/4da3b0bc-84da-4b85-992b-d0700c97157c.jpg?1616182131
value=2.500
rarity=R
type=Creature
subtype=Elf,Druid
cost={G}{G}
pt=1/1
ability=Other Elves you control get +1/+1.;\
        {4}{G}{G}, {T}: Search your library for a card named Elvish Clancaller, put it onto the battlefield, then shuffle your library.
timing=main
oracle=Other Elves you control get +1/+1.\n{4}{G}{G}, {T}: Search your library for a card named Elvish Clancaller, put it onto the battlefield, then shuffle your library.
