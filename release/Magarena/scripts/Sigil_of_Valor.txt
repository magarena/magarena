name=Sigil of Valor
image=https://cards.scryfall.io/border_crop/front/f/1/f134f242-cbb5-4a3d-bfd4-46e8b8669b4b.jpg?1562049228
image_updated=2015-11-05
value=2.500
rarity=U
type=Artifact
subtype=Equipment
cost={2}
ability=Equip {1}
timing=equipment
requires_groovy_code
oracle=Whenever equipped creature attacks alone, it gets +1/+1 until end of turn for each other creature you control.\nEquip {1}
