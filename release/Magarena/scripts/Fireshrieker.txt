name=Fireshrieker
image=https://cards.scryfall.io/border_crop/front/a/b/ab1f2ab8-1ef3-4610-a2d0-cb87b982c649.jpg?1651656287
value=3.625
rarity=U
type=Artifact
subtype=Equipment
cost={3}
ability=Equipped creature has double strike.;\
        Equip {2}
timing=equipment
oracle=Equipped creature has double strike.\nEquip {2}
