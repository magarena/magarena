name=Trygon Predator
image=https://cards.scryfall.io/border_crop/front/7/c/7c03836e-a7a8-4a3e-8d93-6cce89c2fed1.jpg?1625978140
value=4.577
rarity=U
type=Creature
subtype=Beast
cost={1}{G}{U}
pt=2/3
ability=Flying;\
        Whenever SN deals combat damage to a player, you may destroy target artifact or enchantment that player controls.
timing=main
oracle=Flying\nWhenever Trygon Predator deals combat damage to a player, you may destroy target artifact or enchantment that player controls.
