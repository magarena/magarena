name=Goblin Battle Jester
image=https://cards.scryfall.io/border_crop/front/c/1/c13e56b0-becc-4bc2-9ba3-23b3ca8bfe58.jpg?1562559606
value=2.359
rarity=C
type=Creature
subtype=Goblin
cost={3}{R}
pt=2/2
ability=Whenever you cast a red spell, target creature can't block this turn.
timing=main
oracle=Whenever you cast a red spell, target creature can't block this turn.
