name=Kami of the Waning Moon
image=https://cards.scryfall.io/border_crop/front/2/3/23a395a6-b1f5-4b7f-87cc-c77b4d497e9a.jpg?1587857631
value=2.086
rarity=C
type=Creature
subtype=Spirit
cost={2}{B}
pt=1/1
ability=Flying;\
        Whenever you cast a Spirit or Arcane spell, target creature gains fear until end of turn.
timing=main
oracle=Flying\nWhenever you cast a Spirit or Arcane spell, target creature gains fear until end of turn.
