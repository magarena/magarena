name=Cephalid Coliseum
image=https://cards.scryfall.io/border_crop/front/d/5/d5d74112-7244-4c3f-a5eb-b6be671aefe8.jpg?1562934946
value=4.012
rarity=U
type=Land
mana=u2
ability={T}: Add {U}. SN deals 1 damage to you.;\
        {U}, {T}, Sacrifice SN: Target player draws three cards, then discards three cards. Activate this ability only if seven or more cards are in your graveyard.
timing=land
oracle={T}: Add {U}. Cephalid Coliseum deals 1 damage to you.\nThreshold — {U}, {T}, Sacrifice Cephalid Coliseum: Target player draws three cards, then discards three cards. Activate this ability only if seven or more cards are in your graveyard.
