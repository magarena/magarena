name=Soulless One
image=https://cards.scryfall.io/border_crop/front/4/1/410a214b-09c4-49bd-a461-3330d0249ae5.jpg?1562841695
image_updated=2017-01-08
value=4.383
rarity=U
type=Creature
subtype=Zombie,Avatar
cost={3}{B}
pt=*/*
timing=main
requires_groovy_code
oracle=Soulless One's power and toughness are each equal to the number of Zombies on the battlefield plus the number of Zombie cards in all graveyards.
