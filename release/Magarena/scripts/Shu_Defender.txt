name=Shu Defender
image=https://cards.scryfall.io/border_crop/front/e/e/ee57a9ab-c385-4a51-aff7-6a654f5d7611.jpg?1562258001
value=2.333
rarity=C
type=Creature
subtype=Human,Soldier
cost={2}{W}
pt=2/2
ability=Whenever SN blocks, SN gets +0/+2 until end of turn.
timing=main
oracle=Whenever Shu Defender blocks, it gets +0/+2 until end of turn.
