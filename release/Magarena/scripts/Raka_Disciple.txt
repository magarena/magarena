name=Raka Disciple
image=https://cards.scryfall.io/border_crop/front/4/1/41462d43-4f9f-46ba-b79d-434597e74b6b.jpg?1562910379
value=2.983
rarity=C
type=Creature
subtype=Minotaur,Wizard
cost={R}
pt=1/1
ability={W}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.;\
        {U}, {T}: Target creature gains flying until end of turn.
timing=main
oracle={W}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{U}, {T}: Target creature gains flying until end of turn.
