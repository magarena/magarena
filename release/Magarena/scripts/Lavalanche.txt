name=Lavalanche
image=https://cards.scryfall.io/border_crop/front/e/3/e3a3b450-d988-40cb-a1ee-8b00ccdf5071.jpg?1673305568
image_updated=2017-08-17
value=3.594
rarity=R
type=Sorcery
cost={X}{B}{R}{G}
timing=removal
removal=2
requires_groovy_code=Bonfire of the Damned
oracle=Lavalanche deals X damage to target player and each creature he or she controls.
