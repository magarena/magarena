name=colorless artifact token named Land Mine
image=https://api.scryfall.com/cards/tm15/12/en?format=image&version=border_crop
token=Land Mine
type=Artifact
color=
value=1
ability={R}, Sacrifice SN: SN deals 2 damage to target attacking creature without flying.
oracle={R}, Sacrifice this artifact: This artifact deals 2 damage to target attacking creature without flying.
