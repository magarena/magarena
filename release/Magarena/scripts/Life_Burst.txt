name=Life Burst
image=https://cards.scryfall.io/border_crop/front/7/f/7f8eee50-efd2-45fd-b815-051167ef4541.jpg?1562918634
value=3.269
rarity=C
type=Instant
cost={1}{W}
timing=pump
requires_groovy_code
oracle=Target player gains 4 life, then gains 4 life for each card named Life Burst in each graveyard.
