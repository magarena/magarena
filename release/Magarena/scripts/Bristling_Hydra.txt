name=Bristling Hydra
image=https://cards.scryfall.io/border_crop/front/4/b/4bf38096-05bc-4734-b33c-eb1e8f2986de.jpg?1576382395
image_updated=2016-10-05
value=2.500
rarity=R
type=Creature
subtype=Hydra
cost={2}{G}{G}
pt=4/3
ability=When SN enters the battlefield, you get {E}{E}{E}.;\
        Pay {E}{E}{E}: Put a +1/+1 counter on SN.~It gains hexproof until end of turn.
timing=main
oracle=When Bristling Hydra enters the battlefield, you get {E}{E}{E}.\nPay {E}{E}{E}: Put a +1/+1 counter on Bristling Hydra. It gains hexproof until end of turn.
