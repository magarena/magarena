name=Thawing Glaciers
image=https://cards.scryfall.io/border_crop/front/3/9/397facec-f473-45a5-a4ce-02cb56e7bfab.jpg?1562906577
value=4.000
rarity=R
type=Land
ability=SN enters the battlefield tapped.;\
        {1}, {T}: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.~Return SN to its owner's hand at the beginning of the next end step.
timing=land
oracle=Thawing Glaciers enters the battlefield tapped.\n{1}, {T}: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library. Return Thawing Glaciers to its owner's hand at the beginning of the next cleanup step.
#Should be cleanup step
