name=Soldier of Fortune
image=https://cards.scryfall.io/border_crop/front/3/7/37c05f46-2081-4ebb-a758-894ac040ea2a.jpg?1562768246
value=3.758
rarity=U
type=Creature
subtype=Human,Mercenary
cost={R}
pt=1/1
timing=main
requires_groovy_code
oracle={R}, {T}: Target player shuffles his or her library.
