name=Sword of Vengeance
image=https://cards.scryfall.io/border_crop/front/9/d/9dd53110-bb42-4c42-851f-d2688c7c17a2.jpg?1645328007
image_updated=2016-02-29
value=4.282
rarity=R
type=Artifact
subtype=Equipment
cost={3}
ability=Equipped creature gets +2/+0 and has first strike, vigilance, trample, and haste.;\
        Equip {3}
timing=equipment
oracle=Equipped creature gets +2/+0 and has first strike, vigilance, trample, and haste.\nEquip {3}
