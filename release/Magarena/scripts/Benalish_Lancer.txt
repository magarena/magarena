name=Benalish Lancer
image=https://cards.scryfall.io/border_crop/front/8/4/8453b08f-34cb-491d-be7d-dc4022f7c131.jpg?1562922912
value=3.132
rarity=C
type=Creature
subtype=Human,Knight
cost={2}{W}
pt=2/2
timing=main
ability=kicker {2}{W}
requires_groovy_code
oracle=Kicker {2}{W}\nIf Benalish Lancer was kicked, it enters the battlefield with two +1/+1 counters on it and with first strike.
