name=Ruham Djinn
image=https://cards.scryfall.io/border_crop/front/a/4/a46c7718-1ecc-418c-b213-13be9de5cb7f.jpg?1562928232
value=1.959
rarity=U
type=Creature
subtype=Djinn
cost={5}{W}
pt=5/5
ability=First strike
timing=main
requires_groovy_code
oracle=First strike\nRuham Djinn gets -2/-2 as long as white is the most common color among all permanents or is tied for most common.
