name=Pegasus Courser
image=https://cards.scryfall.io/border_crop/front/0/c/0c04eafe-8be0-416e-a5b9-486a3b3b5984.jpg?1562300324
image_updated=2018-08-07
value=2.500
rarity=C
type=Creature
subtype=Pegasus
cost={2}{W}
pt=1/3
ability=Flying;\
        Whenever SN attacks, another target attacking creature gains flying until end of turn.
timing=main
oracle=Flying\nWhenever Pegasus Courser attacks, another target attacking creature gains flying until end of turn.
