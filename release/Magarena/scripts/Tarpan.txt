name=Tarpan
image=https://cards.scryfall.io/border_crop/front/d/2/d2160d57-9ebf-43fb-811f-0c014e417ea0.jpg?1562594253
value=2.457
rarity=C
type=Creature
subtype=Horse
cost={G}
pt=1/1
ability=When SN dies, you gain 1 life.
timing=main
oracle=When Tarpan dies, you gain 1 life.
