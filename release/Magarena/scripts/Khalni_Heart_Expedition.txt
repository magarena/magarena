name=Khalni Heart Expedition
image=https://cards.scryfall.io/border_crop/front/4/5/45cbbcfb-1b1b-4991-988d-17a22a08d5b0.jpg?1608917462
value=3.860
rarity=C
type=Enchantment
cost={1}{G}
ability=Whenever a land enters the battlefield under your control, you may put a quest counter on SN.;\
        Remove three quest counters from SN, Sacrifice SN: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.
timing=enchantment
oracle=Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Khalni Heart Expedition.\nRemove three quest counters from Khalni Heart Expedition and sacrifice it: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.
