name=Rith's Grove
image=https://cards.scryfall.io/border_crop/front/7/4/740fa25d-9c1f-44eb-9eb4-0dd514cb315a.jpg?1562919410
value=3.953
rarity=U
type=Land
subtype=Lair
mana=r2g2w2
ability=When SN enters the battlefield, sacrifice SN unless you return a non-Lair land you control to its owner's hand.;\
        {T}: Add {R}, {G}, or {W}.
timing=land
oracle=When Rith's Grove enters the battlefield, sacrifice it unless you return a non-Lair land you control to its owner's hand.\n{T}: Add {R}, {G}, or {W}.
