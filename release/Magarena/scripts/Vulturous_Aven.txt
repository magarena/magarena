name=Vulturous Aven
image=https://cards.scryfall.io/border_crop/front/b/2/b2fb0cc9-aebd-4bbf-8735-add3b753c118.jpg?1562791790
value=2.500
rarity=C
type=Creature
subtype=Bird,Shaman
cost={3}{B}
pt=2/3
ability=Flying;\
        Exploit;\
        When SN exploits a creature, you draw two cards and you lose 2 life.
timing=main
oracle=Flying\nExploit\nWhen Vulturous Aven exploits a creature, you draw two cards and you lose 2 life.
