name=Wingbeat Warrior
image=https://cards.scryfall.io/border_crop/front/c/d/cd58d164-861d-4c80-ad2f-6283ed82faa1.jpg?1562936257
value=2.706
rarity=C
type=Creature
subtype=Bird,Soldier,Warrior
cost={2}{W}
pt=2/1
ability=Flying;\
        Morph {2}{W};\
        When SN is turned face up, target creature gains first strike until end of turn.
timing=main
oracle=Flying\nMorph {2}{W}\nWhen Wingbeat Warrior is turned face up, target creature gains first strike until end of turn.
