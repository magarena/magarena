name=Tezzeret the Schemer
image=https://cards.scryfall.io/border_crop/front/5/8/58265203-bc16-41d2-875c-2ff3b4870824.jpg?1576382161
value=2.500
rarity=M
type=Legendary,Planeswalker
subtype=Tezzeret
cost={2}{U}{B}
loyalty=5
ability=+1: Create a colorless artifact token named Etherium Cell.;\
        −2: Target creature gets +X/-X until end of turn, where X is the number of artifacts you control.;\
        −7: You get an emblem with "At the beginning of combat on your turn, target artifact you control becomes an artifact creature with base power and toughness 5/5."
timing=main
oracle=+1: Create a colorless artifact token named Etherium Cell with "{T}, Sacrifice this artifact: Add one mana of any color."\n−2: Target creature gets +X/-X until end of turn, where X is the number of artifacts you control.\n−7: You get an emblem with "At the beginning of combat on your turn, target artifact you control becomes an artifact creature with base power and toughness 5/5."
