name=Nearheath Pilgrim
image=https://cards.scryfall.io/border_crop/front/d/8/d81d6fe0-c7c2-46a6-811c-f121284937ea.jpg?1592708373
value=3.704
rarity=U
type=Creature
subtype=Human,Cleric
cost={1}{W}
pt=2/1
ability=Soulbond;\
        As long as SN is paired with another creature, both creatures have lifelink.
timing=main
oracle=Soulbond\nAs long as Nearheath Pilgrim is paired with another creature, both creatures have lifelink.
