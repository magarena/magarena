name=Spire Winder
image=https://cards.scryfall.io/border_crop/front/a/c/aca438d7-1134-4fee-9fad-85c3b1abe8ed.jpg?1555040066
value=2.500
rarity=C
type=Creature
subtype=Snake
cost={3}{U}
pt=2/3
ability=Flying;\
        Ascend;\
        SN gets +1/+1 as long as you have the city's blessing.
timing=main
oracle=Flying\nAscend\nSpire Winder gets +1/+1 as long as you have the city's blessing.
