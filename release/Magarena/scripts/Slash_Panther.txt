name=Slash Panther
image=https://cards.scryfall.io/border_crop/front/2/f/2f510946-34de-4c12-8998-f61887d1a0e1.jpg?1562876395
value=3.908
rarity=C
type=Artifact,Creature
subtype=Cat
cost={4}{R/P}
pt=4/2
ability=Haste;\
        alt cost {4}, Pay 2 life named Pay 2 life
timing=fmain
oracle=Haste
