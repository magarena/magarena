name=Faultgrinder
image=https://cards.scryfall.io/border_crop/front/b/6/b66ca1a0-d638-48df-8474-143228c23382.jpg?1592713546
value=1.938
rarity=C
type=Creature
subtype=Elemental
cost={6}{R}
pt=4/4
ability=Trample;\
        When SN enters the battlefield, destroy target land.;\
        Evoke {4}{R}
timing=main
oracle=Trample\nWhen Faultgrinder enters the battlefield, destroy target land.\nEvoke {4}{R}
