name=Final Strike
image=https://cards.scryfall.io/border_crop/front/e/c/ecdfcb03-2f77-4f54-af62-3012cd3efd4f.jpg?1562448327
value=1.707
rarity=R
type=Sorcery
cost={2}{B}{B}
ability=As an additional cost to cast SN, sacrifice a creature
timing=main
requires_groovy_code
oracle=As an additional cost to cast Final Strike, sacrifice a creature.\nFinal Strike deals damage to target opponent equal to the sacrificed creature's power.
