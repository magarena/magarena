name=Feral Ridgewolf
image=https://cards.scryfall.io/border_crop/front/7/8/78c66cc0-cb0f-4daf-8141-0923ad46a834.jpg?1562832487
value=2.947
rarity=C
type=Creature
subtype=Wolf
cost={2}{R}
pt=1/2
ability=Trample;\
        {1}{R}: SN gets +2/+0 until end of turn.
timing=main
oracle=Trample\n{1}{R}: Feral Ridgewolf gets +2/+0 until end of turn.
