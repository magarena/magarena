name=Loxodon Peacekeeper
image=https://cards.scryfall.io/border_crop/front/9/c/9c9e7029-bea3-4a33-bd05-774e802616d4.jpg?1562152208
value=2.333
rarity=R
type=Creature
subtype=Elephant,Soldier
cost={1}{W}
pt=4/4
timing=main
requires_groovy_code
oracle=At the beginning of your upkeep, the player with the lowest life total gains control of Loxodon Peacekeeper. If two or more players are tied for lowest life total, you choose one of them, and that player gains control of Loxodon Peacekeeper.
