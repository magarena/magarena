name=Treasure Map
image=https://cards.scryfall.io/border_crop/front/c/0/c0f9c733-0818-4a03-8f0c-a163d09e0fff.jpg?1562563368
value=2.500
rarity=R
type=Artifact
cost={2}
transform=Treasure Cove
timing=artifact
requires_groovy_code
oracle={1}, {T}: Scry 1. Put a landmark counter on Treasure Map. Then if there are three or more landmark counters on it, remove those counters, transform Treasure Map, and create three colorless Treasure artifact tokens with "{T}, Sacrifice this artifact: Add one mana of any color."
