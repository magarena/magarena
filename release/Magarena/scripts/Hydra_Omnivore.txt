name=Hydra Omnivore
image=https://cards.scryfall.io/border_crop/front/1/5/15f046d7-092c-4cac-8f9f-b284457620b8.jpg?1592710941
value=4.083
rarity=M
type=Creature
subtype=Hydra
cost={4}{G}{G}
pt=8/8
timing=main
oracle=Whenever Hydra Omnivore deals combat damage to an opponent, it deals that much damage to each other opponent.
