name=Pierce the Sky
image=https://cards.scryfall.io/border_crop/front/d/f/df491512-ba8a-4ba5-ad42-338190201170.jpg?1562744164
value=2.500
rarity=C
type=Instant
cost={1}{G}
effect=SN deals 7 damage to target creature with flying.
timing=removal
oracle=Pierce the Sky deals 7 damage to target creature with flying.
