name=Sparksmith
image=https://cards.scryfall.io/border_crop/front/1/5/15a4460d-3fe8-4b1f-9990-0a19c3345367.jpg?1562900172
value=3.716
rarity=C
type=Creature
subtype=Goblin
cost={1}{R}
pt=1/1
timing=main
requires_groovy_code
oracle={T}: Sparksmith deals X damage to target creature and X damage to you, where X is the number of Goblins on the battlefield.
