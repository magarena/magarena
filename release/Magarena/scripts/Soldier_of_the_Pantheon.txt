name=Soldier of the Pantheon
image=https://cards.scryfall.io/border_crop/front/5/c/5cfc0f46-4df4-4ae3-bfe6-391916eb590f.jpg?1562818721
value=3.743
rarity=R
type=Creature
subtype=Human,Soldier
cost={W}
pt=2/1
ability=Protection from multicolored;\
        Whenever an opponent casts a multicolored spell, you gain 1 life.
timing=main
oracle=Protection from multicolored\nWhenever an opponent casts a multicolored spell, you gain 1 life.
