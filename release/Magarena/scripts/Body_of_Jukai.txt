name=Body of Jukai
image=https://cards.scryfall.io/border_crop/front/f/4/f43fe8e0-23ab-4fe7-8a48-1902be142c6a.jpg?1562880691
value=2.615
rarity=U
type=Creature
subtype=Spirit
cost={7}{G}{G}
pt=8/5
ability=Trample;\
        Soulshift 8
timing=main
oracle=Trample\nSoulshift 8
