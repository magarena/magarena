name=Ring of Evos Isle
image=https://cards.scryfall.io/border_crop/front/a/7/a7c740a8-1bbc-4ec8-a72c-01aee9e48f3d.jpg?1562558282
value=3.689
rarity=U
type=Artifact
subtype=Equipment
cost={2}
ability={2}: Equipped creature gains hexproof until end of turn. Activate this ability only if SN is being equipped.;\
        Equip {1}
timing=equipment
requires_groovy_code
oracle={2}: Equipped creature gains hexproof until end of turn.\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it's blue.\nEquip {1}
