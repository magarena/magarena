name=Equestrian Skill
image=https://cards.scryfall.io/border_crop/front/f/9/f9af14d1-a304-4744-9d6f-9ff5fcd92ad3.jpg?1576385069
value=2.500
rarity=C
type=Enchantment
subtype=Aura
cost={3}{G}
ability=Enchant creature;\
        Enchanted creature gets +3/+3.;\
        As long as enchanted creature is a Human, enchanted creature has trample.
timing=aura
enchant=pump,creature
oracle=Enchant creature\nEnchanted creature gets +3/+3.\nAs long as enchanted creature is a Human, it has trample.
