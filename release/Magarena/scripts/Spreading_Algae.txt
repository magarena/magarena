name=Spreading Algae
image=https://cards.scryfall.io/border_crop/front/3/a/3abaf631-ce63-47c8-8883-04f942f2e7ba.jpg?1562906888
value=3.500
rarity=U
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant Swamp;\
        When enchanted land becomes tapped, destroy RN.;\
        When SN is put into a graveyard from the battlefield, return SN from the graveyard to its owner's hand.
timing=aura
enchant=destroy,neg swamp
oracle=Enchant Swamp\nWhen enchanted land becomes tapped, destroy it.\nWhen Spreading Algae is put into a graveyard from the battlefield, return Spreading Algae to its owner's hand.
