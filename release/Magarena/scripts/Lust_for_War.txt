name=Lust for War
image=https://cards.scryfall.io/border_crop/front/9/c/9c8fa150-383d-448c-8e14-f24f65e13770.jpg?1562706764
value=3.708
rarity=U
type=Enchantment
subtype=Aura
cost={2}{R}
ability=Enchant creature;\
        Whenever enchanted creature becomes tapped, SN deals 3 damage to that creature's controller.;\
        Enchanted creature attacks each combat if able.
timing=aura
enchant=must attack,neg creature
oracle=Enchant creature\nWhenever enchanted creature becomes tapped, Lust for War deals 3 damage to that creature's controller.\nEnchanted creature attacks each combat if able.
