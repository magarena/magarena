name=Ooze Garden
image=https://cards.scryfall.io/border_crop/front/6/2/623e57ed-7ed7-4283-b48f-77ecacad70b0.jpg?1562704660
value=2.971
rarity=R
type=Enchantment
cost={1}{G}
timing=enchantment
requires_groovy_code
oracle={1}{G}, Sacrifice a non-Ooze creature: Create an X/X green Ooze creature token, where X is the sacrificed creature's power. Activate this ability only any time you could cast a sorcery.
