name=Cloudblazer
image=https://cards.scryfall.io/border_crop/front/2/f/2f64ae7d-0bd0-41f8-8f7d-64db4bd2a10b.jpg?1631588432
image_updated=2018-04-27
value=2.500
rarity=U
type=Creature
subtype=Human,Scout
cost={3}{W}{U}
pt=2/2
ability=Flying;\
        When SN enters the battlefield, you gain 2 life~and draw two cards.
timing=main
oracle=Flying\nWhen Cloudblazer enters the battlefield, you gain 2 life and draw two cards.
