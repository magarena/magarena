name=1/1 blue Thopter artifact creature token with flying
token=Thopter
image=https://cards.scryfall.io/border_crop/front/b/c/bc3b9e96-e29b-4fca-a9eb-0b89e6d80cae.jpg?1682210956
value=1
type=Artifact,Creature
subtype=Thopter
color=u
pt=1/1
ability=Flying
oracle=Flying
