name=Golgari Keyrune
image=https://cards.scryfall.io/border_crop/front/9/1/913b803f-ba82-4660-86be-49677d1e32c9.jpg?1562789942
value=3.271
rarity=U
type=Artifact
cost={3}
ability={T}: Add {B} or {G}.;\
        {B}{G}: SN becomes a 2/2 black and green Insect artifact creature with deathtouch until end of turn.
timing=artifact
mana_or_combat
oracle={T}: Add {B} or {G}.\n{B}{G}: Golgari Keyrune becomes a 2/2 black and green Insect artifact creature with deathtouch until end of turn.
