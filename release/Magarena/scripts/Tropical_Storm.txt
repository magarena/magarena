name=Tropical Storm
image=https://cards.scryfall.io/border_crop/front/c/d/cd5f473c-e11e-4047-91f9-81b80f0a3562.jpg?1587912688
value=3.442
rarity=U
type=Sorcery
cost={X}{G}
effect=SN deals X damage to each creature with flying and 1 additional damage to each blue creature.
timing=main
oracle=Tropical Storm deals X damage to each creature with flying and 1 additional damage to each blue creature.
