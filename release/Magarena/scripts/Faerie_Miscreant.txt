name=Faerie Miscreant
image=https://cards.scryfall.io/border_crop/front/2/8/28aa0337-94bd-4274-b2ee-6f43747c77b3.jpg?1592516372
image_updated=2015-11-05
value=2.500
rarity=C
type=Creature
subtype=Faerie,Rogue
cost={U}
pt=1/1
ability=Flying;\
        When SN enters the battlefield, if you control another creature named Faerie Miscreant, draw a card.
timing=main
oracle=Flying\nWhen Faerie Miscreant enters the battlefield, if you control another creature named Faerie Miscreant, draw a card.
