name=Clockwork Hydra
image=https://cards.scryfall.io/border_crop/front/b/a/babba9f7-86e4-4e11-9fb8-acd50fbd8031.jpg?1619404199
image_updated=2016-11-28
value=3.539
rarity=U
type=Artifact,Creature
subtype=Hydra
cost={5}
pt=0/0
ability=SN enters the battlefield with four +1/+1 counters on it.;\
        Whenever SN attacks or blocks, remove a +1/+1 counter from SN. If you do, SN deals 1 damage to target creature or player.;\
        {T}: Put a +1/+1 counter on SN.
timing=main
oracle=Clockwork Hydra enters the battlefield with four +1/+1 counters on it.\nWhenever Clockwork Hydra attacks or blocks, remove a +1/+1 counter from it. If you do, Clockwork Hydra deals 1 damage to target creature or player.\n{T}: Put a +1/+1 counter on Clockwork Hydra.
