name=Branded Brawlers
image=https://cards.scryfall.io/border_crop/front/9/0/90a48065-fbf1-4f2a-993e-7061057a4c45.jpg?1562921861
value=1.605
rarity=C
type=Creature
subtype=Human,Soldier
cost={R}
pt=2/2
timing=main
requires_groovy_code
oracle=Branded Brawlers can't attack if defending player controls an untapped land.\nBranded Brawlers can't block if you control an untapped land.
