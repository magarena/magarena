name=Skyfire Kirin
image=https://cards.scryfall.io/border_crop/front/2/d/2d425562-3f0f-4aa3-a761-48ba445b8380.jpg?1562493019
value=3.465
rarity=R
type=Legendary,Creature
subtype=Kirin,Spirit
cost={2}{R}{R}
pt=3/3
ability=Flying
timing=main
requires_groovy_code
oracle=Flying\nWhenever you cast a Spirit or Arcane spell, you may gain control of target creature with that spell's converted mana cost until end of turn.
