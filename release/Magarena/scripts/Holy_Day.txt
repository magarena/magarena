name=Holy Day
image=https://cards.scryfall.io/border_crop/front/f/3/f3895c0c-0db8-4962-b386-57887fb60701.jpg?1562557717
value=4.133
rarity=C
type=Instant
cost={W}
effect=Prevent all combat damage that would be dealt this turn.
timing=block
oracle=Prevent all combat damage that would be dealt this turn.
