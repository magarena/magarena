name=Infectious Host
image=https://cards.scryfall.io/border_crop/front/1/1/11858b4f-3a90-4990-9984-d888c000ec73.jpg?1598914916
value=1.738
rarity=C
type=Creature
subtype=Zombie
cost={2}{B}
pt=1/1
ability=When SN dies, target player loses 2 life.
timing=main
oracle=When Infectious Host dies, target player loses 2 life.
