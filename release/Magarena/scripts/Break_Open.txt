name=Break Open
image=https://cards.scryfall.io/border_crop/front/a/5/a5ae8050-b644-41db-b1e9-d9bad2173485.jpg?1562934178
value=0.864
rarity=C
type=Instant
cost={1}{R}
effect=Turn target face-down creature an opponent controls face up.
timing=removal
oracle=Turn target face-down creature an opponent controls face up.
