name=Nihil Spellbomb
image=https://cards.scryfall.io/border_crop/front/e/5/e5892a23-efae-4731-9b8f-41c87960fe93.jpg?1562442193
image_updated=2018-04-27
value=4.483
rarity=C
type=Artifact
cost={1}
ability={T}, Sacrifice SN: Exile all cards from target player's graveyard.;\
        When SN is put into a graveyard from the battlefield, you may pay {B}. If you do, draw a card.
timing=artifact
oracle={T}, Sacrifice Nihil Spellbomb: Exile all cards from target player's graveyard.\nWhen Nihil Spellbomb is put into a graveyard from the battlefield, you may pay {B}. If you do, draw a card.
