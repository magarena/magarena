name=Echoing Ruin
image=https://cards.scryfall.io/border_crop/front/0/1/01f842c8-cd6c-4f4d-9aa8-417a92b37867.jpg?1562635146
value=3.776
removal=3
rarity=C
type=Sorcery
cost={1}{R}
timing=removal
requires_groovy_code
oracle=Destroy target artifact and all other artifacts with the same name as that artifact.
