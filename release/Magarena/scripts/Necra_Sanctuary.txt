name=Necra Sanctuary
image=https://cards.scryfall.io/border_crop/front/5/a/5a0bf165-d7eb-4ae6-b30a-4e9fd55f401d.jpg?1562916168
value=3.474
rarity=U
type=Enchantment
cost={2}{B}
timing=enchantment
requires_groovy_code
oracle=At the beginning of your upkeep, if you control a green or white permanent, target player loses 1 life. If you control a green permanent and a white permanent, that player loses 3 life instead.
