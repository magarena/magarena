name=Luminous Angel
image=https://cards.scryfall.io/border_crop/front/c/3/c3ac419f-7630-48c7-9039-88ce28898b6d.jpg?1561771537
image_updated=2016-12-21
value=3.373
rarity=R
type=Creature
subtype=Angel
cost={4}{W}{W}{W}
pt=4/4
ability=Flying;\
        At the beginning of your upkeep, you may create a 1/1 white Spirit creature token with flying.
timing=main
oracle=Flying\nAt the beginning of your upkeep, you may create a 1/1 white Spirit creature token with flying.
