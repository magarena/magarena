name=Hazardous Conditions
image=https://cards.scryfall.io/border_crop/front/d/a/daa9b08b-c56f-480e-874e-069e72d979c8.jpg?1576382835
image_updated=2016-10-05
value=2.500
rarity=U
type=Sorcery
cost={2}{B}{G}
timing=main
requires_groovy_code
oracle=Creatures with no counters on them get -2/-2 until end of turn.
