name=Lurking Skirge
image=https://cards.scryfall.io/border_crop/front/9/0/9063cc12-a822-4488-856e-93d70ecfe37f.jpg?1562863741
value=3.853
rarity=R
type=Enchantment
cost={1}{B}
timing=enchantment
requires_groovy_code
oracle=When a creature is put into an opponent's graveyard from the battlefield, if Lurking Skirge is an enchantment, Lurking Skirge becomes a 3/2 Imp creature with flying.
