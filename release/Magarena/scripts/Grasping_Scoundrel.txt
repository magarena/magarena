name=Grasping Scoundrel
image=https://cards.scryfall.io/border_crop/front/9/f/9fd574a9-2034-402c-9fd6-83c4b9db83d1.jpg?1562303415
value=2.500
rarity=C
type=Creature
subtype=Human,Pirate
cost={B}
pt=1/1
ability=SN gets +1/+0 as long as it's attacking.
timing=main
oracle=Grasping Scoundrel gets +1/+0 as long as it's attacking.
