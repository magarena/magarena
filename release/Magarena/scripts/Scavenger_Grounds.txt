name=Scavenger Grounds
image=https://cards.scryfall.io/border_crop/front/7/8/78748ac8-4726-4156-a97e-023f80fef610.jpg?1625980919
value=2.500
rarity=R
type=Land
subtype=Desert
ability={T}: Add {C}.;\
        {2}, {T}, Sacrifice a Desert: Exile all cards from all graveyards.
timing=land
oracle={T}: Add {C}.\n{2}, {T}, Sacrifice a Desert: Exile all cards from all graveyards.
