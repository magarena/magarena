name=Windbrisk Raptor
image=https://cards.scryfall.io/border_crop/front/5/d/5df7b710-d44c-4ebc-be5b-f81d697086c4.jpg?1562830667
value=3.795
rarity=R
type=Creature
subtype=Bird
cost={5}{W}{W}
pt=5/7
ability=Flying;\
        Attacking creatures you control have lifelink.
timing=main
oracle=Flying\nAttacking creatures you control have lifelink.
