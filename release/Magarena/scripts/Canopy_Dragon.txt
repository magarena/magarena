name=Canopy Dragon
image=https://cards.scryfall.io/border_crop/front/2/0/20c5e4d6-b716-4994-b226-b1eb799bec25.jpg?1562718315
value=3.148
rarity=R
type=Creature
subtype=Dragon
cost={4}{G}{G}
pt=4/4
ability=Trample;\
        {1}{G}: SN gains flying until end of turn.~SN loses trample until end of turn.
timing=main
oracle=Trample\n{1}{G}: Canopy Dragon gains flying and loses trample until end of turn.
