name=Skyline Despot
image=https://cards.scryfall.io/border_crop/front/f/a/fabdbd03-6f1e-4ada-ad90-5c72d7e305b6.jpg?1631587493
image_updated=2016-09-05
value=2.500
rarity=R
type=Creature
subtype=Dragon
cost={5}{R}{R}
pt=5/5
ability=Flying;\
        When SN enters the battlefield, you become the monarch.;\
        At the beginning of your upkeep, if you're the monarch, create a 5/5 red Dragon creature token with flying.
timing=main
oracle=Flying\nWhen Skyline Despot enters the battlefield, you become the monarch.\nAt the beginning of your upkeep, if you're the monarch, create a 5/5 red Dragon creature token with flying.
