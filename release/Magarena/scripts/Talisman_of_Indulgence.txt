name=Talisman of Indulgence
image=https://cards.scryfall.io/border_crop/front/f/a/fa6c62c7-8fd4-46f1-a7f4-fc6e74d34b35.jpg?1631589255
value=3.019
rarity=U
type=Artifact
cost={2}
ability={T}: Add {C}.;\
        {T}: Add {B} or {R}. SN deals 1 damage to you.
timing=artifact
oracle={T}: Add {C}.\n{T}: Add {B} or {R}. Talisman of Indulgence deals 1 damage to you.
