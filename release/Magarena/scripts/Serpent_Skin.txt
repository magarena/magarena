name=Serpent Skin
image=https://cards.scryfall.io/border_crop/front/8/c/8c5722d9-d1a4-4ad2-bf85-db666d4a30d9.jpg?1562762476
value=3.014
rarity=C
type=Enchantment
subtype=Aura
cost={2}{G}
ability=Flash;\
        Enchant creature;\
        Enchanted creature gets +1/+1.;\
        {G}: Regenerate enchanted creature.
timing=pumpFlash
enchant=pump,pos creature
oracle=Flash\nEnchant creature\nEnchanted creature gets +1/+1.\n{G}: Regenerate enchanted creature.
