name=Echoing Courage
image=https://cards.scryfall.io/border_crop/front/0/4/04367e72-3f8c-4cba-8bdf-5a1ba9c9f01f.jpg?1562432457
image_updated=2018-04-27
value=3.667
rarity=C
type=Instant
cost={1}{G}
timing=pump
requires_groovy_code
oracle=Target creature and all other creatures with the same name as that creature get +2/+2 until end of turn.
