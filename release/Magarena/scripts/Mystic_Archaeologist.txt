name=Mystic Archaeologist
image=https://cards.scryfall.io/border_crop/front/d/e/de19d2db-604b-4d5f-8184-9bb0a31c7405.jpg?1600698074
value=2.500
rarity=R
type=Creature
subtype=Human,Wizard
cost={1}{U}
pt=2/1
ability={3}{U}{U}: Draw two cards.
timing=main
oracle={3}{U}{U}: Draw two cards.
