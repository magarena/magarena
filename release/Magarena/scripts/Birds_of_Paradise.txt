name=Birds of Paradise
image=https://cards.scryfall.io/border_crop/front/f/e/feefe9f0-24a6-461c-9ef1-86c5a6f33b83.jpg?1594681430
image_updated=2017-08-29
value=4.424
rarity=R
type=Creature
subtype=Bird
cost={G}
pt=0/1
ability=Flying;\
        {T}: Add one mana of any color.
timing=main
oracle=Flying\n{T}: Add one mana of any color.
