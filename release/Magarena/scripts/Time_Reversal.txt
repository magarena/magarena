name=Time Reversal
image=https://cards.scryfall.io/border_crop/front/2/d/2d6500a1-5aea-4b83-b4dc-560fe547590d.jpg?1562637460
value=3.186
rarity=M
type=Sorcery
cost={3}{U}{U}
timing=main
requires_groovy_code
oracle=Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. Exile Time Reversal.
