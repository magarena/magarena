name=Minister of Inquiries
image=https://cards.scryfall.io/border_crop/front/0/e/0e6c31d1-6e2a-41b3-aa4f-d30a7cd997d2.jpg?1576381327
image_updated=2016-10-05
value=2.500
rarity=U
type=Creature
subtype=Vedalken,Advisor
cost={U}
pt=1/2
ability=When SN enters the battlefield, you get {E}{E}.;\
        {T}, Pay {E}: Target player puts the top three cards of his or her library into his or her graveyard.
timing=main
oracle=When Minister of Inquiries enters the battlefield, you get {E}{E}.\n{T}, Pay {E}: Target player puts the top three cards of his or her library into his or her graveyard.
