name=Sunseed Nurturer
image=https://cards.scryfall.io/border_crop/front/9/d/9d44447e-09d1-450f-907f-42f79b004fe7.jpg?1562707122
value=2.133
rarity=U
type=Creature
subtype=Human,Druid,Wizard
cost={2}{W}
pt=1/1
ability=At the beginning of your end step, if you control a creature with power 5 or greater, you may gain 2 life.;\
        {T}: Add {C}.
timing=main
oracle=At the beginning of your end step, if you control a creature with power 5 or greater, you may gain 2 life.\n{T}: Add {C}.
