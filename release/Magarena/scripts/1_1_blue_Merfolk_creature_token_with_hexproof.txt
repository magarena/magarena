name=1/1 blue Merfolk creature token with hexproof
token=Merfolk
image=https://api.scryfall.com/cards/txln/3/en?format=image&version=border_crop
value=1
pt=1/1
color=u
subtype=Merfolk
type=Creature
ability=Hexproof
oracle=Hexproof
