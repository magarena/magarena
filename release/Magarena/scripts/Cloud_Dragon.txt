name=Cloud Dragon
image=https://cards.scryfall.io/border_crop/front/6/9/69aec764-7832-4d9c-b6c1-fdf5b27fedc0.jpg?1562919865
value=2.820
rarity=R
type=Creature
subtype=Illusion,Dragon
cost={5}{U}
pt=5/4
ability=Flying;\
        SN can block only creatures with flying.
timing=main
oracle=Flying\nCloud Dragon can block only creatures with flying.
