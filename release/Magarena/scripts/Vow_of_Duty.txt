name=Vow of Duty
image=https://cards.scryfall.io/border_crop/front/4/4/44829e30-b51c-4cd3-8598-3e2c1e06f334.jpg?1625975835
image_updated=2016-02-29
value=3.500
rarity=U
type=Enchantment
subtype=Aura
cost={2}{W}
ability=Enchant creature;\
        Enchanted creature gets +2/+2 and has vigilance.;\
        As long as it's not your turn, enchanted creature can't attack.
timing=aura
enchant=default,creature
oracle=Enchant creature\nEnchanted creature gets +2/+2, has vigilance, and can't attack you or a planeswalker you control.
