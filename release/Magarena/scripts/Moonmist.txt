name=Moonmist
image=https://cards.scryfall.io/border_crop/front/5/7/57153c3f-9e55-418c-b67b-36901f29f9c1.jpg?1562830528
value=3.880
rarity=C
type=Instant
cost={1}{G}
timing=block
oracle=Transform all Humans. Prevent all combat damage that would be dealt this turn by creatures other than Werewolves and Wolves.
requires_groovy_code
