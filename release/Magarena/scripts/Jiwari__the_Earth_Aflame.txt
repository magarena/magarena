name=Jiwari, the Earth Aflame
image=https://cards.scryfall.io/border_crop/front/6/b/6b46105b-32c2-48fc-b71d-c3e8fcb8a8ea.jpg?1562494205
value=3.355
rarity=R
type=Legendary,Creature
subtype=Spirit
cost={3}{R}{R}
pt=3/3
ability={X}{R}, {T}: SN deals X damage to target creature without flying.;\
        {X}{R}{R}{R}, Discard SN: SN deals X damage to each creature without flying.
timing=main
oracle={X}{R}, {T}: Jiwari, the Earth Aflame deals X damage to target creature without flying.\nChannel — {X}{R}{R}{R}, Discard Jiwari: Jiwari deals X damage to each creature without flying.
