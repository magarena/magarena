name=Yavimaya Gnats
image=https://cards.scryfall.io/border_crop/front/9/d/9d8b7020-ca8f-4867-bc51-13d824daf154.jpg?1587912092
value=2.786
rarity=U
type=Creature
subtype=Insect
cost={2}{G}
pt=0/1
ability=Flying;\
        {G}: Regenerate SN.
timing=main
oracle=Flying\n{G}: Regenerate Yavimaya Gnats.
