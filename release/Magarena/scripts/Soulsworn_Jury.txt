name=Soulsworn Jury
image=https://cards.scryfall.io/border_crop/front/6/6/668c4e40-0035-44e0-8401-dd17fa11ecd4.jpg?1593272567
value=3.510
rarity=C
type=Creature
subtype=Spirit
cost={2}{W}
pt=1/4
ability=Defender;\
        {1}{U}, Sacrifice SN: Counter target creature spell.
timing=smain
oracle=Defender\n{1}{U}, Sacrifice Soulsworn Jury: Counter target creature spell.
