name=Voracious Hatchling
image=https://cards.scryfall.io/border_crop/front/b/6/b66b1a84-ab15-48d8-909c-08116dc71c76.jpg?1562933514
value=3.805
rarity=U
type=Creature
subtype=Elemental
cost={3}{W/B}
pt=6/6
ability=Lifelink;\
        SN enters the battlefield with four -1/-1 counters on it.;\
        Whenever you cast a white spell, remove a -1/-1 counter from SN.;\
        Whenever you cast a black spell, remove a -1/-1 counter from SN.
timing=main
oracle=Lifelink\nVoracious Hatchling enters the battlefield with four -1/-1 counters on it.\nWhenever you cast a white spell, remove a -1/-1 counter from Voracious Hatchling.\nWhenever you cast a black spell, remove a -1/-1 counter from Voracious Hatchling.
