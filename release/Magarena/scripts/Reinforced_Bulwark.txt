name=Reinforced Bulwark
image=https://cards.scryfall.io/border_crop/front/4/3/437a91e7-f98e-4ed8-9ab7-f4db62979f5b.jpg?1562703233
value=2.372
rarity=C
type=Artifact,Creature
subtype=Wall
cost={3}
pt=0/4
ability=Defender;\
        {T}: Prevent the next 1 damage that would be dealt to you this turn.
timing=smain
oracle=Defender\n{T}: Prevent the next 1 damage that would be dealt to you this turn.
