name=Scavenging Ooze
image=https://cards.scryfall.io/border_crop/front/6/7/67d93e17-13fd-4cf5-a53c-a7b6c57a8351.jpg?1673484773
image_updated=2017-08-08
value=4.552
rarity=R
type=Creature
subtype=Ooze
cost={1}{G}
pt=2/2
timing=main
requires_groovy_code
oracle={G}: Exile target card from a graveyard. If it was a creature card, put a +1/+1 counter on Scavenging Ooze and you gain 1 life.
