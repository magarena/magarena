name=Wall of Shards
image=https://cards.scryfall.io/border_crop/front/8/8/884ee8d8-4c0d-4e44-8321-bccd18195693.jpg?1593274857
value=3.500
rarity=U
type=Snow,Creature
subtype=Wall
cost={1}{W}
pt=1/8
ability=Defender, flying
timing=smain
oracle=Defender, flying\nCumulative upkeep—An opponent gains 1 life.
requires_groovy_code
