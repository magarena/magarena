name=Phantom Flock
image=https://cards.scryfall.io/border_crop/front/6/1/617d4246-c827-4742-ab6a-c5170c12cb87.jpg?1562630147
value=2.981
rarity=U
type=Creature
subtype=Bird,Soldier,Spirit
cost={3}{W}{W}
pt=0/0
ability=Flying;\
        SN enters the battlefield with three +1/+1 counters on it.
timing=main
requires_groovy_code=Phantom Nomad
oracle=Flying\nPhantom Flock enters the battlefield with three +1/+1 counters on it.\nIf damage would be dealt to Phantom Flock, prevent that damage. Remove a +1/+1 counter from Phantom Flock.
