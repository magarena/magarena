name=Wingcrafter
image=https://cards.scryfall.io/border_crop/front/0/b/0b79a71b-568f-4700-9b1d-e7965159da0a.jpg?1593813198
image_updated=2017-08-08
value=3.858
rarity=C
type=Creature
subtype=Human,Wizard
cost={U}
pt=1/1
ability=Soulbond;\
        As long as SN is paired with another creature, both creatures have flying.
timing=main
oracle=Soulbond\nAs long as Wingcrafter is paired with another creature, both creatures have flying.
