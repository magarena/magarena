name=Whip Silk
image=https://cards.scryfall.io/border_crop/front/1/0/10566804-fd15-4ef0-ad7d-cc979f4cc8c5.jpg?1562898296
value=3.404
rarity=C
type=Enchantment
subtype=Aura
cost={G}
ability=Enchant creature;\
        Enchanted creature has reach.;\
        {G}: Return SN to its owner's hand.
timing=aura
enchant=flying,pos creature
oracle=Enchant creature\nEnchanted creature has reach.\n{G}: Return Whip Silk to its owner's hand.
