name=Eternal Dragon
image=https://cards.scryfall.io/border_crop/front/0/f/0f57fc0c-cab1-4eb0-a58a-0c7a762f1572.jpg?1631585882
value=4.000
rarity=R
type=Creature
subtype=Dragon,Spirit
cost={5}{W}{W}
pt=5/5
ability=Flying;\
        {3}{W}{W}: Return SN from your graveyard to your hand. Activate this ability only during your upkeep.;\
        Plainscycling {2}
timing=main
oracle=Flying\n{3}{W}{W}: Return Eternal Dragon from your graveyard to your hand. Activate this ability only during your upkeep.\nPlainscycling {2}
