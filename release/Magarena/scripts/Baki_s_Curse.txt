name=Baki's Curse
image=https://cards.scryfall.io/border_crop/front/e/3/e3261b4c-7963-4ca0-875d-77b7c8571b3f.jpg?1562588703
value=1.830
rarity=R
type=Sorcery
cost={2}{U}{U}
ability=Cast SN with AI only if an opponent controls a creature that is enchanted.
timing=main
requires_groovy_code
oracle=Baki's Curse deals 2 damage to each creature for each Aura attached to that creature.
