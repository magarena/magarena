name=Clutch of Undeath
image=https://cards.scryfall.io/border_crop/front/7/3/7301fdec-ca17-47ae-9a0a-84ea8665ece1.jpg?1562530624
value=2.013
rarity=C
type=Enchantment
subtype=Aura
cost={3}{B}{B}
ability=Enchant creature
timing=aura
#Could easily be enchant=weaken -3/-3,neg creature
enchant=pump,creature
oracle=Enchant creature\nEnchanted creature gets +3/+3 as long as it's a Zombie. Otherwise, it gets -3/-3.
requires_groovy_code
