name=Argothian Pixies
image=https://cards.scryfall.io/border_crop/front/6/d/6d672009-a442-495a-b2aa-b25b92db7030.jpg?1562920760
value=3.136
rarity=C
type=Creature
subtype=Faerie
cost={1}{G}
pt=2/1
ability=SN can't be blocked by artifact creatures.
timing=main
requires_groovy_code
oracle=Argothian Pixies can't be blocked by artifact creatures.\nPrevent all damage that would be dealt to Argothian Pixies by artifact creatures.
