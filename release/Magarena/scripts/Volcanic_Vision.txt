name=Volcanic Vision
image=https://cards.scryfall.io/border_crop/front/f/9/f973e1a6-c6f9-47f5-9bf0-b7fa06959bd4.jpg?1625194143
image_updated=2017-08-17
value=2.500
rarity=R
type=Sorcery
cost={5}{R}{R}
timing=main
requires_groovy_code
oracle=Return target instant or sorcery card from your graveyard to your hand. Volcanic Vision deals damage equal to that card's converted mana cost to each creature your opponents control. Exile Volcanic Vision.
