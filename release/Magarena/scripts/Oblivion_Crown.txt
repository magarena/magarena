name=Oblivion Crown
image=https://cards.scryfall.io/border_crop/front/0/c/0cd5486b-c6f4-4cfc-9590-8ffc78b48b0a.jpg?1562897621
value=2.943
rarity=C
type=Enchantment
subtype=Aura
cost={1}{B}
ability=Flash;\
        Enchant creature;\
        Enchanted creature has "Discard a card: This creature gets +1/+1 until end of turn."
enchant=pump,pos creature
timing=pumpFlash
oracle=Flash\nEnchant creature\nEnchanted creature has "Discard a card: This creature gets +1/+1 until end of turn."
