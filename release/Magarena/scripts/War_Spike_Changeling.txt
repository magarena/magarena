name=War-Spike Changeling
image=https://cards.scryfall.io/border_crop/front/5/2/523500d3-5614-4be8-95c7-d63ca279c1b1.jpg?1561967342
value=2.767
rarity=C
type=Creature
subtype=Shapeshifter
cost={3}{R}
pt=3/3
ability=Changeling;\
        {R}: SN gains first strike until end of turn.
timing=main
oracle=Changeling\n{R}: War-Spike Changeling gains first strike until end of turn.
