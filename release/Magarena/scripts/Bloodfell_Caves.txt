name=Bloodfell Caves
image=https://cards.scryfall.io/border_crop/front/8/5/85930f68-6f53-4921-9556-2887ac3abfd2.jpg?1682205829
value=3.333
rarity=C
type=Land
mana=b3r3
ability=SN enters the battlefield tapped.;\
        When SN enters the battlefield, you gain 1 life.;\
        {T}: Add {B} or {R}.
timing=tapland
oracle=Bloodfell Caves enters the battlefield tapped.\nWhen Bloodfell Caves enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R}.
