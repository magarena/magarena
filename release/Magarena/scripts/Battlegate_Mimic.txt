name=Battlegate Mimic
image=https://cards.scryfall.io/border_crop/front/c/3/c330f44c-0479-472d-81ed-16445d182c37.jpg?1562842884
image_updated=2016-12-13
value=3.559
rarity=C
type=Creature
subtype=Shapeshifter
cost={1}{R/W}
pt=2/1
timing=main
oracle=Whenever you cast a spell that's both red and white, Battlegate Mimic has base power and toughness 4/2 until end of turn and gains first strike until end of turn.
requires_groovy_code
