name=Shield Sphere
image=https://cards.scryfall.io/border_crop/front/7/a/7a76a97e-a857-4593-ae7c-e393d8a868ac.jpg?1559592340
value=4.380
rarity=U
type=Artifact,Creature
subtype=Wall
cost={0}
pt=0/6
ability=Defender;\
        Whenever SN blocks, put a -0/-1 counter on SN.
timing=smain
oracle=Defender\nWhenever Shield Sphere blocks, put a -0/-1 counter on it.
