name=Sharding Sphinx
image=https://cards.scryfall.io/border_crop/front/1/d/1d93d905-cc36-4a78-b07c-07f8bbf39a47.jpg?1682208884
value=3.643
rarity=R
type=Artifact,Creature
subtype=Sphinx
cost={4}{U}{U}
pt=4/4
ability=Flying;\
        Whenever an artifact creature you control deals combat damage to a player, you may create a 1/1 blue Thopter artifact creature token with flying.
timing=main
oracle=Flying\nWhenever an artifact creature you control deals combat damage to a player, you may create a 1/1 blue Thopter artifact creature token with flying.
