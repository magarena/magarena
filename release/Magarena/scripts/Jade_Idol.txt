name=Jade Idol
image=https://cards.scryfall.io/border_crop/front/a/5/a58f63c5-cdc5-4079-a727-cff45668d546.jpg?1562763445
value=2.300
rarity=U
type=Artifact
cost={4}
ability=Whenever you cast a Spirit or Arcane spell, SN becomes a 4/4 Spirit artifact creature until end of turn.
timing=artifact
oracle=Whenever you cast a Spirit or Arcane spell, Jade Idol becomes a 4/4 Spirit artifact creature until end of turn.
