name=Squadron Hawk
image=https://cards.scryfall.io/border_crop/front/9/e/9e81806d-5d87-4032-ad94-c2cdeabecdbf.jpg?1562439058
image_updated=2018-04-27
value=4.191
rarity=C
type=Creature
subtype=Bird
cost={1}{W}
pt=1/1
ability=Flying;\
        When SN enters the battlefield, you may search your library for up to three cards named Squadron Hawk, reveal them, put them into your hand, then shuffle your library.
timing=main
oracle=Flying\nWhen Squadron Hawk enters the battlefield, you may search your library for up to three cards named Squadron Hawk, reveal them, put them into your hand, then shuffle your library.
