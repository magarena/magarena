name=Adriana, Captain of the Guard
image=https://cards.scryfall.io/border_crop/front/0/1/01c2337d-1c3c-4c5e-8b06-dc8a8c56164e.jpg?1673305427
image_updated=2016-09-05
value=2.500
rarity=R
type=Legendary,Creature
subtype=Human,Knight
cost={3}{R}{W}
pt=4/4
ability=Melee;\
        Other creatures you control have melee.
timing=main
oracle=Melee\nOther creatures you control have melee.
