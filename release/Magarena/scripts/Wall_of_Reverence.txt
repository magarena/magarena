name=Wall of Reverence
image=https://cards.scryfall.io/border_crop/front/1/5/1515e039-b838-40b6-b087-eb9e9d9ff008.jpg?1547516009
value=3.778
rarity=R
type=Creature
subtype=Spirit,Wall
cost={3}{W}
pt=1/6
ability=Defender, flying
timing=smain
requires_groovy_code
oracle=Defender, flying\nAt the beginning of your end step, you may gain life equal to the power of target creature you control.
