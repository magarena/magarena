name=Ravenous Harpy
image=https://cards.scryfall.io/border_crop/front/6/7/676ec702-75c4-4733-b500-eb15406778bb.jpg?1562302650
value=2.500
rarity=U
type=Creature
subtype=Harpy
cost={2}{B}
pt=1/2
ability=Flying;\
        {1}, Sacrifice another creature: Put a +1/+1 counter on SN.
timing=main
oracle=Flying\n{1}, Sacrifice another creature: Put a +1/+1 counter on Ravenous Harpy.
