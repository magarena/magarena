name=Soratami Mirror-Mage
image=https://cards.scryfall.io/border_crop/front/2/1/215209b1-1a5d-48f4-9eca-19a9848b2fed.jpg?1562758400
value=1.226
rarity=U
type=Creature
subtype=Moonfolk,Wizard
cost={3}{U}
pt=2/1
ability=Flying;\
        {3}, Return three lands you control to their owner's hand: Return target creature to its owner's hand.
timing=main
oracle=Flying\n{3}, Return three lands you control to their owner's hand: Return target creature to its owner's hand.
