name=Ballista Charger
image=https://cards.scryfall.io/border_crop/front/f/b/fbd3f376-1075-42c9-91df-80dc2d5faacf.jpg?1576382983
image_updated=2016-10-05
value=2.500
rarity=U
type=Artifact
subtype=Vehicle
cost={5}
pt=6/6
ability=Whenever SN attacks, SN deals 1 damage to target creature or player.;\
        Crew 3
timing=artifact
oracle=Whenever Ballista Charger attacks, it deals 1 damage to target creature or player.\nCrew 3
