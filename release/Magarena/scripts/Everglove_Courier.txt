name=Everglove Courier
image=https://cards.scryfall.io/border_crop/front/1/3/13bf5786-e41a-4839-b8a0-5c7a413b23d0.jpg?1562899727
value=2.788
rarity=U
type=Creature
subtype=Elf
cost={2}{G}
pt=2/1
ability=You may choose not to untap SN during your untap step.
requires_groovy_code
timing=main
oracle=You may choose not to untap Everglove Courier during your untap step.\n{2}{G}, {T}: Target Elf creature gets +2/+2 and has trample for as long as Everglove Courier remains tapped.
