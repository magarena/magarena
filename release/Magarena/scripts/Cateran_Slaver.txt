name=Cateran Slaver
image=https://cards.scryfall.io/border_crop/front/2/d/2d293c51-714c-45b8-bfa4-fe35e8f3fbc1.jpg?1562379906
value=3.530
rarity=R
type=Creature
subtype=Horror,Mercenary
cost={4}{B}{B}
pt=5/5
ability=Swampwalk;\
        {5}, {T}: Search your library for a Mercenary permanent card with converted mana cost 5 or less and put it onto the battlefield. Then shuffle your library.
timing=main
oracle=Swampwalk\n{5}, {T}: Search your library for a Mercenary permanent card with converted mana cost 5 or less and put it onto the battlefield. Then shuffle your library.
