name=Ghastly Demise
image=https://cards.scryfall.io/border_crop/front/7/f/7fcfe11a-fcc9-41e1-91ef-755bb4e22389.jpg?1608909764
value=4.069
rarity=C
type=Instant
cost={B}
timing=removal
requires_groovy_code
oracle=Destroy target nonblack creature if its toughness is less than or equal to the number of cards in your graveyard.
