name=Tana, the Bloodsower
image=https://cards.scryfall.io/border_crop/front/a/3/a3d8d64f-a403-42a7-881b-4f70e9fe15a2.jpg?1562413758
image_updated=2016-12-28
value=2.500
rarity=M
type=Legendary,Creature
subtype=Elf,Druid
cost={2}{R}{G}
pt=2/2
ability=Trample;\
        Partner
timing=main
oracle=Trample\nWhenever Tana, the Bloodsower deals combat damage to a player, create that many 1/1 green Saproling creature tokens.\nPartner
requires_groovy_code
