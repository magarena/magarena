name=Barbed Field
image=https://cards.scryfall.io/border_crop/front/1/c/1c76db48-6f05-49c3-a49c-587c0a8a3613.jpg?1562900292
value=2.946
rarity=U
type=Enchantment
subtype=Aura
cost={2}{R}{R}
ability=Enchant land;\
        Enchanted land has "{T}: This land deals 1 damage to target creature or player."
timing=aura
enchant=pump,pos land
oracle=Enchant land\nEnchanted land has "{T}: This land deals 1 damage to target creature or player."
