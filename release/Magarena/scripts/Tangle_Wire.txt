name=Tangle Wire
image=https://cards.scryfall.io/border_crop/front/a/d/ad62f313-8a8a-4ffa-ada2-b12b76288729.jpg?1562631474
value=4.562
rarity=R
type=Artifact
cost={3}
ability=Fading 4
timing=artifact
requires_groovy_code
oracle=Fading 4\nAt the beginning of each player's upkeep, that player taps an untapped artifact, creature, or land he or she controls for each fade counter on Tangle Wire.
