name=Phyrexian Obliterator
image=https://cards.scryfall.io/border_crop/front/6/7/67a9c38b-6b3a-4056-a87c-fc48446f854f.jpg?1675957044
image_updated=2018-04-27
value=4.354
rarity=M
type=Creature
subtype=Horror
cost={B}{B}{B}{B}
pt=5/5
ability=Trample
timing=main
requires_groovy_code
oracle=Trample\nWhenever a source deals damage to Phyrexian Obliterator, that source's controller sacrifices that many permanents.
