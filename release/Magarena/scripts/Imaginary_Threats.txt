name=Imaginary Threats
image=https://cards.scryfall.io/border_crop/front/5/0/50c3a6a5-9a50-4a38-9f5f-b8caa320909d.jpg?1562799019
value=2.500
rarity=U
type=Instant
cost={2}{U}{U}
ability=Cycling {2}
timing=removal
requires_groovy_code
oracle=Creatures target opponent controls attack this turn if able. During that player's next untap step, creatures he or she controls don't untap.\nCycling {2}
