name=Magnigoth Treefolk
image=https://cards.scryfall.io/border_crop/front/9/0/90c2869b-43cf-4d5e-8a54-9ae200f5bff9.jpg?1562925506
value=3.657
rarity=R
type=Creature
subtype=Treefolk
cost={4}{G}
pt=2/6
timing=main
requires_groovy_code
oracle=Domain — For each basic land type among lands you control, Magnigoth Treefolk has landwalk of that type.
