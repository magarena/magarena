name=Terravore
image=https://cards.scryfall.io/border_crop/front/c/3/c39c412b-2f21-483a-b744-5d55bc007c0d.jpg?1562931563
value=4.139
rarity=R
type=Creature
subtype=Lhurgoyf
cost={1}{G}{G}
pt=*/*
ability=Trample;\
        SN's power and toughness are each equal to the number of land cards in all graveyards.
timing=main
oracle=Trample\nTerravore's power and toughness are each equal to the number of land cards in all graveyards.
