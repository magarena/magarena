name=Serum Raker
image=https://cards.scryfall.io/border_crop/front/f/1/f157d08a-7cd7-4120-a8bb-1b50fa0ba99b.jpg?1562615834
value=2.109
rarity=C
type=Creature
subtype=Drake
cost={2}{U}{U}
pt=3/2
ability=Flying;\
        When SN dies, each player discards a card.
timing=main
oracle=Flying\nWhen Serum Raker dies, each player discards a card.
