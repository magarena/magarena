name=Gravespawn Sovereign
image=https://cards.scryfall.io/border_crop/front/5/8/58d36326-882e-46bb-a00c-5ea0f6515f65.jpg?1637633090
value=3.920
rarity=R
type=Creature
subtype=Zombie
cost={4}{B}{B}
pt=3/3
ability=Tap five untapped Zombies you control: Put target creature card from a graveyard onto the battlefield under your control.
timing=main
oracle=Tap five untapped Zombies you control: Put target creature card from a graveyard onto the battlefield under your control.
