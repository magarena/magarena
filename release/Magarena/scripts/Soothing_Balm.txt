name=Soothing Balm
image=https://cards.scryfall.io/border_crop/front/9/6/96b8f4be-9f4d-4373-8141-a03518ecd38a.jpg?1562382063
value=2.472
rarity=C
type=Instant
cost={1}{W}
effect=Target player gains 5 life.
timing=main
oracle=Target player gains 5 life.
