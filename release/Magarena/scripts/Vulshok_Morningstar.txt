name=Vulshok Morningstar
image=https://cards.scryfall.io/border_crop/front/0/3/033aa2cd-531c-4594-a7eb-b3cbeb2c5a3c.jpg?1581708589
image_updated=2017-01-11
value=3.433
rarity=U
type=Artifact
subtype=Equipment
cost={2}
ability=Equipped creature gets +2/+2.;\
        Equip {2}
timing=equipment
oracle=Equipped creature gets +2/+2.\nEquip {2}
