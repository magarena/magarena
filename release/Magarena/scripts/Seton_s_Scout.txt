name=Seton's Scout
image=https://cards.scryfall.io/border_crop/front/7/3/732bf7bb-5326-4742-8311-96ddbfe14b38.jpg?1562630354
value=3.717
rarity=U
type=Creature
subtype=Centaur,Druid,Scout,Archer
cost={1}{G}
pt=2/1
ability=Reach;\
        SN gets +2/+2 as long as seven or more cards are in your graveyard.
timing=main
oracle=Reach\nThreshold — Seton's Scout gets +2/+2 as long as seven or more cards are in your graveyard.
