name=Savaen Elves
image=https://cards.scryfall.io/border_crop/front/3/8/38fb3014-f631-4a75-92cd-7e626b13a4c3.jpg?1562908406
value=2.476
rarity=C
type=Creature
subtype=Elf
cost={G}
pt=1/1
timing=main
requires_groovy_code
oracle={G}{G}, {T}: Destroy target Aura attached to a land.
