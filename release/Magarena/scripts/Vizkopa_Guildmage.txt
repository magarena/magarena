name=Vizkopa Guildmage
image=https://cards.scryfall.io/border_crop/front/d/f/dfdce9d1-a163-40e1-a70a-1f09cf400a6c.jpg?1562855100
image_updated=2017-04-12
value=3.825
rarity=U
type=Creature
subtype=Human,Wizard
cost={W}{B}
pt=2/2
ability={1}{W}{B}: Target creature gains lifelink until end of turn.
timing=main
requires_groovy_code
oracle={1}{W}{B}: Target creature gains lifelink until end of turn.\n{1}{W}{B}: Whenever you gain life this turn, each opponent loses that much life.
