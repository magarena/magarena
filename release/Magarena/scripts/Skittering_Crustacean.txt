name=Skittering Crustacean
image=https://cards.scryfall.io/border_crop/front/7/b/7b7cf6ee-55a9-4ce0-8653-b2fd6f820753.jpg?1576381774
image_updated=2016-09-05
value=2.500
rarity=C
type=Creature
subtype=Crab
cost={2}{U}
pt=2/3
ability={6}{U}: Monstrosity 4.;\
        As long as SN is monstrous, it has hexproof.
timing=main
oracle={6}{U}: Monstrosity 4.\nAs long as Skittering Crustacean is monstrous, it has hexproof.
