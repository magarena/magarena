name=Great Sable Stag
image=https://cards.scryfall.io/border_crop/front/4/5/451c8a79-1e8a-437d-836f-31155914c551.jpg?1561978268
value=4.351
rarity=R
type=Creature
subtype=Elk
cost={1}{G}{G}
pt=3/3
ability=This spell can't be countered.;\
        Protection from blue;protection from black
timing=main
oracle=This spell can't be countered.\nProtection from blue and from black
