name=Sangrophage
image=https://cards.scryfall.io/border_crop/front/c/9/c9ceaa1f-4c11-4f06-aa87-2f2a0deb47e1.jpg?1619396009
value=2.840
rarity=C
type=Creature
subtype=Zombie
cost={B}{B}
pt=3/3
ability=At the beginning of your upkeep, tap SN unless you pay 2 life.
timing=main
oracle=At the beginning of your upkeep, tap Sangrophage unless you pay 2 life.
