name=Storm Sculptor
image=https://cards.scryfall.io/border_crop/front/4/b/4bb9ac55-4bb4-4447-b214-6a108ddb3f07.jpg?1600698728
value=2.500
rarity=C
type=Creature
subtype=Merfolk,Wizard
cost={3}{U}
pt=3/2
ability=SN can't be blocked.;\
        When SN enters the battlefield, return a creature you control to its owner's hand.
timing=main
oracle=Storm Sculptor can't be blocked.\nWhen Storm Sculptor enters the battlefield, return a creature you control to its owner's hand.
