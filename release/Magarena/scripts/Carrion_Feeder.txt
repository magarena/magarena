name=Carrion Feeder
image=https://cards.scryfall.io/border_crop/front/0/a/0a19da90-880e-4eca-8cf7-6d7baf090d53.jpg?1562201576
value=4.348
rarity=C
type=Creature
subtype=Zombie
cost={B}
pt=1/1
ability=SN can't block.;\
        Sacrifice a creature: Put a +1/+1 counter on SN. Activate this ability with AI only if you control another creature.
timing=main
oracle=Carrion Feeder can't block.\nSacrifice a creature: Put a +1/+1 counter on Carrion Feeder.
